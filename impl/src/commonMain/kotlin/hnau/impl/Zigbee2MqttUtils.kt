package hnau.impl

import hnau.common.mqtt.topic.MqttTopic

internal object Zigbee2MqttUtils {

    val root: MqttTopic.Absolute = MqttTopic.Absolute.root + "zigbee2mqtt"
}