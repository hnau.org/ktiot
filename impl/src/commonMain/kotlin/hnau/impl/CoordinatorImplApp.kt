package hnau.impl

import hnau.common.mqtt.MqttClientParameters
import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.MqttConnectionState
import hnau.common.mqtt.flow
import hnau.ktiot.coordinator.applyCoordinator
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.slf4j.simple.SimpleLogger

private val logger = run {
    System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "TRACE")
    KotlinLogging.logger { }
}

fun main() = runBlocking {
    logger.debug { "Start" }
    MqttConnection
        .flow(
            clientParameters = MqttClientParameters(
                serverUri = "tcp://127.0.0.1:1883",
                clientId = "test_client",
                persistence = MemoryPersistence(),
            ),
        )
        .collectLatest { state ->
            when (state) {
                is MqttConnectionState.Connected -> state.connection.applyCoordinator(
                    connectionScope = state.connectionScope,
                ) {
                    with(state.connectionScope) {
                        configRoot()
                    }
                }

                MqttConnectionState.Connecting, is MqttConnectionState.Disconnected -> {}
            }
        }
}
