package hnau.impl

import arrow.core.nonEmptyListOf
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.coordinator.Element
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import hnau.ktiot.scheme.property.presentation.mapper.extractFromJson
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToString
import hnau.ktiot.scheme.property.presentation.mapper.stringToBoolean
import hnau.ktiot.scheme.property.presentation.mapper.then
import kotlinx.coroutines.CoroutineScope

context (CoroutineScope)
internal fun Element.configEspHomeTest() {
    property(
        topic = MqttTopic.Absolute(
            "esphometest",
            "light",
            "statusled",
            "state"
        ),
        publishTopic = MqttTopic.Absolute(
            "esphometest",
            "light",
            "statusled",
            "command"
        ),
        type = PropertyViewType.State.Flag,
        mapper = SerializableMapper
            .mqttDataToString
            .then {
                extractFromJson(
                    JsonPath(key = "state"),
                    JsonType.String,
                )
            }
            .then {
                stringToBoolean(
                    trueKeys = nonEmptyListOf("ON"),
                    falseKey = "OFF",
                )
            },
        valueSource = ValueSource.Manual,
    )
}
