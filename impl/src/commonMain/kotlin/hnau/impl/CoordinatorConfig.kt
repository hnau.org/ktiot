package hnau.impl

import hnau.common.color.RGBABytes
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.coordinator.Element
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import hnau.ktiot.scheme.property.presentation.mapper.extractFromJson
import hnau.ktiot.scheme.property.presentation.mapper.floatsToHSLFloats
import hnau.ktiot.scheme.property.presentation.mapper.hslFloatsToRGBFloats
import hnau.ktiot.scheme.property.presentation.mapper.intToFloat
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import hnau.ktiot.scheme.property.presentation.mapper.json.plus
import hnau.ktiot.scheme.property.presentation.mapper.mapTriple
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToString
import hnau.ktiot.scheme.property.presentation.mapper.rgbFloatsToRGBABytes
import hnau.ktiot.scheme.property.presentation.mapper.then
import hnau.ktiot.scheme.property.presentation.mapper.timesFloat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

context (CoroutineScope)
internal fun Element.configRoot() {
    property(
        topic = MqttTopic.Relative("no_value_2"),
        type = PropertyViewType.State.Timestamp,
        valueSource = ValueSource.Manual,
    )
    val lampTopic = Zigbee2MqttUtils.root + "bedroom_lamp"
    property(
        topic = lampTopic,
        publishTopic = lampTopic + "set",
        type = PropertyViewType.State.RGB,
        mapper = SerializableMapper
            .mqttDataToString
            .then {
                extractFromJson(
                    JsonPath("color") + "hue",
                    JsonType.Int,
                ).and(
                    JsonPath("color") + "saturation",
                    JsonType.Int,
                ).and(
                    JsonPath("brightness"),
                    JsonType.Int,
                )
            }
            .then {
                mapTriple(
                    SerializableMapper.intToFloat.then { timesFloat(1f / 360f) },
                    SerializableMapper.intToFloat.then { timesFloat(1f / 100f) },
                    SerializableMapper.intToFloat.then { timesFloat(1f / 256f) },
                )
            }
            .then { floatsToHSLFloats }
            .then { hslFloatsToRGBFloats }
            .then { rgbFloatsToRGBABytes },
        valueSource = ValueSource.Manual,
    ).apply {
        launch { send(RGBABytes.Magenta) }
    }
    child(
        key = MqttTopic.Relative("temperature_and_humidity")
    ) {
        configTemperatureHumidity()
    }
    child(
        key = MqttTopic.Relative("bedroom_lamp")
    ) {
        configBedroomLamp()
    }
    child(
        key = MqttTopic.Relative("esp_home_test")
    ) {
        configEspHomeTest()
    }
    configViewTypesTest()
}
