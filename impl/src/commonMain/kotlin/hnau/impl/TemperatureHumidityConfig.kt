package hnau.impl

import arrow.core.nonEmptyListOf
import hnau.ktiot.coordinator.Element
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToString
import hnau.ktiot.scheme.property.presentation.mapper.stringToBoolean
import hnau.ktiot.scheme.property.presentation.mapper.then
import kotlinx.coroutines.CoroutineScope

context (CoroutineScope)
internal fun Element.configTemperatureHumidity() {
    val deviceTopic = Zigbee2MqttUtils.root + "temperature_humidity_1"
    property(
        title = "Humidity",
        topic = deviceTopic + "humidity",
        type = PropertyViewType.State.Fraction(
            min = 0f,
            max = 100f,
            display = PropertyViewType.State.Fraction.Display.Raw(
                decimalPlaces = 1,
                suffix = "%"
            )
        ),
        valueSource = ValueSource.Hardware,
    )
    property(
        title = "Temperature",
        topic = deviceTopic + "temperature",
        type = PropertyViewType.State.Number(suffix = "℃"),
        valueSource = ValueSource.Hardware,
    )
    property(
        title = "Battery",
        topic = deviceTopic + "battery",
        type = PropertyViewType.State.Fraction(
            min = 0f,
            max = 100f,
            display = PropertyViewType.State.Fraction.Display.Raw(
                decimalPlaces = 1,
                suffix = "%"
            )
        ),
        valueSource = ValueSource.Hardware,
    )
    val publishDeviceTopic = deviceTopic + "set"
    property(
        title = "Temperature mode",
        topic = deviceTopic + "temperature_display_mode",
        publishTopic = publishDeviceTopic + "temperature_display_mode",
        type = PropertyViewType.State.Enum("celsius", "fahrenheit"),
        valueSource = ValueSource.Manual,
    )
    property(
        title = "Display",
        topic = deviceTopic + "enable_display",
        publishTopic = publishDeviceTopic + "enable_display",
        type = PropertyViewType.State.Flag,
        mapper = SerializableMapper
            .mqttDataToString
            .then {
                stringToBoolean(
                    trueKeys = nonEmptyListOf("ON"),
                    falseKey = "OFF",
                )
            },
        valueSource = ValueSource.Manual,
    )
    property(
        title = "Smiley",
        topic = deviceTopic + "show_smiley",
        publishTopic = publishDeviceTopic + "show_smiley",
        type = PropertyViewType.State.Flag,
        mapper = SerializableMapper
            .mqttDataToString
            .then {
                stringToBoolean(
                    trueKeys = nonEmptyListOf("SHOW"),
                    falseKey = "HIDE",
                )
            },
        valueSource = ValueSource.Manual,
    )
}