package hnau.impl

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.color.RGBABytes
import hnau.common.kotlin.Timestamped
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.coordinator.Element
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock


context (CoroutineScope)
internal fun Element.configViewTypesTest() {
    child(
        key = MqttTopic.Relative("view_types_test"),
    ) {
        listOf(
            "text" to PropertyViewType.State.Text,
            "flag" to PropertyViewType.State.Flag,
            "rgb" to PropertyViewType.State.RGB,
            "option" to PropertyViewType.State.Option(PropertyViewType.State.Text),
            "timestamped" to PropertyViewType.State.Timestamped(PropertyViewType.State.Text),
            "timestamp" to PropertyViewType.State.Timestamp,
            "fraction" to PropertyViewType.State.Fraction(),
            "enum" to PropertyViewType.State.Enum("One", "Two", "Three"),
            "number" to PropertyViewType.State.Number(),
        ).forEach { (key, type) ->
            child(
                MqttTopic.Relative(key),
            ) {
                stateProperties(
                    key = key,
                    type = type,
                )
            }
        }
    }
}

context (CoroutineScope)
private fun <T> Element.stateProperties(
    key: String,
    type: PropertyViewType.State<T>,
) {
    val initialValue: T = when (type) {
        is PropertyViewType.State.Enum -> type.variants.head.key
        PropertyViewType.State.Flag -> true
        PropertyViewType.State.RGB -> RGBABytes.Magenta
        is PropertyViewType.State.Fraction -> 0.5f
        is PropertyViewType.State.Number -> 123.456f
        is PropertyViewType.State.Option<*> -> None
        PropertyViewType.State.Text -> "qwerty"
        PropertyViewType.State.Timestamp -> Clock.System.now()
        is PropertyViewType.State.Timestamped<*> -> Timestamped(
            timestamp = Clock.System.now(),
            value = "QWERTY",
        )
    } as T
    val value = PropertyValue(
        key = key,
        type = type,
        value = initialValue,
    )
    listOf(
        value,
        value.toOption(),
    ).forEach { valueLocal ->
        listOf(
            ValueSource.Manual,
            ValueSource.Calculated,
        ).forEach { valueSource ->
            valueLocal.property(valueSource)
        }
    }
}

private data class PropertyValue<T>(
    val key: String,
    val type: PropertyViewType.State<T>,
    val value: T,
) {

    fun toOption(): PropertyValue<Option<T>> = PropertyValue(
        key = "option_$key",
        type = PropertyViewType.State.Option(type),
        value = Some(value),
    )

    context (Element, CoroutineScope)
    fun property(
        valueSource: ValueSource,
    ) {
        property(
            topic = MqttTopic.Relative(key + "_" + valueSource.name.lowercase()),
            type = type,
            valueSource = valueSource,
        ).apply { launch { send(value) } }
    }
}