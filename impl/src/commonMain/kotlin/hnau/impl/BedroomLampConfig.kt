package hnau.impl

import hnau.ktiot.coordinator.Element
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope

context (CoroutineScope)
internal fun Element.configBedroomLamp() {
    val deviceTopic = Zigbee2MqttUtils.root + "bedroom_lamp"
    val publishDeviceTopic = deviceTopic + "set"
    property(
        title = "Color",
        topic = deviceTopic + "color",
        publishTopic = publishDeviceTopic + "color",
        type = PropertyViewType.State.RGB,
        valueSource = ValueSource.Manual,
    )
}