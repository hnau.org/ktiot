plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    jvm {
        mainRun {
            mainClass.set("hnau.impl.CoordinatorImplAppKt")
        }
    }
    sourceSets {
        commonMain.dependencies {
            implementation(libs.kotlin.datetime)
            implementation(libs.paho)
            implementation(libs.slf4j.kotlin)
            implementation(libs.slf4j.simple)
            implementation(project(":common:color"))
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:coordinator"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
