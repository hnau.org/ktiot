plugins {
    alias(libs.plugins.kotlin.serialization)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(libs.paho)
            implementation(libs.slf4j.kotlin)
        }
    }
}
