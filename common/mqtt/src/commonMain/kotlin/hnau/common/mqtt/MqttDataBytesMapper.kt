package hnau.common.mqtt

import hnau.common.kotlin.mapper.Mapper

private val mqttDataToBytesMapper: Mapper<MqttData, ByteArray> =
    Mapper(MqttData::data, ::MqttData)

val Mapper.Companion.mqttDataToBytes: Mapper<MqttData, ByteArray>
    get() = mqttDataToBytesMapper