package hnau.common.mqtt.utils

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.eclipse.paho.client.mqttv3.IMqttAsyncClient

internal class MqttClientAccessor(
    private val client: IMqttAsyncClient,
) {

    private val accessMutex = Mutex()

    suspend operator fun <R> invoke(
        block: suspend IMqttAsyncClient.() -> R,
    ): R = accessMutex.withLock {
        block(client)
    }
}
