package hnau.common.mqtt.utils

import hnau.common.kotlin.coroutines.createChild
import hnau.common.kotlin.ifNull
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.MqttQos
import hnau.common.mqtt.topic.MqttTopic
import io.github.oshai.kotlinlogging.KLogger
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlin.time.Duration

internal class MqttSubscriptionsCache(
    private val logger: KLogger,
    private val scope: CoroutineScope,
    private val cache: Boolean,
    private val clientAccessor: MqttClientAccessor,
    private val debugDelay: Duration?,
) {

    data class Values(
        val payloads: Flow<MqttData>,
        val getLastPayload: () -> MqttData?,
    )

    private data class Subscription(
        val scope: CoroutineScope,
        val values: Flow<MqttData>,
        val subscribersCount: Int,
        var lastPayload: MqttData? = null,
    )

    private val subscriptions = HashMap<MqttTopic.Absolute, Subscription>()

    private fun acquireSubscription(
        topic: MqttTopic.Absolute,
        qos: MqttQos,
    ): Flow<MqttData> = synchronized(subscriptions) {
        subscriptions[topic]
            ?.let { it.copy(subscribersCount = it.subscribersCount + 1) }
            .ifNull {
                val subscriptionScope = scope.createChild()
                val payloads = subscribe(
                    subscriptionScope = subscriptionScope,
                    topic = topic,
                    qos = qos,
                )
                Subscription(
                    values = payloads,
                    subscribersCount = 1,
                    scope = subscriptionScope,
                ).also { subscription ->
                    if (cache) {
                        subscriptionScope.launch {
                            payloads.collect { newPayload ->
                                subscription.lastPayload = newPayload
                            }
                        }
                    }
                }
            }
            .also { updatedOrCreatedSubscription ->
                subscriptions[topic] = updatedOrCreatedSubscription
            }
            .values
    }

    private fun subscribe(
        subscriptionScope: CoroutineScope,
        topic: MqttTopic.Absolute,
        qos: MqttQos,
    ): Flow<MqttData> = MutableSharedFlow<MqttData>(
        replay = 0,
        extraBufferCapacity = Int.MAX_VALUE,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    ).also { payloads ->
        subscriptionScope.launch {
            clientAccessor {
                logger.debug { "Subscribing to $topic" }
                try {
                    subscribe(
                        topic.parts.encoded,
                        qos.code,
                    ) { _, message ->
                        val payloadString = MqttConstants
                            .payloadMapper
                            .direct(message.payload)
                        logger.info { "Received '$payloadString' from $topic" }
                        payloads.tryEmit(payloadString)
                    }
                        .await()
                    logger.trace { "Subscribed to $topic" }
                } catch (ex: CancellationException) {
                    throw ex
                } catch (th: Throwable) {
                    logger.warn { "Error while subscribing to $topic" }
                }
            }
        }
    }
        .let { flow ->
            when (debugDelay) {
                null -> flow
                else -> flow.onEach { delay(debugDelay) }
            }
        }

    private fun releaseSubscription(
        topic: MqttTopic.Absolute,
    ) {
        synchronized(subscriptions) {
            subscriptions.getValue(topic).run {
                val subscribersCount = subscribersCount - 1
                when {
                    subscribersCount < 0 -> error("Unexpected negative subscribers count")
                    subscribersCount > 0 -> subscriptions[topic] = copy(
                        subscribersCount = subscribersCount,
                    )

                    else -> scope.launch {
                        clientAccessor {
                            if (isConnected) {
                                logger.debug { "Unsubscribing from $topic" }
                                try {
                                    unsubscribe(topic.parts.encoded).await()
                                    logger.trace { "Unsubscribed from $topic" }
                                } catch (ex: CancellationException) {
                                    throw ex
                                } catch (th: Throwable) {
                                    logger.warn { "Error while unsubscribing from $topic" }
                                }
                            }
                            subscriptions.remove(topic)
                        }
                    }
                }
            }
        }
    }

    fun get(
        topic: MqttTopic.Absolute,
        qos: MqttQos,
    ): Values = Values(
        payloads = callbackFlow {
            launch {
                acquireSubscription(topic, qos).collect(::send)
            }
            awaitClose {
                releaseSubscription(topic)
            }
        },
        getLastPayload = { subscriptions[topic]?.lastPayload },
    )
}
