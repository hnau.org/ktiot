package hnau.common.mqtt.utils

import hnau.common.kotlin.mapper.Mapper
import hnau.common.mqtt.MqttData

internal object MqttConstants {

    val payloadMapper: Mapper<ByteArray, MqttData> =
        Mapper(::MqttData, MqttData::data)
}
