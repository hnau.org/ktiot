package hnau.common.mqtt

enum class MqttQos(
    val code: Int,
) {
    AtMostOnce(code = 0),
    AtLeastOnce(code = 1),
    ExactlyOnce(code = 2),
    ;

    companion object {

        val default = AtLeastOnce
    }
}
