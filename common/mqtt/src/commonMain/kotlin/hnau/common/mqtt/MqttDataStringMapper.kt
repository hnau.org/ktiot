package hnau.common.mqtt

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.bytesToString
import hnau.common.kotlin.mapper.plus

private val bytesToStringMapper: Mapper<ByteArray, String> =
    Mapper.bytesToString(MqttData.defaultCharset)

private val mqttDataToStringMapper: Mapper<MqttData, String> =
    Mapper.mqttDataToBytes + bytesToStringMapper

val Mapper.Companion.mqttDataToString: Mapper<MqttData, String>
    get() = mqttDataToStringMapper