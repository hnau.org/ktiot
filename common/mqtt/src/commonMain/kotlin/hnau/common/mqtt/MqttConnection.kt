package hnau.common.mqtt

import hnau.common.mqtt.topic.MqttTopic
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface MqttConnection {

    fun subscribeToState(
        topic: MqttTopic.Absolute,
        qos: MqttQos = MqttQos.default,
    ): StateFlow<MqttData?>

    fun subscribeToEvents(
        topic: MqttTopic.Absolute,
        qos: MqttQos = MqttQos.default,
    ): Flow<MqttData>

    suspend fun publish(
        topic: MqttTopic.Absolute,
        retained: Boolean,
        qos: MqttQos = MqttQos.default,
        payload: MqttData,
    )

    companion object
}
