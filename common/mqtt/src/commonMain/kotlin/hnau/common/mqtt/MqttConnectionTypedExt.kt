package hnau.common.mqtt

import arrow.core.Either
import arrow.core.Option
import arrow.core.toOption
import hnau.common.kotlin.ErrorOrValue
import hnau.common.kotlin.coroutines.mapState
import hnau.common.mqtt.topic.MqttTopic
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map

fun <T> MqttConnection.subscribeToEventsTyped(
    topic: MqttTopic.Absolute,
    deserializer: (MqttData) -> T,
    qos: MqttQos = MqttQos.default,
): Flow<T> = this
    .subscribeToEvents(
        topic = topic,
        qos = qos,
    )
    .map(deserializer)

fun <T> MqttConnection.subscribeToStateTyped(
    scope: CoroutineScope,
    topic: MqttTopic.Absolute,
    deserializer: (MqttData) -> T,
    qos: MqttQos = MqttQos.default,
): StateFlow<Option<ErrorOrValue<T>>> = subscribeToState(
    topic = topic,
    qos = qos,
).mapState(scope) { serializedOrNull ->
    serializedOrNull
        .toOption()
        .map { serialized ->
            Either.catch { deserializer(serialized) }
        }
}

suspend fun <T> MqttConnection.publishTyped(
    topic: MqttTopic.Absolute,
    retained: Boolean,
    serializer: (T) -> MqttData,
    qos: MqttQos = MqttQos.default,
    payload: T,
) = publish(
    topic = topic,
    retained = retained,
    qos = qos,
    payload = payload.let(serializer),
)
