package hnau.common.mqtt.topic

fun MqttTopic.toAbsolute(
    relativePrefix: MqttTopic.Absolute,
): MqttTopic.Absolute = when (this) {
    is MqttTopic.Absolute -> this
    is MqttTopic.Relative -> relativePrefix + this
}

fun MqttTopic.Relative.toTitle(): String =
    parts.toTitle()

fun MqttTopic.Absolute.toTitle(): String =
    MqttTopicParts.Separator + parts.toTitle()

fun MqttTopic.toTitle(): String = when (this) {
    is MqttTopic.Absolute -> toTitle()
    is MqttTopic.Relative -> toTitle()
}
