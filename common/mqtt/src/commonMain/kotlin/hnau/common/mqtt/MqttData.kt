package hnau.common.mqtt

import kotlinx.serialization.Serializable

@Serializable
@JvmInline
value class MqttData(
    val data: ByteArray,
) {

    override fun toString(): String = try {
        data.toString(defaultCharset)
    } catch (th: Throwable) {
        data.toString()
    }

    companion object {

        val empty: MqttData = MqttData(byteArrayOf())

        val defaultCharset = Charsets.UTF_8
    }
}
