package hnau.common.mqtt

import org.eclipse.paho.client.mqttv3.MqttClientPersistence
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttPingSender
import org.eclipse.paho.client.mqttv3.TimerPingSender
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import java.util.concurrent.ScheduledExecutorService

data class MqttClientParameters(
    val serverUri: String,
    val clientId: String,
    val connectOptions: MqttConnectOptions = MqttConnectOptions(),
    val persistence: MqttClientPersistence = defaultPersistence,
    val executorService: ScheduledExecutorService? = defaultScheduledExecutorService,
    val pingSender: MqttPingSender = defaultPingSender,
) {

    companion object {

        val defaultPersistence: MqttClientPersistence
            get() = MemoryPersistence()

        val defaultScheduledExecutorService: ScheduledExecutorService?
            get() = null

        val defaultPingSender: MqttPingSender
            get() = TimerPingSender()
    }
}
