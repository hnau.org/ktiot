package hnau.common.mqtt

import arrow.core.Either
import hnau.common.kotlin.coroutines.createChild
import hnau.common.mqtt.topic.MqttTopic
import hnau.common.mqtt.utils.MqttClientAccessor
import hnau.common.mqtt.utils.MqttConstants
import hnau.common.mqtt.utils.MqttSubscriptionsCache
import hnau.common.mqtt.utils.await
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.cancel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttAsyncClient
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.util.concurrent.CancellationException
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds
import kotlin.time.TimeMark
import kotlin.time.TimeSource

private val logger = KotlinLogging.logger {}

sealed interface MqttConnectionState {

    data class Disconnected(
        val nextAttempt: TimeMark,
        val reason: Throwable,
    ) : MqttConnectionState

    data object Connecting : MqttConnectionState

    data class Connected(
        val connectionScope: CoroutineScope,
        val connection: MqttConnection,
    ) : MqttConnectionState
}

fun MqttConnection.Companion.flow(
    clientParameters: MqttClientParameters,
    debugDelay: Duration? = null,
): Flow<MqttConnectionState> = flow {
    coroutineScope {
        val flowScope = this
        while (true) {
            val client = createConnectedClient(
                clientParameters = clientParameters,
                onStateChanged = ::emit,
            )
            val connectionMetadata = ConnectionMetadata(
                scope = flowScope.createChild(),
                accessor = MqttClientAccessor(client),
            )
            try {
                val connection = connectionMetadata.accessor.toConnection(
                    debugDelay = debugDelay,
                    scope = connectionMetadata.scope,
                )
                emit(
                    MqttConnectionState.Connected(
                        connectionScope = connectionMetadata.scope,
                        connection = connection,
                    ),
                )
                client.awaitDisconnection()
            } finally {
                connectionMetadata.run {
                    accessor.disconnect()
                    scope.cancel()
                }
            }
        }
    }
}

private data class ConnectionMetadata(
    val scope: CoroutineScope,
    val accessor: MqttClientAccessor,
)

private suspend fun MqttClientAccessor.disconnect() = withContext(NonCancellable) {
    invoke {
        if (isConnected) {
            try {
                disconnect().await()
            } catch (th: Throwable) {
                logger.warn { "Error while disconnecting from client: ${th.message}" }
            }
        }
    }
}

private suspend fun IMqttAsyncClient.awaitDisconnection() =
    suspendCancellableCoroutine { disconnectionContinuation ->
        setCallback(
            object : MqttCallback {
                override fun connectionLost(cause: Throwable?) =
                    disconnectionContinuation.resume(Unit, null)

                override fun messageArrived(topic: String?, message: MqttMessage?) {}
                override fun deliveryComplete(token: IMqttDeliveryToken?) {}
            },
        )
    }

private fun MqttClientAccessor.toConnection(
    scope: CoroutineScope,
    debugDelay: Duration?,
): MqttConnection = object : MqttConnection {

    private val accessor: MqttClientAccessor
        get() = this@toConnection

    private val statesCache: MqttSubscriptionsCache = MqttSubscriptionsCache(
        logger = logger,
        scope = scope,
        clientAccessor = accessor,
        cache = true,
        debugDelay = debugDelay,
    )

    override fun subscribeToState(
        topic: MqttTopic.Absolute,
        qos: MqttQos,
    ): StateFlow<MqttData?> = statesCache.get(
        topic = topic,
        qos = qos,
    ).let { (payloads, getLastPayload) ->
        object : StateFlow<MqttData?> {

            override val value: MqttData?
                get() = getLastPayload()

            override val replayCache: List<MqttData?>
                get() = listOf(value)

            override suspend fun collect(
                collector: FlowCollector<MqttData?>,
            ): Nothing {
                collector.emit(value)
                payloads.collect(collector)
                awaitCancellation()
            }
        }
    }

    private val eventsCache: MqttSubscriptionsCache = MqttSubscriptionsCache(
        logger = logger,
        scope = scope,
        clientAccessor = accessor,
        cache = false,
        debugDelay = debugDelay,
    )

    override fun subscribeToEvents(
        topic: MqttTopic.Absolute,
        qos: MqttQos,
    ): Flow<MqttData> = eventsCache
        .get(
            topic = topic,
            qos = qos,
        )
        .payloads

    override suspend fun publish(
        topic: MqttTopic.Absolute,
        retained: Boolean,
        qos: MqttQos,
        payload: MqttData,
    ) {
        if (debugDelay != null) {
            delay(debugDelay)
        }
        val payloadBytes = MqttConstants.payloadMapper.reverse(payload)
        logger.debug { "Publishing to $topic message $payload" }
        accessor {
            publish(
                topic.parts.encoded,
                payloadBytes,
                qos.code,
                retained,
            ).await()
        }
        logger.trace { "Published to $topic message $payload" }
    }
}

private suspend fun createConnectedClient(
    clientParameters: MqttClientParameters,
    onStateChanged: suspend (MqttConnectionState) -> Unit,
): IMqttAsyncClient {
    var reconnectionDelay = 3.seconds
    var result: IMqttAsyncClient?
    do {
        onStateChanged(MqttConnectionState.Connecting)
        val client = tryCreateConnectedClient(clientParameters)
        result = when (client) {
            is Either.Right -> client.value
            is Either.Left -> {
                onStateChanged(
                    MqttConnectionState.Disconnected(
                        nextAttempt = TimeSource.Monotonic.markNow() + reconnectionDelay,
                        reason = client.value,
                    ),
                )
                delay(reconnectionDelay)
                reconnectionDelay *= 2
                null
            }
        }
    } while (result == null)
    return result
}

private suspend fun tryCreateConnectedClient(
    clientParameters: MqttClientParameters,
): Either<Throwable, IMqttAsyncClient> {
    val client = with(clientParameters) {
        MqttAsyncClient(
            serverUri,
            clientId,
            persistence,
            pingSender,
            executorService,
        )
    }
    return try {
        client.connect().await()
        Either.Right(client)
    } catch (ex: CancellationException) {
        throw ex
    } catch (th: Throwable) {
        Either.Left(th)
    }
}
