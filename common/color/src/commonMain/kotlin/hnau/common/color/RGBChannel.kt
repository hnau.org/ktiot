package hnau.common.color

enum class RGBChannel { R, G, B }