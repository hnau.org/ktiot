package hnau.common.compose.uikit.bubble

interface BubblesShower {

    fun showBubble(
        bubble: Bubble,
    )
}
