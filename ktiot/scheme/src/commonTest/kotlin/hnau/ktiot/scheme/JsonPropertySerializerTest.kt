package hnau.ktiot.scheme.hnau.ktiot.scheme

import hnau.common.kotlin.mapper.Mapper
import hnau.ktiot.scheme.property.presentation.serializer.decorator.jsonProperty
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer

class JsonPropertySerializerTest : FunSpec({

    val mapper: Mapper<String, List<String>> = Mapper.jsonProperty(
        key = "qwerty",
        serializer = ListSerializer(String.serializer()),
    )

    val initial = """
            {
                "a": 123,
                "qwerty": ["a", "b", "c"],
                "b": {"sdc":"xcv"}
            }
        """.trimIndent()

    val deserialized = mapper.direct(initial)
    deserialized.shouldBe(
        expected = listOf("a", "b", "c"),
    )


    val serialized = mapper.reverse(deserialized)
    serialized.shouldBe(
        expected = "{\"qwerty\":[\"a\",\"b\",\"c\"]}",
    )
})