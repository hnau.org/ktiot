package hnau.ktiot.scheme.hnau.ktiot.scheme

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.swap
import hnau.common.mqtt.mqttDataToString
import hnau.ktiot.scheme.property.presentation.serializer.PropertySerializer
import hnau.ktiot.scheme.property.presentation.serializer.decorator.PropertySerializerDecorator
import hnau.ktiot.scheme.property.presentation.serializer.decorator.then
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class PropertySerializerTest : FunSpec({
    val serializer: Mapper<String, Option<List<String>>> =
        Mapper.mqttDataToString.swap() + PropertySerializer.Option(
            content = PropertySerializer.String.then(
                PropertySerializerDecorator.JsonProperty(
                    key = "value_key",
                    type = PropertySerializerDecorator.JsonProperty.Type.List(
                        PropertySerializerDecorator.JsonProperty.Type.String,
                    ),
                ),
            ),
        ).mapper


    val values: List<Pair<Option<List<String>>, String>> = listOf(
        None to "",
        Some(listOf("a", "b", "c")) to """+{"value_key":["a","b","c"]}""",
    )

    values.forEach { (value, expectedSerialized) ->
        val serialized = serializer.reverse(value)
        serialized.shouldBe(expectedSerialized)
        val deserialized = serializer.direct(serialized)
        deserialized.shouldBe(value)
    }
})