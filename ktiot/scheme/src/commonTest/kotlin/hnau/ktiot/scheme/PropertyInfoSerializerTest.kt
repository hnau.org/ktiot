package hnau.ktiot.scheme.hnau.ktiot.scheme

import hnau.common.kotlin.mapper.toMapper
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.PropertyInfo
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyType
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.serializer.PropertySerializer
import hnau.ktiot.scheme.property.presentation.serializer.decorator.PropertySerializerDecorator
import hnau.ktiot.scheme.property.presentation.serializer.decorator.then
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.serialization.json.Json

class PropertyInfoSerializerTest : FunSpec({
    val mapper = Json.toMapper(PropertyInfo.Serializer)

    val propertyType = PropertyType(
        viewType = PropertyViewType.State.Option(PropertyViewType.State.Option(PropertyViewType.State.Text)),
        mapper = PropertySerializer.Option(
            PropertySerializer.Option(
                PropertySerializer.String.then(
                    PropertySerializerDecorator.JsonProperty(
                        key = "text_key",
                        type = PropertySerializerDecorator.JsonProperty.Type.String,
                    ),
                ),
            ),
        ),
    )

    val propertyInfo = PropertyInfo(
        topic = MqttTopic.Relative("key"),
        title = null,
        type = propertyType,
        valueSource = ValueSource.Hardware,
    )

    val serialized = mapper.reverse(propertyInfo)
    serialized.shouldBe(
        expected = """{"type":{"view":{"type":"option","contentType":{"type":"option","contentType":{"type":"text"}}},"serializer":{"type":"option","content":{"type":"option","content":{"type":"decorated","serializer":{"type":"string"},"decorator":{"type":"json_property","key":"text_key","property_type":{"type":"string"}}}}}},"valueSource":"hardware"}""",
    )

    val deserialized = mapper.direct(serialized)
    deserialized.shouldBe(propertyInfo)
})