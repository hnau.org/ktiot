package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.color.RGBFloats
import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("floats_to_rgb_floats")
data object FloatsToRGBFloatsSerializableMapper :
    SerializableMapper<Triple<Float, Float, Float>, RGBFloats> {

    @Transient
    override val mapper: Mapper<Triple<Float, Float, Float>, RGBFloats> = Mapper(
        direct = { (r, g, b) -> RGBFloats(r, g, b) },
        reverse = { (r, g, b) -> Triple(r, g, b) },
    )
}

val SerializableMapper.Companion.floatsToRGBFloats: FloatsToRGBFloatsSerializableMapper
    get() = FloatsToRGBFloatsSerializableMapper