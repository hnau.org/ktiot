package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.color.HSLFloats
import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("floats_to_hsl_floats")
data object FloatsToHSLFloatsSerializableMapper :
    SerializableMapper<Triple<Float, Float, Float>, HSLFloats> {

    @Transient
    override val mapper: Mapper<Triple<Float, Float, Float>, HSLFloats> = Mapper(
        direct = { (h, s, l) -> HSLFloats(h, s, l) },
        reverse = { (h, s, l) -> Triple(h, s, l) }
    )
}

val SerializableMapper.Companion.floatsToHSLFloats: FloatsToHSLFloatsSerializableMapper
    get() = FloatsToHSLFloatsSerializableMapper