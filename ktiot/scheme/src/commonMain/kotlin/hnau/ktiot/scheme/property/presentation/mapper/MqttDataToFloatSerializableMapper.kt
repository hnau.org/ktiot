package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.stringToFloat
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_float")
data object MqttDataToFloatSerializableMapper : SerializableMapper<MqttData, Float> {

    @Transient
    override val mapper: Mapper<MqttData, Float> =
        Mapper.mqttDataToString + Mapper.stringToFloat
}

val SerializableMapper.Companion.mqttDataToFloat: MqttDataToFloatSerializableMapper
    get() = MqttDataToFloatSerializableMapper