package hnau.ktiot.scheme.property.presentation

import arrow.core.NonEmptyList
import arrow.core.serialization.NonEmptyListSerializer
import hnau.common.color.RGBABytes
import hnau.common.color.gradient.Gradient
import hnau.common.color.gradient.create
import hnau.common.mqtt.MqttData
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToBoolean
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToFloat
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToInstant
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToOption
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToRGBABytes
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToString
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToTimestamped
import hnau.ktiot.scheme.property.presentation.mapper.mqttDataToUnit
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import arrow.core.Option as ArrowOption
import hnau.common.kotlin.Timestamped as CommonTimestamped
import kotlinx.datetime.Instant as KotlinInstant

@Serializable
sealed interface PropertyViewType<T> {

    @Transient
    val defaultMqttDataMapper: SerializableMapper<MqttData, T>

    @Serializable
    sealed interface State<T> : PropertyViewType<T> {

        @Serializable
        @SerialName("number")
        data class Number(
            val suffix: String = "",
        ) : State<Float> {

            override val defaultMqttDataMapper: SerializableMapper<MqttData, Float>
                get() = SerializableMapper.mqttDataToFloat
        }

        @Serializable
        @SerialName("text")
        data object Text : State<String> {

            override val defaultMqttDataMapper: SerializableMapper<MqttData, String>
                get() = SerializableMapper.mqttDataToString
        }

        @Serializable
        @SerialName("flag")
        data object Flag : State<Boolean> {

            override val defaultMqttDataMapper: SerializableMapper<MqttData, Boolean>
                get() = SerializableMapper.mqttDataToBoolean
        }

        @Serializable
        @SerialName("rgb")
        data object RGB : State<RGBABytes> {

            override val defaultMqttDataMapper: SerializableMapper<MqttData, RGBABytes>
                get() = SerializableMapper.mqttDataToRGBABytes
        }

        @Serializable
        @SerialName("enum")
        data class Enum(
            @Serializable(NonEmptyListSerializer::class)
            val variants: NonEmptyList<Variant>,
        ) : State<String> {

            constructor(
                firstVariant: String,
                vararg otherVariants: String,
            ) : this(
                variants = NonEmptyList(
                    head = firstVariant,
                    tail = otherVariants.toList(),
                ).map { key ->
                    Variant(
                        key = key,
                    )
                }
            )

            @Serializable
            data class Variant(
                val key: String,
                val customTitle: String? = null,
            )

            override val defaultMqttDataMapper: SerializableMapper<MqttData, String>
                get() = SerializableMapper.mqttDataToString
        }

        @Serializable
        @SerialName("timestamp")
        data object Timestamp : State<KotlinInstant> {

            override val defaultMqttDataMapper: SerializableMapper<MqttData, KotlinInstant>
                get() = SerializableMapper.mqttDataToInstant
        }

        @Serializable
        @SerialName("option")
        data class Option<T>(
            val contentType: State<T>,
        ) : State<ArrowOption<T>> {

            @Transient
            override val defaultMqttDataMapper: SerializableMapper<MqttData, ArrowOption<T>> =
                SerializableMapper.mqttDataToOption(contentType.defaultMqttDataMapper)
        }

        @Serializable
        @SerialName("timestamped")
        data class Timestamped<T>(
            val contentType: State<T>,
        ) : State<CommonTimestamped<T>> {

            @Transient
            override val defaultMqttDataMapper: SerializableMapper<MqttData, CommonTimestamped<T>> =
                SerializableMapper.mqttDataToTimestamped(contentType.defaultMqttDataMapper)
        }

        @Serializable
        @SerialName("fraction")
        data class Fraction(
            val min: Float = 0f,
            val max: Float = 1f,
            val display: Display = Display.Percent,
            val gradient: Gradient<RGBABytes> =
                Gradient.create(RGBABytes.Black, RGBABytes.White),
        ) : State<Float> {

            @Serializable
            sealed interface Display {

                @Serializable
                @SerialName("percent")
                data object Percent : Display

                @Serializable
                @SerialName("raw")
                data class Raw(
                    val suffix: String = "",
                    val decimalPlaces: Int = 0,
                ) : Display
            }

            override val defaultMqttDataMapper: SerializableMapper<MqttData, Float>
                get() = SerializableMapper.mqttDataToFloat
        }
    }

    @Serializable
    sealed interface Events<T> : PropertyViewType<T> {

        @Serializable
        @SerialName("tic")
        data object Tic : Events<Unit> {

            @Transient
            override val defaultMqttDataMapper: SerializableMapper<MqttData, Unit>
                get() = SerializableMapper.mqttDataToUnit
        }
    }
}
