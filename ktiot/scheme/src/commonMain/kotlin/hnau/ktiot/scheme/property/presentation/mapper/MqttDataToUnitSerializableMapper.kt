package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.createPredeterminated
import hnau.common.kotlin.mapper.plus
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_unit")
data object MqttDataToUnitSerializableMapper : SerializableMapper<MqttData, Unit> {

    @Transient
    override val mapper: Mapper<MqttData, Unit> =
        Mapper.mqttDataToString + Mapper.createPredeterminated("*", Unit)
}

val SerializableMapper.Companion.mqttDataToUnit: MqttDataToUnitSerializableMapper
    get() = MqttDataToUnitSerializableMapper