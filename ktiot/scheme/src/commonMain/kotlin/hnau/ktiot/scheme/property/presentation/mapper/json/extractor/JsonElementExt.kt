package hnau.ktiot.scheme.property.presentation.mapper.json.extractor

import arrow.core.NonEmptyList
import arrow.core.toNonEmptyListOrNull
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject

tailrec fun <T> JsonElement.extract(
    path: JsonPath,
    type: JsonType<T>,
): T {
    val nonEmptyParts: NonEmptyList<JsonPath.Part> = path
        .parts
        .toNonEmptyListOrNull()
        ?: return type.jsonMapper.direct(this)
    val innerElement: JsonElement = when (val part = nonEmptyParts.head) {
        is JsonPath.Part.Key -> jsonObject.getValue(part.key)
        is JsonPath.Part.Index -> jsonArray[part.index]
    }
    return innerElement.extract(
        path = JsonPath(nonEmptyParts.tail),
        type = type,
    )
}

fun <T> buildJsonElement(
    value: T,
    path: JsonPath,
    type: JsonType<T>,
): JsonElement {
    val nonEmptyParts: NonEmptyList<JsonPath.Part> = path
        .parts
        .toNonEmptyListOrNull()
        ?: return type.jsonMapper.reverse(value)
    val nestedJson: JsonElement = buildJsonElement(
        value = value,
        path = JsonPath(nonEmptyParts.tail),
        type = type,
    )
    return when (val part = nonEmptyParts.head) {
        is JsonPath.Part.Key -> JsonObject(mapOf(part.key to nestedJson))
        is JsonPath.Part.Index -> JsonArray(listOf(nestedJson))
    }
}

fun JsonElement.mergeWith(
    other: JsonElement,
): JsonElement {

    fun throwUnableMerge(): Nothing = throw IllegalArgumentException(
        "Unable merge two JSONs: `${log}` and `${other.log}`"
    )

    return when (this) {
        is JsonArray -> when (other) {
            is JsonArray -> JsonArray(
                buildList {
                    addAll(this)
                    addAll(other)
                }
            )

            is JsonObject,
            is JsonPrimitive,
            JsonNull,
            -> throwUnableMerge()
        }

        is JsonObject -> when (other) {
            is JsonObject -> run {
                val otherElements: MutableMap<String, JsonElement> = other.toMutableMap()
                val mergedElements: Map<String, JsonElement> = mapValues { (key, value) ->
                    when (val otherValueWithSameKey = otherElements.remove(key)) {
                        null -> value
                        else -> value.mergeWith(otherValueWithSameKey)
                    }
                } + otherElements
                JsonObject(mergedElements)
            }

            is JsonArray,
            is JsonPrimitive,
            JsonNull,
            -> throwUnableMerge()
        }

        is JsonPrimitive,
        JsonNull,
        -> throwUnableMerge()
    }
}

private val loggingJson: Json =
    Json { prettyPrint = false }

private val JsonElement.log: String
    get() = loggingJson.encodeToString(JsonElement.serializer(), this)