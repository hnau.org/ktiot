package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("combine")
data class CombineSerializableMapper<I, M, O>(
    private val first: SerializableMapper<I, M>,
    private val second: SerializableMapper<M, O>,
) : SerializableMapper<I, O> {

    @Transient
    override val mapper: Mapper<I, O> =
        first.mapper + second.mapper
}

fun <I, M, O> SerializableMapper.Companion.combine(
    first: SerializableMapper<I, M>,
    second: SerializableMapper<M, O>,
): CombineSerializableMapper<I, M, O> = CombineSerializableMapper(
    first = first,
    second = second,
)

fun <I, M, O> SerializableMapper<I, M>.then(
    other: SerializableMapper<M, O>,
): CombineSerializableMapper<I, M, O> = CombineSerializableMapper(
    first = this,
    second = other,
)

inline fun <I, M, O> SerializableMapper<I, M>.then(
    build: SerializableMapper.Companion.() -> SerializableMapper<M, O>,
): CombineSerializableMapper<I, M, O> = then(
    other = SerializableMapper.build(),
)

operator fun <I, M, O> SerializableMapper<I, M>.plus(
    other: SerializableMapper<M, O>,
): CombineSerializableMapper<I, M, O> = then(other)