package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.color.RGBABytes
import hnau.common.color.getStringMapper
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_rgb")
data object MqttDataToRGBABytesSerializableMapper : SerializableMapper<MqttData, RGBABytes> {

    @Transient
    override val mapper: Mapper<MqttData, RGBABytes> =
        Mapper.mqttDataToString + RGBABytes.getStringMapper(useAlpha = false)
}

val SerializableMapper.Companion.mqttDataToRGBABytes: MqttDataToRGBABytesSerializableMapper
    get() = MqttDataToRGBABytesSerializableMapper