package hnau.ktiot.scheme.property.presentation.mapper.json

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.stringSplit
import hnau.common.kotlin.serialization.MappingKSerializer
import kotlinx.serialization.builtins.serializer

object JsonPathSerializer : MappingKSerializer<String, JsonPath>(
    base = String.serializer(),
    mapper = Mapper.stringSplit('/') + Mapper(
        direct = { parts ->
            JsonPath(
                parts.map { part ->
                    when {
                        part.startsWith(KeyPrefix) ->
                            JsonPath.Part.Key(part.drop(1))

                        else ->
                            JsonPath.Part.Index(part.toInt())
                    }
                }
            )
        },
        reverse = { (parts) ->
            parts.map { part ->
                when (part) {
                    is JsonPath.Part.Index -> part.index.toString()
                    is JsonPath.Part.Key -> "$KeyPrefix${part.key}"
                }
            }
        }
    )
)

private const val KeyPrefix = '$'