package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.Serializable

@Serializable
sealed interface SerializableMapper<I, O> {

    val mapper: Mapper<I, O>

    companion object
}