package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("int_to_float")
data object IntToFloatSerializableMapper :
    SerializableMapper<Int, Float> {

    @Transient
    override val mapper: Mapper<Int, Float> = Mapper(
        direct = Int::toFloat,
        reverse = Float::toInt,
    )
}

val SerializableMapper.Companion.intToFloat: IntToFloatSerializableMapper
    get() = IntToFloatSerializableMapper