package hnau.ktiot.scheme.property.presentation

val <T, S : PropertyViewType.State<T>> S.option: PropertyViewType.State.Option<T>
    get() = PropertyViewType.State.Option(this)

val <T, S : PropertyViewType.State<T>> S.timestamped: PropertyViewType.State.Timestamped<T>
    get() = PropertyViewType.State.Timestamped(this)