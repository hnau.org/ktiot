package hnau.ktiot.scheme

import hnau.common.mqtt.topic.MqttTopic


object SchemeConstants {

    val ktiotTopic: MqttTopic.Absolute = MqttTopic.Absolute.root + "ktiot"

    val dataElementTopic: MqttTopic.Absolute = ktiotTopic + "data"

    val schemeTopic: MqttTopic.Absolute = ktiotTopic + "scheme"
}
