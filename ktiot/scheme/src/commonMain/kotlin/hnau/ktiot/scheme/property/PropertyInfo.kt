package hnau.ktiot.scheme.property

import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.presentation.PropertyType
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.serializer

@Serializable
data class PropertyInfo<T>(
    val topic: MqttTopic,
    val publishTopic: MqttTopic? = null,
    val title: String? = null,
    val type: PropertyType<T>,
    val valueSource: ValueSource = ValueSource.Calculated,
) {

    object Serializer : KSerializer<PropertyInfo<Any?>> by serializer()

    companion object {

        fun <T> serializer(): KSerializer<PropertyInfo<T>> =
            serializer(Unit.serializer() as KSerializer<T>)
    }
}