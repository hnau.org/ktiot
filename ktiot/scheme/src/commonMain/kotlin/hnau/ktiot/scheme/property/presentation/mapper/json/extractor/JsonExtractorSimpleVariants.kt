package hnau.ktiot.scheme.property.presentation.mapper.json.extractor

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.equality
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType

fun <T> JsonExtractor.Companion.single(
    path: JsonPath,
    type: JsonType<T>,
): JsonExtractor<T> = JsonExtractor(
    path = path,
    type = type,
)

fun <A, B> JsonExtractor.Companion.pair(
    pathA: JsonPath,
    typeA: JsonType<A>,
    pathB: JsonPath,
    typeB: JsonType<B>,
): JsonExtractor<Pair<A, B>> = JsonExtractor
    .single(
        path = pathA,
        type = typeA,
    )
    .then(
        path = pathB,
        type = typeB,
        mergeMapper = Mapper.equality(),
    )

fun <A, B, C> JsonExtractor.Companion.triple(
    pathA: JsonPath,
    typeA: JsonType<A>,
    pathB: JsonPath,
    typeB: JsonType<B>,
    pathC: JsonPath,
    typeC: JsonType<C>,
): JsonExtractor<Triple<A, B, C>> = JsonExtractor
    .pair(
        pathA = pathA,
        typeA = typeA,
        pathB = pathB,
        typeB = typeB,
    )
    .then(
        path = pathC,
        type = typeC,
        mergeMapper = Mapper(
            direct = { (ab, c) ->
                val (a, b) = ab
                Triple(a, b, c)
            },
            reverse = { (a, b, c) ->
                (a to b) to c
            }
        ),
    )