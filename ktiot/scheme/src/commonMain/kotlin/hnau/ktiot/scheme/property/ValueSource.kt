package hnau.ktiot.scheme.property

import kotlinx.serialization.SerialName

enum class ValueSource {

    @SerialName("hardware")
    Hardware,

    @SerialName("calculated")
    Calculated,

    @SerialName("manual")
    Manual,
    ;

    companion object {

        val default = Calculated
    }
}
