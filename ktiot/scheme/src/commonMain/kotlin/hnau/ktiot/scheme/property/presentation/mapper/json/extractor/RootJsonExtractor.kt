package hnau.ktiot.scheme.property.presentation.mapper.json.extractor

import hnau.common.kotlin.mapper.Mapper
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import kotlinx.serialization.json.JsonElement

class RootJsonExtractor<T>(
    private val path: JsonPath,
    private val type: JsonType<T>,
) : JsonExtractor<T> {

    override val mapper: Mapper<JsonElement, T> = Mapper(
        direct = { json -> json.extract(path, type) },
        reverse = { value -> buildJsonElement(value, path, type) },
    )

    override fun <N, R> then(
        path: JsonPath,
        type: JsonType<N>,
        mergeMapper: Mapper<Pair<T, N>, R>,
    ): JsonExtractor<R> = NestedJsonExtractor(
        parent = this,
        path = path,
        type = type,
        mergeMapper = mergeMapper,
    )
}