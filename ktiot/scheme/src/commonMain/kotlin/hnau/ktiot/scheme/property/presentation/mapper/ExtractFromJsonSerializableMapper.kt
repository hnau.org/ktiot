package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.toMapper
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import hnau.ktiot.scheme.property.presentation.mapper.json.extractor.JsonExtractor
import hnau.ktiot.scheme.property.presentation.mapper.json.extractor.pair
import hnau.ktiot.scheme.property.presentation.mapper.json.extractor.single
import hnau.ktiot.scheme.property.presentation.mapper.json.extractor.triple
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement

private val stringToJsonElementMapper: Mapper<String, JsonElement> =
    Json.toMapper(JsonElement.serializer())

@Serializable
@SerialName("extract_from_json")
data class ExtractFromJsonSerializableMapper<T>(
    private val path: JsonPath,
    @SerialName("json_type")
    private val type: JsonType<T>,
) : SerializableMapper<String, T> {

    @Transient
    override val mapper: Mapper<String, T> = stringToJsonElementMapper +
            JsonExtractor
                .single(
                    path = path,
                    type = type
                )
                .mapper

    fun <B> and(
        path: JsonPath,
        type: JsonType<B>,
    ): JsonPairSerializableMapper<T, B> = JsonPairSerializableMapper(
        pathA = this.path,
        typeA = this.type,
        pathB = path,
        typeB = type,
    )
}

fun <T> SerializableMapper.Companion.extractFromJson(
    path: JsonPath,
    type: JsonType<T>,
): ExtractFromJsonSerializableMapper<T> = ExtractFromJsonSerializableMapper(
    path = path,
    type = type
)


@Serializable
@SerialName("json_pair")
data class JsonPairSerializableMapper<A, B>(
    private val pathA: JsonPath,
    private val typeA: JsonType<A>,
    private val pathB: JsonPath,
    private val typeB: JsonType<B>,
) : SerializableMapper<String, Pair<A, B>> {

    @Transient
    override val mapper: Mapper<String, Pair<A, B>> = stringToJsonElementMapper +
            JsonExtractor
                .pair(
                    pathA = pathA,
                    typeA = typeA,
                    pathB = pathB,
                    typeB = typeB,
                )
                .mapper

    fun <C> and(
        path: JsonPath,
        type: JsonType<C>,
    ): JsonTripleSerializableMapper<A, B, C> = JsonTripleSerializableMapper(
        pathA = pathA,
        typeA = typeA,
        pathB = pathB,
        typeB = typeB,
        pathC = path,
        typeC = type,
    )
}


@Serializable
@SerialName("json_triple")
data class JsonTripleSerializableMapper<A, B, C>(
    private val pathA: JsonPath,
    private val typeA: JsonType<A>,
    private val pathB: JsonPath,
    private val typeB: JsonType<B>,
    private val pathC: JsonPath,
    private val typeC: JsonType<C>,
) : SerializableMapper<String, Triple<A, B, C>> {

    @Transient
    override val mapper: Mapper<String, Triple<A, B, C>> = stringToJsonElementMapper +
            JsonExtractor
                .triple(
                    pathA = pathA,
                    typeA = typeA,
                    pathB = pathB,
                    typeB = typeB,
                    pathC = pathC,
                    typeC = typeC,
                )
                .mapper
}