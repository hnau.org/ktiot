package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.stringToBoolean
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_boolean")
data object MqttDataToBooleanSerializableMapper : SerializableMapper<MqttData, Boolean> {

    @Transient
    override val mapper: Mapper<MqttData, Boolean> =
        Mapper.mqttDataToString + Mapper.stringToBoolean
}

val SerializableMapper.Companion.mqttDataToBoolean: MqttDataToBooleanSerializableMapper
    get() = MqttDataToBooleanSerializableMapper