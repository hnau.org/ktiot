package hnau.ktiot.scheme.property.presentation.mapper.json.extractor

import hnau.common.kotlin.mapper.Mapper
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import kotlinx.serialization.json.JsonElement

class NestedJsonExtractor<T, N, R>(
    private val parent: JsonExtractor<T>,
    private val path: JsonPath,
    private val type: JsonType<N>,
    private val mergeMapper: Mapper<Pair<T, N>, R>,
) : JsonExtractor<R> {

    override val mapper: Mapper<JsonElement, R> = Mapper(
        direct = { json ->
            val parentValue = parent.mapper.direct(json)
            val thisValue = json.extract(path, type)
            mergeMapper.direct(parentValue to thisValue)
        },
        reverse = { value ->
            val (parentValue, thisValue) = mergeMapper.reverse(value)
            val parentJson = parent.mapper.reverse(parentValue)
            val thisJson = buildJsonElement(thisValue, path, type)
            parentJson.mergeWith(thisJson)
        },
    )

    override fun <N1, R1> then(
        path: JsonPath,
        type: JsonType<N1>,
        mergeMapper: Mapper<Pair<R, N1>, R1>,
    ): JsonExtractor<R1> = NestedJsonExtractor(
        parent = this,
        path = path,
        type = type,
        mergeMapper = mergeMapper,
    )

}