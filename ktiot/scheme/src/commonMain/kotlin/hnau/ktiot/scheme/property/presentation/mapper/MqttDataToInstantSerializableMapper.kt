package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.datetime.Instant
import kotlinx.datetime.format
import kotlinx.datetime.format.DateTimeComponents
import kotlinx.datetime.format.DateTimeFormat
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_instant")
data object MqttDataToInstantSerializableMapper : SerializableMapper<MqttData, Instant> {


    @Transient
    override val mapper: Mapper<MqttData, Instant> = run {
        val format: DateTimeFormat<DateTimeComponents> =
            DateTimeComponents.Formats.ISO_DATE_TIME_OFFSET

        val stringToInstant: Mapper<String, Instant> = Mapper(
            direct = { encoded ->
                Instant.parse(
                    input = encoded,
                    format = format,
                )
            },
            reverse = { instant ->
                instant.format(
                    format = format,
                )
            }
        )
        Mapper.mqttDataToString + stringToInstant
    }
}

val SerializableMapper.Companion.mqttDataToInstant: MqttDataToInstantSerializableMapper
    get() = MqttDataToInstantSerializableMapper