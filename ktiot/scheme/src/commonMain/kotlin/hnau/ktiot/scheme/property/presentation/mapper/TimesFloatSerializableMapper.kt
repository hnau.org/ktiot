package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("times_float")
data class TimesFloatSerializableMapper(
    val factor: Float,
) : SerializableMapper<Float, Float> {

    @Transient
    override val mapper: Mapper<Float, Float> = Mapper(
        direct = { it * factor },
        reverse = { it / factor }
    )
}

fun SerializableMapper.Companion.timesFloat(
    factor: Float,
): TimesFloatSerializableMapper = TimesFloatSerializableMapper(
    factor = factor,
)