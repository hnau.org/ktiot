package hnau.ktiot.scheme.property.presentation.mapper.json

import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.double
import kotlinx.serialization.json.float
import kotlinx.serialization.json.int
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.json.long
import kotlin.Double as KotlinDouble
import kotlin.Float as KotlinFloat
import kotlin.Int as KotlinInt
import kotlin.Long as KotlinLong
import kotlin.String as KotlinString
import kotlin.collections.List as KotlinList

@Serializable
sealed interface JsonType<T> {

    val jsonMapper: Mapper<JsonElement, T>

    @Serializable
    @SerialName("float")
    data object Float : JsonType<KotlinFloat> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinFloat> = Mapper(
            direct = { json -> json.jsonPrimitive.float },
            reverse = { float -> JsonPrimitive(float) },
        )
    }

    @Serializable
    @SerialName("double")
    data object Double : JsonType<KotlinDouble> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinDouble> = Mapper(
            direct = { json -> json.jsonPrimitive.double },
            reverse = { double -> JsonPrimitive(double) },
        )
    }

    @Serializable
    @SerialName("int")
    data object Int : JsonType<KotlinInt> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinInt> = Mapper(
            direct = { json -> json.jsonPrimitive.int },
            reverse = { int -> JsonPrimitive(int) },
        )
    }

    @Serializable
    @SerialName("long")
    data object Long : JsonType<KotlinLong> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinLong> = Mapper(
            direct = { json -> json.jsonPrimitive.long },
            reverse = { int -> JsonPrimitive(int) },
        )
    }

    @Serializable
    @SerialName("string")
    data object String : JsonType<KotlinString> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinString> = Mapper(
            direct = { json -> json.jsonPrimitive.content },
            reverse = { string -> JsonPrimitive(string) },
        )
    }

    @Serializable
    @SerialName("list")
    data class List<T>(
        private val item: JsonType<T>,
    ) : JsonType<KotlinList<T>> {

        @Transient
        override val jsonMapper: Mapper<JsonElement, KotlinList<T>> = Mapper(
            direct = { json ->
                json.jsonArray.map(item.jsonMapper.direct)
            },
            reverse = { list ->
                JsonArray(list.map(item.jsonMapper.reverse))
            },
        )
    }
}