package hnau.ktiot.scheme

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.toMapper
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.PropertyInfo
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class SchemeElement(
    val title: String? = null,
    val properties: List<@Serializable(PropertyInfo.Serializer::class) PropertyInfo<@Contextual Any?>> = emptyList(),
    val children: Map<MqttTopic.Relative, SchemeElement> = emptyMap(),
) {

    companion object {

        val serializer: Mapper<MqttData, SchemeElement> = run {
            val json = Json
            val stringMapper = json.toMapper(serializer())
            Mapper.mqttDataToString + stringMapper
        }
    }
}
