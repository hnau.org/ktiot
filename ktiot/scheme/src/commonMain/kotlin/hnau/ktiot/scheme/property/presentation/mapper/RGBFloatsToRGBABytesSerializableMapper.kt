package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.color.RGBABytes
import hnau.common.color.RGBFloats
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.swap
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("floats_to_rgba_bytes")
data object RGBFloatsToRGBABytesSerializableMapper :
    SerializableMapper<RGBFloats, RGBABytes> {

    @Transient
    override val mapper: Mapper<RGBFloats, RGBABytes> =
        RGBFloats.rgbaBytesMapper.swap()
}

val SerializableMapper.Companion.rgbFloatsToRGBABytes: RGBFloatsToRGBABytesSerializableMapper
    get() = RGBFloatsToRGBABytesSerializableMapper