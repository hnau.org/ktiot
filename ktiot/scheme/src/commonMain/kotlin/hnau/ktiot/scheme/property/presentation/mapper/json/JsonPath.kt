package hnau.ktiot.scheme.property.presentation.mapper.json

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable(JsonPathSerializer::class)
data class JsonPath(
    val parts: List<Part>,
) {

    constructor(
        vararg parts: Part,
    ) : this(
        parts = parts.toList(),
    )

    constructor(
        key: String,
    ) : this(
        Part.Key(key)
    )

    constructor(
        index: Int,
    ) : this(
        Part.Index(index)
    )

    @Serializable
    sealed interface Part {

        @Serializable
        @SerialName("key")
        data class Key(
            val key: String,
        ) : Part

        @Serializable
        @SerialName("index")
        data class Index(
            val index: Int,
        ) : Part
    }

    companion object
}

operator fun JsonPath.plus(
    part: JsonPath.Part,
): JsonPath = JsonPath(
    parts = parts + part,
)

operator fun JsonPath.plus(
    key: String,
): JsonPath = plus(
    part = JsonPath.Part.Key(key)
)

operator fun JsonPath.plus(
    index: Int,
): JsonPath = plus(
    part = JsonPath.Part.Index(index)
)