package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.Timestamped
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.bytesToString
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.swap
import hnau.common.kotlin.mapper.toMapper
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToBytes
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.json.Json

@Serializable
@SerialName("mqtt_data_to_timestamped")
data class MqttDataToTimestampedSerializableMapper<T>(
    private val content: SerializableMapper<MqttData, T>,
) : SerializableMapper<MqttData, Timestamped<T>> {

    @Transient
    override val mapper: Mapper<MqttData, Timestamped<T>> = run {

        val valueMapper: Mapper<ByteArray, T> = Mapper.mqttDataToBytes.swap() + content.mapper

        Mapper.mqttDataToBytes + Mapper(
            direct = { bytes ->
                val separatorIndex = bytes.indexOf(Separator)
                Timestamped(
                    timestamp = bytes
                        .take(separatorIndex)
                        .toByteArray()
                        .let(instantMapper.direct),
                    value = bytes
                        .drop(separatorIndex + 1)
                        .toByteArray()
                        .let(valueMapper.direct),
                )
            },
            reverse = { (timestamp, value) ->
                buildList {
                    addAll(instantMapper.reverse(timestamp).toList())
                    add(Separator)
                    addAll(valueMapper.reverse(value).toList())
                }.toByteArray()
            }
        )
    }

    companion object {
        private const val Separator: Byte = '|'.code.toByte()

        private val instantMapper: Mapper<ByteArray, Instant> =
            Mapper.bytesToString(Charsets.UTF_8) + Json.toMapper(Instant.serializer())
    }
}

fun <T> SerializableMapper.Companion.mqttDataToTimestamped(
    content: SerializableMapper<MqttData, T>,
): MqttDataToTimestampedSerializableMapper<T> = MqttDataToTimestampedSerializableMapper(
    content = content,
)

inline fun <T> SerializableMapper.Companion.mqttDataToTimestamped(
    buildContent: SerializableMapper.Companion.() -> SerializableMapper<MqttData, T>,
): MqttDataToTimestampedSerializableMapper<T> = mqttDataToTimestamped(
    content = buildContent(),
)