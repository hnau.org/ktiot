package hnau.ktiot.scheme.property.presentation.mapper.json.extractor

import hnau.common.kotlin.mapper.Mapper
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonPath
import hnau.ktiot.scheme.property.presentation.mapper.json.JsonType
import kotlinx.serialization.json.JsonElement

interface JsonExtractor<T> {

    val mapper: Mapper<JsonElement, T>

    fun <N, R> then(
        path: JsonPath,
        type: JsonType<N>,
        mergeMapper: Mapper<Pair<T, N>, R>,
    ): JsonExtractor<R>

    companion object
}

fun <T> JsonExtractor(
    path: JsonPath,
    type: JsonType<T>,
): JsonExtractor<T> = RootJsonExtractor(
    path = path,
    type = type,
)

