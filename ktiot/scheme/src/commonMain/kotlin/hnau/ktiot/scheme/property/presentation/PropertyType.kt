package hnau.ktiot.scheme.property.presentation

import hnau.common.mqtt.MqttData
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.serializer

@Serializable
data class PropertyType<T>(
    val viewType: PropertyViewType<T>,
    val mapper: SerializableMapper<MqttData, T>,
) {

    companion object {

        fun <T> serializer(): KSerializer<PropertyType<T>> =
            serializer(Unit.serializer() as KSerializer<T>)
    }
}
