package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToString
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_string")
data object MqttDataToStringSerializableMapper : SerializableMapper<MqttData, String> {

    @Transient
    override val mapper: Mapper<MqttData, String> =
        Mapper.mqttDataToString
}

val SerializableMapper.Companion.mqttDataToString: MqttDataToStringSerializableMapper
    get() = MqttDataToStringSerializableMapper