package hnau.ktiot.scheme.property.presentation.mapper

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.mqttDataToBytes
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("mqtt_data_to_option")
data class MqttDataToOptionSerializableMapper<T>(
    private val content: SerializableMapper<MqttData, T>,
) : SerializableMapper<MqttData, Option<T>> {

    @Transient
    override val mapper: Mapper<MqttData, Option<T>> = Mapper.mqttDataToBytes + Mapper(
        direct = { encodedOptionBytes ->
            when (encodedOptionBytes.firstOrNull()) {
                SomePrefix -> {
                    val contentBytes = encodedOptionBytes.drop(1).toByteArray()
                    val contentData = MqttData(contentBytes)
                    val content = content.mapper.direct(contentData)
                    Some(content)
                }

                else -> None
            }
        },
        reverse = { value ->
            when (value) {
                None -> byteArrayOf(NonePrefix)
                is Some -> {
                    val contentData = content.mapper.reverse(value.value)
                    val contentBytes = contentData.data
                    byteArrayOf(SomePrefix) + contentBytes
                }
            }
        },
    )

    companion object {
        private const val SomePrefix = '+'.code.toByte()
        private const val NonePrefix = '-'.code.toByte()
    }
}

fun <T> SerializableMapper.Companion.mqttDataToOption(
    content: SerializableMapper<MqttData, T>,
): MqttDataToOptionSerializableMapper<T> = MqttDataToOptionSerializableMapper(
    content = content,
)

inline fun <T> SerializableMapper.Companion.mqttDataToOption(
    buildContent: SerializableMapper.Companion.() -> SerializableMapper<MqttData, T>,
): MqttDataToOptionSerializableMapper<T> = mqttDataToOption(
    content = buildContent(),
)