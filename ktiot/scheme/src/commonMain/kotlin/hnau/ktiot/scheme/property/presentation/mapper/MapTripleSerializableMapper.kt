package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("map_triple")
class MapTripleSerializableMapper<AI, BI, CI, AO, BO, CO>(
    private val aMapper: SerializableMapper<AI, AO>,
    private val bMapper: SerializableMapper<BI, BO>,
    private val cMapper: SerializableMapper<CI, CO>,
) : SerializableMapper<Triple<AI, BI, CI>, Triple<AO, BO, CO>> {

    @Transient
    override val mapper: Mapper<Triple<AI, BI, CI>, Triple<AO, BO, CO>> = Mapper(
        direct = { (a, b, c) ->
            Triple(
                aMapper.mapper.direct(a),
                bMapper.mapper.direct(b),
                cMapper.mapper.direct(c),
            )
        },
        reverse = { (a, b, c) ->
            Triple(
                aMapper.mapper.reverse(a),
                bMapper.mapper.reverse(b),
                cMapper.mapper.reverse(c),
            )
        }
    )
}


fun <AI, BI, CI, AO, BO, CO> SerializableMapper.Companion.mapTriple(
    aMapper: SerializableMapper<AI, AO>,
    bMapper: SerializableMapper<BI, BO>,
    cMapper: SerializableMapper<CI, CO>,
): MapTripleSerializableMapper<AI, BI, CI, AO, BO, CO> = MapTripleSerializableMapper(
    aMapper = aMapper,
    bMapper = bMapper,
    cMapper = cMapper,
)