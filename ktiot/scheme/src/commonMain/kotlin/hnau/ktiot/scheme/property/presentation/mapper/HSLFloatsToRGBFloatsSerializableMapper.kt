package hnau.ktiot.scheme.property.presentation.mapper

import hnau.common.color.HSLFloats
import hnau.common.color.RGBFloats
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.swap
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("hsl_floats_to_rgb_floats")
data object HSLFloatsToRGBFloatsSerializableMapper :
    SerializableMapper<HSLFloats, RGBFloats> {

    @Transient
    override val mapper: Mapper<HSLFloats, RGBFloats> =
        HSLFloats.rgbFloatsMapper.swap()
}

val SerializableMapper.Companion.hslFloatsToRGBFloats: HSLFloatsToRGBFloatsSerializableMapper
    get() = HSLFloatsToRGBFloatsSerializableMapper