package hnau.ktiot.scheme.property.presentation.mapper

import arrow.core.NonEmptyList
import arrow.core.nonEmptyListOf
import arrow.core.serialization.NonEmptyListSerializer
import hnau.common.kotlin.mapper.Mapper
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("string_to_boolean")
data class StringToBooleanSerializableMapper(
    @Serializable(NonEmptyListSerializer::class)
    val trueKeys: NonEmptyList<String> = defaultTrueKeys,
    val falseKey: String = defaultFalseKey,
    val ignoreCase: Boolean = defaultIgnoreCase,
) : SerializableMapper<String, Boolean> {


    @Transient
    override val mapper: Mapper<String, Boolean> = Mapper(
        direct = { string ->
            trueKeys.any { trueKey ->
                trueKey.equals(
                    other = string,
                    ignoreCase = ignoreCase,
                )
            }
            string in trueKeys
        },
        reverse = { boolean ->
            when (boolean) {
                true -> trueKeys.head
                false -> falseKey
            }
        }
    )

    companion object {

        val defaultTrueKeys: NonEmptyList<String> =
            nonEmptyListOf("true", "on", "enabled", "yes", "1")

        val defaultFalseKey: String = "false"

        val defaultIgnoreCase: Boolean = true
    }
}


fun SerializableMapper.Companion.stringToBoolean(
    trueKeys: NonEmptyList<String> = StringToBooleanSerializableMapper.defaultTrueKeys,
    falseKey: String = StringToBooleanSerializableMapper.defaultFalseKey,
    ignoreCase: Boolean = StringToBooleanSerializableMapper.defaultIgnoreCase,
): StringToBooleanSerializableMapper = StringToBooleanSerializableMapper(
    trueKeys = trueKeys,
    falseKey = falseKey,
    ignoreCase = ignoreCase,
)