plugins {
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        jvmMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:init:api"))
        }
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:connected:api"))
            implementation(project(":ktiot:client:model:connected:impl"))
            implementation(project(":ktiot:client:model:element:api"))
            implementation(project(":ktiot:client:model:element:impl"))
            implementation(project(":ktiot:client:model:init:api"))
            implementation(project(":ktiot:client:model:init:impl"))
            implementation(project(":ktiot:client:model:initialized:api"))
            implementation(project(":ktiot:client:model:initialized:impl"))
            implementation(project(":ktiot:client:model:logged:api"))
            implementation(project(":ktiot:client:model:logged:impl"))
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:client:model:mainstack:impl"))
            implementation(project(":ktiot:client:model:property:api"))
            implementation(project(":ktiot:client:model:property:impl"))
            implementation(project(":ktiot:client:model:scheme:api"))
            implementation(project(":ktiot:client:model:scheme:impl"))
            implementation(project(":ktiot:client:model:settings:api"))
            implementation(project(":ktiot:client:model:settings:impl"))
        }
    }
}
