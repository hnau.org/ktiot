package hnau.ktiot.client.app

import hnau.common.app.storage.Storage

actual fun KtIotApp.Dependencies.Companion.commonImpl(
    storageFactory: Storage.Factory,
): KtIotApp.Dependencies = KtIotApp.Dependencies.impl(
    storageFactory = storageFactory,
)