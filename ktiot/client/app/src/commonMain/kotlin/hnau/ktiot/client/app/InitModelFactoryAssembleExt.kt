package hnau.ktiot.client.app

import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.connected.impl.impl
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.element.impl.impl
import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.model.init.impl.impl
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.initialized.impl.impl
import hnau.ktiot.client.model.logged.api.LoggedModel
import hnau.ktiot.client.model.logged.impl.impl
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.model.mainstack.impl.impl
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.model.property.impl.impl
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.scheme.impl.impl
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.client.model.settings.impl.impl

internal fun InitModel.Factory.Companion.assemble(): InitModel.Factory {
    val settings = SettingsModel.Factory.impl()
    val property = PropertyModel.Factory.impl()
    val element = ElementModel.Factory.impl(
        propertyModelFactory = property,
    )
    val scheme = SchemeModel.Factory.impl(
        elementModelFactory = element,
    )
    val mainStack = MainStackModel.Factory.impl(
        settingsModelFactory = settings,
        schemeModelFactory = scheme,
    )
    val connected = ConnectedModel.Factory.impl(
        mainStackModelFactory = mainStack,
    )
    val logged = LoggedModel.Factory.impl(
        connectedModelFactory = connected,
    )
    val initialized = InitializedModel.Factory.impl(
        loggedModelFactory = logged,
    )
    val init = InitModel.Factory.impl(
        initializedModelFactory = initialized,
    )
    return init
}