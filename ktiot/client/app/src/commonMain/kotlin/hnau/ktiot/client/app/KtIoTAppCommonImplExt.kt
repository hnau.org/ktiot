package hnau.ktiot.client.app

import hnau.common.app.storage.Storage

expect fun KtIotApp.Dependencies.Companion.commonImpl(
    storageFactory: Storage.Factory,
): KtIotApp.Dependencies