package hnau.ktiot.client.app

@JvmInline
value class SavedState(
    val savedState: String?,
)
