plugins {
    alias(libs.plugins.compose.desktop)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        jvmMain.dependencies {
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:init:api"))
        }
        commonMain.dependencies {
            implementation(compose.materialIconsExtended)
            implementation(project(":common:app"))
            implementation(project(":common:color"))
            implementation(project(":ktiot:client:app"))
            implementation(project(":ktiot:client:model:init:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:connected:api"))
            implementation(project(":ktiot:client:projector:connected:impl"))
            implementation(project(":ktiot:client:projector:element:api"))
            implementation(project(":ktiot:client:projector:element:impl"))
            implementation(project(":ktiot:client:projector:init:api"))
            implementation(project(":ktiot:client:projector:init:impl"))
            implementation(project(":ktiot:client:projector:initialized:api"))
            implementation(project(":ktiot:client:projector:initialized:impl"))
            implementation(project(":ktiot:client:projector:logged:api"))
            implementation(project(":ktiot:client:projector:logged:impl"))
            implementation(project(":ktiot:client:projector:mainstack:api"))
            implementation(project(":ktiot:client:projector:mainstack:impl"))
            implementation(project(":ktiot:client:projector:property:api"))
            implementation(project(":ktiot:client:projector:property:impl"))
            implementation(project(":ktiot:client:projector:scheme:api"))
            implementation(project(":ktiot:client:projector:scheme:impl"))
            implementation(project(":ktiot:client:projector:settings:api"))
            implementation(project(":ktiot:client:projector:settings:impl"))
        }
    }
}
