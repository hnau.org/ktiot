package hnau.ktiot.client.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.kotlin.remindSupertype
import hnau.common.kotlin.remindType
import hnau.ktiot.client.app.KtIotApp
import hnau.ktiot.client.compose.utils.KtIotTheme
import hnau.ktiot.client.compose.utils.LocalizerImpl
import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.init.api.InitProjector
import hnau.ktiot.client.projector.init.api.commonImpl

@Composable
fun KtIotApp.Content(
    localizer: Localizer = LocalizerImpl,
) {
    val projectorScope = rememberCoroutineScope()
    val projector = remember {
        InitProjector
            .Factory
            .assemble()
            .createInitProjector(
                scope = projectorScope,
                dependencies = InitProjector.Dependencies.commonImpl(
                    localizer = localizer,
                ),
                model = model
                    .remindType<InitModel>()
                    .remindSupertype<_, GoBackHandlerProvider>(),
            )
    }
    KtIotTheme {
        projector.Content()
    }
}
