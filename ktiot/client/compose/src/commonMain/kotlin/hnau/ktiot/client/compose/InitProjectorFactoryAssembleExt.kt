package hnau.ktiot.client.compose

import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.ktiot.client.projector.connected.impl.impl
import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.ktiot.client.projector.element.impl.impl
import hnau.ktiot.client.projector.init.api.InitProjector
import hnau.ktiot.client.projector.init.impl.impl
import hnau.ktiot.client.projector.initialized.api.InitializedProjector
import hnau.ktiot.client.projector.initialized.impl.impl
import hnau.ktiot.client.projector.logged.api.LoggedProjector
import hnau.ktiot.client.projector.logged.impl.impl
import hnau.ktiot.client.projector.mainstack.api.MainStackProjector
import hnau.ktiot.client.projector.mainstack.impl.impl
import hnau.ktiot.client.projector.property.api.PropertyProjector
import hnau.ktiot.client.projector.property.impl.impl
import hnau.ktiot.client.projector.scheme.api.SchemeProjector
import hnau.ktiot.client.projector.scheme.impl.impl
import hnau.ktiot.client.projector.settings.api.SettingsProjector
import hnau.ktiot.client.projector.settings.impl.impl

internal fun InitProjector.Factory.Companion.assemble(): InitProjector.Factory {
    val settings = SettingsProjector.Factory.impl()
    val property = PropertyProjector.Factory.impl()
    val element = ElementProjector.Factory.impl(
        propertyProjectorFactory = property,
    )
    val scheme = SchemeProjector.Factory.impl(
        elementProjectorFactory = element,
    )
    val mainStack = MainStackProjector.Factory.impl(
        settingsProjectorFactory = settings,
        schemeProjectorFactory = scheme,
    )
    val connected = ConnectedProjector.Factory.impl(
        mainStackProjectorFactory = mainStack,
    )
    val logged = LoggedProjector.Factory.impl(
        connectedProjectorFactory = connected,
    )
    val initialized = InitializedProjector.Factory.impl(
        loggedProjectorFactory = logged,
    )
    val init = InitProjector.Factory.impl(
        initializedProjectorFactory = initialized,
    )
    return init
}