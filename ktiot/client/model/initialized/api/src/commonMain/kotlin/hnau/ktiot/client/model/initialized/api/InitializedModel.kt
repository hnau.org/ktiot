package hnau.ktiot.client.model.initialized.api

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.app.storage.Storage
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.logged.api.LoggedModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface InitializedModel : GoBackHandlerProvider {

    sealed interface State : GoBackHandlerProvider {

        data class Login(
            val login: LoginModel,
        ) : State, GoBackHandlerProvider by login

        data class Logged(
            val logged: LoggedModel,
        ) : State, GoBackHandlerProvider by logged

        @Serializable
        sealed interface Skeleton {

            @Serializable
            @SerialName("login")
            data class Login(
                val login: LoginModel.Skeleton = LoginModel.Skeleton(),
            ) : Skeleton

            @Serializable
            @SerialName("logged")
            data class Logged(
                val logged: LoggedModel.Skeleton = LoggedModel.Skeleton(),
            ) : Skeleton
        }
    }

    @Serializable
    data class Skeleton(
        @Serializable(OptionSerializer::class)
        var state: Option<State.Skeleton> = None,
    )

    @Shuffle
    interface Dependencies {

        val storage: Storage

        fun login(): LoginModel.Dependencies

        fun logged(
            doLogout: DoLogout,
        ): LoggedModel.Dependencies
    }

    val state: StateFlow<State>

    fun interface Factory {

        fun createInitializedModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
        ): InitializedModel

        companion object
    }
}
