package hnau.ktiot.client.model.initialized.api

import hnau.common.app.EditingString
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.app.storage.Storage
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface LoginModel : GoBackHandlerProvider {

    @Serializable
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val address: MutableStateFlow<EditingString> = MutableStateFlow(EditingString.empty),

        @Serializable(MutableStateFlowSerializer::class)
        val port: MutableStateFlow<EditingString> = MutableStateFlow(EditingString.empty),

        @Serializable(MutableStateFlowSerializer::class)
        val clientId: MutableStateFlow<EditingString> = MutableStateFlow(EditingString.empty),
    )

    @Shuffle
    interface Dependencies {

        val storage: Storage
    }

    enum class Message {
        PortIsIncorrect,
    }

    val messages: Flow<Message>

    val address: MutableStateFlow<EditingString>

    val port: MutableStateFlow<EditingString>

    val clientId: MutableStateFlow<EditingString>

    val loginOrNull: StateFlow<(() -> Unit)?>
}
