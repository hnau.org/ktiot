plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:initialized:api"))
            implementation(project(":ktiot:client:model:logged:api"))
        }
    }
}
