package hnau.ktiot.client.model.initialized.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.app.goback.stateGoBackHandler
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.entry
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.map
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.option
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.common.MqttConnectionInfo
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.logged.api.LoggedModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class InitializedModelImpl(
    scope: CoroutineScope,
    private val skeleton: InitializedModel.Skeleton,
    private val dependencies: InitializedModel.Dependencies,
    private val loggedModelFactory: LoggedModel.Factory,
) : InitializedModel {

    private val connectionInfo: StorageEntry<MqttConnectionInfo?> = dependencies.storage.entry(
        key = "connection_info",
        defaultValue = null,
        mapper = MqttConnectionInfo.optionalStringMapper,
    )

    private val doLogout: DoLogout = DoLogout { connectionInfo.updateValue(null) }

    override val state: StateFlow<InitializedModel.State> = connectionInfo
        .value
        .scopedInState(scope)
        .mapState(scope) { (stateScope, currentCredentials) ->
            when (currentCredentials) {
                null -> InitializedModel.State.Login(
                    LoginModelImpl.create(
                        scope = stateScope,
                        skeletonOrNone = skeleton::state
                            .toAccessor()
                            .shrinkType<_, InitializedModel.State.Skeleton.Login>()
                            .map(
                                mapper = Mapper.option(
                                    Mapper(
                                        direct = InitializedModel.State.Skeleton.Login::login,
                                        reverse = InitializedModel.State.Skeleton::Login,
                                    )
                                )
                            ),
                        dependencies = dependencies.login(),
                        login = connectionInfo::updateValue,
                    )
                )

                else -> InitializedModel.State.Logged(
                    loggedModelFactory.createLoggedModel(
                        scope = stateScope,
                        skeleton = skeleton::state
                            .toAccessor()
                            .shrinkType<_, InitializedModel.State.Skeleton.Logged>()
                            .getOrInit { InitializedModel.State.Skeleton.Logged() }
                            .logged,
                        dependencies = dependencies.logged(
                            doLogout = doLogout,
                        ),
                        credentials = currentCredentials,
                    )
                )
            }
        }

    override val goBackHandler: GoBackHandler = state.stateGoBackHandler(scope)
}
