package hnau.ktiot.client.model.initialized.impl

import arrow.core.Option
import hnau.common.app.EditingString
import hnau.common.app.messages.SharedMessagesHolder
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.entry
import hnau.common.app.toEditingString
import hnau.common.kotlin.MutableAccessor
import hnau.common.kotlin.coroutines.actionOrNullIfExecuting
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.ifNull
import hnau.ktiot.client.model.common.MqttConnectionInfo
import hnau.ktiot.client.model.initialized.api.LoginModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

internal class LoginModelImpl private constructor(
    scope: CoroutineScope,
    private val skeleton: LoginModel.Skeleton,
    private val cachedConnectionInfo: StorageEntry<MqttConnectionInfo?>,
    private val login: suspend (MqttConnectionInfo) -> Unit,
) : LoginModel {

    private val messagesHolder = SharedMessagesHolder<LoginModel.Message>()

    override val messages: Flow<LoginModel.Message>
        get() = messagesHolder.messages

    override val address: MutableStateFlow<EditingString>
        get() = skeleton.address

    override val port: MutableStateFlow<EditingString>
        get() = skeleton.port

    override val clientId: MutableStateFlow<EditingString>
        get() = skeleton.clientId

    override val loginOrNull: StateFlow<(() -> Unit)?> = actionOrNullIfExecuting(scope) {

        val address = address.value.text
            .takeIf { it.isNotEmpty() }
            ?: MqttConnectionInfo.defaultAddress

        val port = port.value.text
            .takeIf { it.isNotEmpty() }
            ?.let { portString ->
                portString.toIntOrNull().ifNull {
                    messagesHolder.sendMessage(LoginModel.Message.PortIsIncorrect)
                    return@actionOrNullIfExecuting
                }
            }
            ?: MqttConnectionInfo.defaultPort

        val clientId = clientId.value.text
            .takeIf { it.isNotEmpty() }
            ?: MqttConnectionInfo.generateRandomClientId()

        val connectionInfo = MqttConnectionInfo(
            address = address,
            port = port,
            clientId = clientId,
        )

        cachedConnectionInfo.updateValue(connectionInfo)
        login(connectionInfo)
    }

    companion object {

        fun create(
            scope: CoroutineScope,
            skeletonOrNone: MutableAccessor<Option<LoginModel.Skeleton>>,
            dependencies: LoginModel.Dependencies,
            login: suspend (MqttConnectionInfo) -> Unit,
        ): LoginModelImpl {
            val cachedConnectionInfo: StorageEntry<MqttConnectionInfo?> =
                dependencies.storage.entry(
                    key = "cached_connection_info",
                    defaultValue = null,
                    mapper = MqttConnectionInfo.optionalStringMapper,
                )
            val skeleton = skeletonOrNone.getOrInit {
                val cached = cachedConnectionInfo.value.value
                LoginModel.Skeleton(
                    address = MutableStateFlow(
                        cached
                            ?.address
                            ?.takeIf { it != MqttConnectionInfo.defaultAddress }
                            .orEmpty()
                            .toEditingString(),
                    ),
                    port = MutableStateFlow(
                        cached
                            ?.port
                            ?.takeIf { it != MqttConnectionInfo.defaultPort }
                            ?.toString()
                            .orEmpty()
                            .toEditingString(),
                    ),
                    clientId = MutableStateFlow(
                        cached
                            ?.clientId
                            .orEmpty()
                            .toEditingString(),
                    )
                )
            }
            return LoginModelImpl(
                scope = scope,
                cachedConnectionInfo = cachedConnectionInfo,
                login = login,
                skeleton = skeleton,
            )
        }
    }
}
