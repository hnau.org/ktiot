package hnau.ktiot.client.model.initialized.impl

import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.logged.api.LoggedModel

fun InitializedModel.Factory.Companion.impl(
    loggedModelFactory: LoggedModel.Factory,
): InitializedModel.Factory = InitializedModel.Factory { scope, skeleton, dependencies ->
    InitializedModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        loggedModelFactory = loggedModelFactory,
    )
}