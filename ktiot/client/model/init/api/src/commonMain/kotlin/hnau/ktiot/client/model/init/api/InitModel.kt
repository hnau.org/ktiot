package hnau.ktiot.client.model.init.api

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.app.storage.Storage
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.settings.item.SettingsAllowEditCalculatedProperties
import hnau.ktiot.client.model.settings.item.SettingsAllowEditHardwareProperties
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface InitModel : GoBackHandlerProvider {

    @Serializable
    data class Skeleton(
        @Serializable(OptionSerializer::class)
        var loggable: Option<InitializedModel.Skeleton> = None,
    )

    @Shuffle
    interface Dependencies {

        val storageFactory: Storage.Factory

        fun initialized(
            storage: Storage,
            allowEditHardwareProperties: SettingsAllowEditHardwareProperties,
            allowEditCalculatedProperties: SettingsAllowEditCalculatedProperties,
        ): InitializedModel.Dependencies
    }

    val initialized: StateFlow<InitializedModel?>

    fun interface Factory {

        fun createInitModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
        ): InitModel

        companion object
    }
}
