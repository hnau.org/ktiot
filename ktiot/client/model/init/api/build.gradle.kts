plugins {
    alias(libs.plugins.ksp)
    alias(libs.plugins.kotlin.serialization)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:initialized:api"))
            implementation(project(":ktiot:client:model:settings:item"))
        }
    }
}
