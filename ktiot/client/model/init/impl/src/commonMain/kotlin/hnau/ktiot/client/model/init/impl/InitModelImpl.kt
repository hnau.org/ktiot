package hnau.ktiot.client.model.init.impl

import hnau.common.app.goback.NeverGoBackHandler
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.settings.item.settingsAllowEditCalculatedProperties
import hnau.ktiot.client.model.settings.item.settingsAllowEditHardwareProperties
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn

internal class InitModelImpl(
    scope: CoroutineScope,
    private val skeleton: InitModel.Skeleton,
    private val dependencies: InitModel.Dependencies,
    private val initializedModelFactory: InitializedModel.Factory,
) : InitModel {

    override val initialized: StateFlow<InitializedModel?> = flow {
        val storage = dependencies.storageFactory.createStorage()
        emit(storage)
    }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = null,
        )
        .scopedInState(scope)
        .mapState(scope) { (storageScope, storageOrNull) ->
            storageOrNull?.let { storage ->
                initializedModelFactory.createInitializedModel(
                    scope = storageScope,
                    skeleton = skeleton::loggable
                        .toAccessor()
                        .getOrInit { InitializedModel.Skeleton() },
                    dependencies = dependencies.initialized(
                        storage = storage,
                        allowEditCalculatedProperties = storage.settingsAllowEditCalculatedProperties,
                        allowEditHardwareProperties = storage.settingsAllowEditHardwareProperties,
                    ),
                )
            }
        }

    override val goBackHandler: StateFlow<(() -> Unit)?> =
        initialized.flatMapState(scope) { initialized ->
            initialized
                ?.goBackHandler
                ?: NeverGoBackHandler
        }
}
