package hnau.ktiot.client.model.init.impl

import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.model.initialized.api.InitializedModel

fun InitModel.Factory.Companion.impl(
    initializedModelFactory: InitializedModel.Factory,
): InitModel.Factory = InitModel.Factory { scope, skeleton, dependencies ->
    InitModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        initializedModelFactory = initializedModelFactory,
    )
}