package hnau.ktiot.client.model.settings.api

import hnau.common.app.ListScrollState
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.DoLogout
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface SettingsModel : GoBackHandlerProvider {

    @Shuffle
    interface Dependencies {
        val doLogout: DoLogout
    }

    @Serializable
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val scrollState: MutableStateFlow<ListScrollState> =
            MutableStateFlow(ListScrollState.initial),
    )

    val logout: StateFlow<(() -> Unit)?>

    val scrollState: MutableStateFlow<ListScrollState>

    fun interface Factory {

        fun createSettingsModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
        ): SettingsModel

        companion object
    }
}