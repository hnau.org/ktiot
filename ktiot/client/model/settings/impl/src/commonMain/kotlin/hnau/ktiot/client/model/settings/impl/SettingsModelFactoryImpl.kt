package hnau.ktiot.client.model.settings.impl

import hnau.ktiot.client.model.settings.api.SettingsModel

fun SettingsModel.Factory.Companion.impl(): SettingsModel.Factory =
    SettingsModel.Factory(::SettingsModelImpl)