package hnau.ktiot.client.model.settings.impl

import hnau.common.app.ListScrollState
import hnau.common.kotlin.coroutines.actionOrNullIfExecuting
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.settings.api.SettingsModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

internal class SettingsModelImpl(
    scope: CoroutineScope,
    private val skeleton: SettingsModel.Skeleton,
    dependencies: SettingsModel.Dependencies,
) : SettingsModel {

    override val logout: StateFlow<(() -> Unit)?> = actionOrNullIfExecuting(
        scope = scope,
        action = dependencies.doLogout.remindType<DoLogout>()::logout,
    )

    override val scrollState: MutableStateFlow<ListScrollState>
        get() = skeleton.scrollState
}