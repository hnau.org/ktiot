package hnau.ktiot.client.model.settings.item

import hnau.common.app.storage.Storage
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.float

interface SettingsUIScale {

    val value: StorageEntry<Float>
}

val Storage.settingsUIScale: SettingsUIScale
    get() = object : SettingsUIScale {

        override val value: StorageEntry<Float> = float(
            key = "settings_system_or_light_or_dark_theme",
            defaultValue = 1f,
        )
    }