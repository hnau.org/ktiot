package hnau.ktiot.client.model.settings.item

import hnau.common.app.storage.Storage
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.boolean

interface SettingsAllowEditHardwareProperties {

    val value: StorageEntry<Boolean>
}

val Storage.settingsAllowEditHardwareProperties: SettingsAllowEditHardwareProperties
    get() = object : SettingsAllowEditHardwareProperties {

        override val value: StorageEntry<Boolean> = boolean(
            key = "settings_allow_edit_hardware_properties",
            defaultValue = false,
        )
    }