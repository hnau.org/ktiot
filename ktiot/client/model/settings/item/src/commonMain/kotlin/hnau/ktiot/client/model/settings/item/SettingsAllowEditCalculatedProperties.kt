package hnau.ktiot.client.model.settings.item

import hnau.common.app.storage.Storage
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.boolean

interface SettingsAllowEditCalculatedProperties {

    val value: StorageEntry<Boolean>
}

val Storage.settingsAllowEditCalculatedProperties: SettingsAllowEditCalculatedProperties
    get() = object : SettingsAllowEditCalculatedProperties {

        override val value: StorageEntry<Boolean> = boolean(
            key = "settings_allow_edit_calculated_properties",
            defaultValue = false,
        )
    }