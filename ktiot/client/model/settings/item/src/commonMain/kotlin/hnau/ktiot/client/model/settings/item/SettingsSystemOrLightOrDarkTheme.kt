package hnau.ktiot.client.model.settings.item

import hnau.common.app.storage.Storage
import hnau.common.app.storage.StorageEntry
import hnau.common.app.storage.entry
import hnau.common.kotlin.mapper.toMapper
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

interface SettingsSystemOrLightOrDarkTheme {

    @Serializable
    enum class Value {
        System, Light, Dark;

        companion object {

            val default: Value = System
        }
    }

    val value: StorageEntry<Value>
}

val Storage.settingsSystemOrLightOrDarkTheme: SettingsSystemOrLightOrDarkTheme
    get() = object : SettingsSystemOrLightOrDarkTheme {

        override val value: StorageEntry<SettingsSystemOrLightOrDarkTheme.Value> = entry(
            key = "settings_system_or_light_or_dark_theme",
            mapper = Json.toMapper(SettingsSystemOrLightOrDarkTheme.Value.serializer()),
            defaultValue = SettingsSystemOrLightOrDarkTheme.Value.default,
        )
    }