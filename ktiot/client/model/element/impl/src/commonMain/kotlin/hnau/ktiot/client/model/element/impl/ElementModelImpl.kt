package hnau.ktiot.client.model.element.impl

import hnau.common.app.ListScrollState
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.client.model.common.ClickableElement
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.scheme.property.PropertyInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.json.Json

internal class ElementModelImpl(
    scope: CoroutineScope,
    override val children: List<ClickableElement>,
    skeleton: ElementModel.Skeleton,
    dependencies: ElementModel.Dependencies,
    private val topic: MqttTopic.Absolute,
    properties: List<PropertyInfo<*>>,
    propertyModelFactory: PropertyModel.Factory,
) : ElementModel {

    override val propertiesScrollState: MutableStateFlow<ListScrollState> by skeleton::propertiesScrollState

    override val properties: List<PropertyModel<*>> = properties
        .map { info ->
            propertyModelFactory.createPropertyModel(
                scope = scope,
                skeleton = skeleton.properties.getOrPut(info.key) { PropertyModel.Skeleton() },
                dependencies = dependencies.property(),
                parenTopic = topic,
                info = info,
            )
        }

    override val childrenScrollState: MutableStateFlow<ListScrollState> by skeleton::childrenScrollState

    private val PropertyInfo<*>.key: String
        get() = Json.encodeToString(PropertyInfo.serializer(), this)
}