package hnau.ktiot.client.model.element.impl

import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.property.api.PropertyModel

fun ElementModel.Factory.Companion.impl(
    propertyModelFactory: PropertyModel.Factory,
): ElementModel.Factory =
    ElementModel.Factory { scope, children, skeleton, dependencies, topic, properties ->
        ElementModelImpl(
            scope = scope,
            children = children,
            skeleton = skeleton,
            dependencies = dependencies,
            topic = topic,
            properties = properties,
            propertyModelFactory = propertyModelFactory
        )
    }