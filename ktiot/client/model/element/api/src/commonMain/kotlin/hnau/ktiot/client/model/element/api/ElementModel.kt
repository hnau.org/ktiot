package hnau.ktiot.client.model.element.api

import hnau.common.app.ListScrollState
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.client.model.common.ClickableElement
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.scheme.property.PropertyInfo
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.Serializable

interface ElementModel {

    @Serializable
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val propertiesScrollState: MutableStateFlow<ListScrollState> =
            MutableStateFlow(ListScrollState.initial),

        @Serializable(MutableStateFlowSerializer::class)
        val childrenScrollState: MutableStateFlow<ListScrollState> =
            MutableStateFlow(ListScrollState.initial),

        val properties: HashMap<String, PropertyModel.Skeleton> = HashMap(),
    ) {

        companion object {

            val default: Skeleton = Skeleton(
                propertiesScrollState = MutableStateFlow(ListScrollState.initial),
                childrenScrollState = MutableStateFlow(ListScrollState.initial),
                properties = HashMap(),
            )
        }
    }

    @Shuffle
    interface Dependencies {

        fun property(): PropertyModel.Dependencies
    }

    val propertiesScrollState: MutableStateFlow<ListScrollState>

    val properties: List<PropertyModel<*>>

    val childrenScrollState: MutableStateFlow<ListScrollState>

    val children: List<ClickableElement>

    fun interface Factory {

        fun createElementModel(
            scope: CoroutineScope,
            children: List<ClickableElement>,
            skeleton: Skeleton,
            dependencies: Dependencies,
            topic: MqttTopic.Absolute,
            properties: List<PropertyInfo<*>>,
        ): ElementModel

        companion object
    }
}