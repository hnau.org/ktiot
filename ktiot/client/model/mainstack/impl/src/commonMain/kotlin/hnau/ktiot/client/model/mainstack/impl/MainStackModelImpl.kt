package hnau.ktiot.client.model.mainstack.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.app.goback.fallback
import hnau.common.app.model.stack.NonEmptyStack
import hnau.common.app.model.stack.StackModelElements
import hnau.common.app.model.stack.push
import hnau.common.app.model.stack.stackGoBackHandler
import hnau.common.app.model.stack.tailGoBackHandler
import hnau.ktiot.client.model.common.SettingsOpener
import hnau.ktiot.client.model.mainstack.api.MainStackElementModel
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.scheme.SchemeElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class MainStackModelImpl(
    scope: CoroutineScope,
    private val skeleton: MainStackModel.Skeleton,
    dependencies: MainStackModel.Dependencies,
    private val scheme: SchemeElement,
    private val settingsModelFactory: SettingsModel.Factory,
    private val schemeModelFactory: SchemeModel.Factory,
) : MainStackModel, GoBackHandlerProvider {

    @Suppress("RedundantSamConstructor")
    override val stack: StateFlow<NonEmptyStack<MainStackElementModel>> = run {
        val stack = skeleton.stack
        val elementDependencies = dependencies.element(
            settingsOpener = SettingsOpener { stack.push(MainStackElementModel.Skeleton.Settings()) },
        )
        StackModelElements(
            scope = scope,
            skeletonsStack = stack,
        ) { modelScope, skeleton ->
            createModel(
                modelScope = modelScope,
                skeleton = skeleton,
                elementDependencies = elementDependencies,
            )
        }
    }

    private fun createModel(
        modelScope: CoroutineScope,
        skeleton: MainStackElementModel.Skeleton,
        elementDependencies: MainStackModel.Dependencies.Element,
    ): MainStackElementModel = when (skeleton) {
        is MainStackElementModel.Skeleton.Scheme -> MainStackElementModel.Scheme(
            schemeModelFactory.createSchemeModel(
                scope = modelScope,
                skeleton = skeleton.scheme,
                dependencies = elementDependencies.scheme(),
                scheme = scheme,
            )
        )

        is MainStackElementModel.Skeleton.Settings -> MainStackElementModel.Settings(
            settingsModelFactory.createSettingsModel(
                scope = modelScope,
                skeleton = skeleton.settings,
                dependencies = elementDependencies.settings(),
            )
        )
    }

    override val goBackHandler: GoBackHandler = stack
        .tailGoBackHandler(scope)
        .fallback(
            scope = scope,
            fallback = skeleton.stack.stackGoBackHandler(scope),
        )
}