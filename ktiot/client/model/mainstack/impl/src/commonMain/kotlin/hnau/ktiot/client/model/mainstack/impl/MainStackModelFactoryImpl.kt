package hnau.ktiot.client.model.mainstack.impl

import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.settings.api.SettingsModel

fun MainStackModel.Factory.Companion.impl(
    settingsModelFactory: SettingsModel.Factory,
    schemeModelFactory: SchemeModel.Factory,
): MainStackModel.Factory = MainStackModel.Factory { scope, skeleton, dependencies, scheme ->
    MainStackModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        scheme = scheme,
        settingsModelFactory = settingsModelFactory,
        schemeModelFactory = schemeModelFactory,
    )
}