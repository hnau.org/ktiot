plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:client:model:scheme:api"))
            implementation(project(":ktiot:client:model:settings:api"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
