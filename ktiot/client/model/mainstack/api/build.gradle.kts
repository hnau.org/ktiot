plugins {
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:scheme:api"))
            implementation(project(":ktiot:client:model:settings:api"))
            implementation(project(":ktiot:client:model:settings:item"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
