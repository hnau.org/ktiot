package hnau.ktiot.client.model.mainstack.api

import hnau.common.app.goback.GoBackHandlerProvider
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.settings.api.SettingsModel
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed interface MainStackElementModel : GoBackHandlerProvider {

    data class Settings(
        val settings: SettingsModel,
    ) : MainStackElementModel, GoBackHandlerProvider by settings

    data class Scheme(
        val scheme: SchemeModel,
    ) : MainStackElementModel, GoBackHandlerProvider by scheme

    @Serializable
    sealed interface Skeleton {

        @Serializable
        @SerialName("settings")
        data class Settings(
            val settings: SettingsModel.Skeleton = SettingsModel.Skeleton(),
        ) : Skeleton

        @Serializable
        @SerialName("scheme")
        data class Scheme(
            val scheme: SchemeModel.Skeleton = SchemeModel.Skeleton(),
        ) : Skeleton
    }
}
