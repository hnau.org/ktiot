package hnau.ktiot.client.model.mainstack.api

import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.app.model.stack.NonEmptyStack
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.SettingsOpener
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.scheme.SchemeElement
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface MainStackModel : GoBackHandlerProvider {

    @Serializable
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val stack: MutableStateFlow<NonEmptyStack<MainStackElementModel.Skeleton>> =
            MutableStateFlow(NonEmptyStack(MainStackElementModel.Skeleton.Scheme())),
    )

    @Shuffle
    interface Dependencies {

        fun element(
            settingsOpener: SettingsOpener,
        ): Element

        @Shuffle
        interface Element {

            fun scheme(): SchemeModel.Dependencies

            fun settings(): SettingsModel.Dependencies
        }
    }

    val stack: StateFlow<NonEmptyStack<MainStackElementModel>>

    fun interface Factory {

        fun createMainStackModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
            scheme: SchemeElement,
        ): MainStackModel

        companion object
    }
}