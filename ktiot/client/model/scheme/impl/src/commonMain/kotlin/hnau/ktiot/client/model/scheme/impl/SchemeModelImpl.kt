package hnau.ktiot.client.model.scheme.impl

import arrow.core.toNonEmptyListOrNull
import hnau.common.app.ListScrollState
import hnau.common.app.goback.GoBackHandler
import hnau.common.kotlin.coroutines.createChild
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.runningFoldState
import hnau.common.mqtt.topic.MqttTopic
import hnau.common.mqtt.topic.MqttTopicParts
import hnau.ktiot.client.model.common.ClickableElement
import hnau.ktiot.client.model.common.NowProvider
import hnau.ktiot.client.model.common.orTitle
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.scheme.api.SchemeModelStack
import hnau.ktiot.scheme.SchemeElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

internal class SchemeModelImpl(
    private val scope: CoroutineScope,
    private val skeleton: SchemeModel.Skeleton,
    private val dependencies: SchemeModel.Dependencies,
    private val scheme: SchemeElement,
    private val elementModelFactory: ElementModel.Factory,
) : SchemeModel {

    override fun openSettings() = dependencies.settingsOpener.openSettings()


    override val goBackHandler: GoBackHandler = skeleton
        .elementsStack
        .mapState(scope) { stack ->
            val items = stack.items.toNonEmptyListOrNull() ?: return@mapState null
            return@mapState {
                skeleton.elementsStack.value = stack.copy(
                    items = items.dropLast(1),
                )
            }
        }

    private data class Path(
        val parts: List<MqttTopic.Relative>,
    ) {

        val topic: MqttTopic.Relative = parts.fold(
            initial = emptyPath,
        ) { acc, part ->
            acc + part
        }

        operator fun plus(
            topic: MqttTopic.Relative,
        ): Path = copy(
            parts = parts + topic,
        )

        companion object {

            val empty = Path(emptyList())
        }
    }

    private val withNowProviderDependencies: SchemeModel.Dependencies.WithNowProvider =
        dependencies.withNowProvider(
            nowProvider = NowProvider(
                scope = scope,
            )
        )

    override val stackTitlesScrollState: MutableStateFlow<ListScrollState>
            by skeleton::stackTitlesScrollState

    private data class CancellableModel(
        val skeleton: ElementModel.Skeleton,
        val model: ElementModel,
        val topic: MqttTopic,
        val cancel: () -> Unit,
    )

    override val stack: StateFlow<SchemeModelStack<ElementModel>> = skeleton
        .elementsStack
        .runningFoldState(
            scope = scope,
            createInitial = { skeletonsStack ->
                skeletonsStack
                    .toModelsStack()
            },
        ) { previousModelsStack, newStack ->
            val previousModels = previousModelsStack
                .tail
                .map { it.value }
                .plus(previousModelsStack.head)
                .associateBy { cancellableModel -> cancellableModel.topic }
                .toMutableMap()
            val result = newStack.toModelsStack { previousModels.remove(it) }
            previousModels.forEach { (_, cancellableModel) ->
                cancellableModel.cancel()
            }
            result
        }
        .mapState(scope) { cancellableModelsStack ->
            cancellableModelsStack.map { cancellableModel -> cancellableModel.model }
        }

    private inline fun ElementModel.Skeleton.toModel(
        path: Path,
        element: SchemeElement,
        crossinline getCachedModel: (MqttTopic) -> CancellableModel?,
    ): CancellableModel {
        val topic = hnau.ktiot.scheme.SchemeConstants.dataElementTopic + path.topic
        return getCachedModel(topic) ?: run {
            val elementModelScope = scope.createChild()
            CancellableModel(
                skeleton = this,
                cancel = { elementModelScope.cancel() },
                topic = topic,
                model = elementModelFactory.createElementModel(
                    scope = elementModelScope,
                    skeleton = this,
                    dependencies = withNowProviderDependencies.element(),
                    topic = topic,
                    properties = element.properties,
                    children = element.children.map { (key, child) ->
                        ClickableElement(
                            keyOrTitle = key.orTitle(child.title),
                            onClick = { navigateToTopic(path + key) },
                        )
                    },
                ),
            )
        }
    }

    private inline fun SchemeModel.Skeleton.ElementsStack.toModelsStack(
        crossinline getCachedModel: (MqttTopic) -> CancellableModel? = { null },
    ): SchemeModelStack<CancellableModel> = SchemeModelStack(
        head = rootElementSkeleton.toModel(
            path = Path.empty,
            element = scheme,
            getCachedModel = getCachedModel,
        ),
        tail = items
            .fold(
                initial = Triple(
                    emptyList<SchemeModelStack.Item<CancellableModel>>(),
                    scheme,
                    Path.empty,
                ),
            ) { acc, item ->
                val (collectedItems, parentElement, parentTopicSuffix) = acc
                val key = item.key
                val topicPath = parentTopicSuffix + key
                val element = parentElement.children[key] ?: return@fold acc
                Triple(
                    collectedItems + SchemeModelStack.Item(
                        value = item.elementSkeleton.toModel(
                            path = topicPath,
                            element = element,
                            getCachedModel = getCachedModel,
                        ),
                        keyOrTitle = key.orTitle(element.title),
                        onPreviousClick = { navigateToTopic(parentTopicSuffix) },
                    ),
                    element,
                    topicPath,
                )
            }
            .first,
    )

    private fun navigateToTopic(
        path: Path,
    ) {
        skeleton.elementsStack.update { previousStack ->
            val reusableSkeletons = previousStack
                .items
                .runningFold(
                    initial = previousStack.rootElementSkeleton to Path.empty,
                ) { (_, parentTopicSuffix), (key, skeleton) ->
                    skeleton to parentTopicSuffix + key
                }
                .associate { (skeleton, topicSuffix) ->
                    topicSuffix to skeleton
                }

            fun getSkeleton(
                path: Path,
            ): ElementModel.Skeleton = reusableSkeletons[path]
                ?: ElementModel.Skeleton.default

            val rootSkeleton = getSkeleton(Path.empty)
            SchemeModel.Skeleton.ElementsStack(
                rootElementSkeleton = rootSkeleton,
                items = path
                    .parts
                    .fold(
                        initial = emptyList<SchemeModel.Skeleton.ElementsStack.Item>() to Path.empty,
                    ) { (collected, parentTopicSuffix), key ->
                        val elementTopicSuffix = parentTopicSuffix + key
                        val skeletons = collected + SchemeModel.Skeleton.ElementsStack.Item(
                            key = key,
                            elementSkeleton = getSkeleton(elementTopicSuffix),
                        )
                        skeletons to elementTopicSuffix
                    }
                    .first,
            )
        }
    }

    companion object {

        private val emptyPath = MqttTopic.Relative(MqttTopicParts())
    }
}