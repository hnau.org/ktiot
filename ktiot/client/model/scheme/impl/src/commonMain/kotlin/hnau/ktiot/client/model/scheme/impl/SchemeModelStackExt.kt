package hnau.ktiot.client.model.scheme.impl

import hnau.ktiot.client.model.scheme.api.SchemeModelStack

internal inline fun <I, O> SchemeModelStack<I>.map(
    transform: (I) -> O,
): SchemeModelStack<O> = SchemeModelStack(
    head = transform(head),
    tail = tail.map { item -> item.map(transform) },
)

internal inline fun <I, O> SchemeModelStack.Item<I>.map(
    transform: (I) -> O,
): SchemeModelStack.Item<O> = SchemeModelStack.Item(
    value = transform(value),
    keyOrTitle = keyOrTitle,
    onPreviousClick = onPreviousClick,
)
