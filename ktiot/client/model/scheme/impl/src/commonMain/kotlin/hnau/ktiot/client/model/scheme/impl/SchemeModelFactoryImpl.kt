package hnau.ktiot.client.model.scheme.impl

import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.scheme.api.SchemeModel

fun SchemeModel.Factory.Companion.impl(
    elementModelFactory: ElementModel.Factory,
): SchemeModel.Factory = SchemeModel.Factory { scope, skeleton, dependencies, scheme ->
    SchemeModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        scheme = scheme,
        elementModelFactory = elementModelFactory,
    )
}