package hnau.ktiot.client.model.scheme.api

import hnau.common.app.ListScrollState
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.client.model.common.NowProvider
import hnau.ktiot.client.model.common.SettingsOpener
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.scheme.SchemeElement
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface SchemeModel : GoBackHandlerProvider {

    @Serializable
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val elementsStack: MutableStateFlow<ElementsStack> =
            MutableStateFlow(ElementsStack.default),

        @Serializable(MutableStateFlowSerializer::class)
        val stackTitlesScrollState: MutableStateFlow<ListScrollState> =
            MutableStateFlow(ListScrollState.initial),

        ) {

        @Serializable
        data class ElementsStack(
            val rootElementSkeleton: ElementModel.Skeleton,
            val items: List<Item>,
        ) {

            @Serializable
            data class Item(
                val key: MqttTopic.Relative,
                val elementSkeleton: ElementModel.Skeleton,
            )

            companion object {

                val default = ElementsStack(
                    rootElementSkeleton = ElementModel.Skeleton.default,
                    items = emptyList(),
                )
            }
        }
    }

    @Shuffle
    interface Dependencies {

        val settingsOpener: SettingsOpener

        @Shuffle
        interface WithNowProvider {

            fun element(): ElementModel.Dependencies
        }

        fun withNowProvider(
            nowProvider: NowProvider,
        ): WithNowProvider
    }

    val stackTitlesScrollState: MutableStateFlow<ListScrollState>

    val stack: StateFlow<SchemeModelStack<ElementModel>>

    fun openSettings()

    fun interface Factory {

        fun createSchemeModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
            scheme: SchemeElement,
        ): SchemeModel

        companion object
    }
}