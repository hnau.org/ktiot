package hnau.ktiot.client.model.scheme.api

import arrow.core.Either
import hnau.common.mqtt.topic.MqttTopic

data class SchemeModelStack<T>(
    val head: T,
    val tail: List<Item<T>>,
) {

    data class Item<T>(
        val value: T,
        val keyOrTitle: Either<MqttTopic, String>,
        val onPreviousClick: () -> Unit,
    )
}
