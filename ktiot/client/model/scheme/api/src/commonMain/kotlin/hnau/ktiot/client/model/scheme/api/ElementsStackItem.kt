package hnau.ktiot.client.model.scheme.api

import hnau.common.mqtt.topic.MqttTopic
import kotlinx.serialization.Serializable

@Serializable
data class ElementsStackItem<T>(
    val value: T,
    val next: Next<T>? = null,
) {

    @Serializable
    data class Next<T>(
        val part: MqttTopic.Relative,
        val item: ElementsStackItem<T>,
    ) {

        companion object
    }

    companion object
}

internal fun <T> ElementsStackItem<T>.rebuild(
    parts: List<MqttTopic.Relative>,
    createNew: (MqttTopic.Relative) -> T,
): ElementsStackItem<T> = copy(
    next = parts
        .firstOrNull()
        ?.let { head ->
            val tail = parts.drop(1)
            next
                ?.takeIf { it.part == head }
                ?.let { next ->
                    next.copy(
                        item = next.item.rebuild(
                            parts = tail,
                            createNew = createNew,
                        ),
                    )
                }
                ?: ElementsStackItem.Next.create(
                    parts = tail,
                    createNew = createNew,
                )
        },
)

internal fun <T> ElementsStackItem.Next.Companion.create(
    parts: List<MqttTopic.Relative>,
    createNew: (MqttTopic.Relative) -> T,
): ElementsStackItem.Next<T>? = parts
    .firstOrNull()
    ?.let { head ->
        ElementsStackItem.Next(
            part = head,
            item = ElementsStackItem(
                value = createNew(head),
                next = ElementsStackItem.Next.create(
                    parts = parts.drop(1),
                    createNew = createNew,
                ),
            ),
        )
    }
