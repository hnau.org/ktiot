package hnau.ktiot.client.model.common

import arrow.core.Either
import hnau.common.mqtt.topic.MqttTopic

data class ClickableElement(
    val keyOrTitle: Either<MqttTopic, String>,
    val onClick: () -> Unit,
)
