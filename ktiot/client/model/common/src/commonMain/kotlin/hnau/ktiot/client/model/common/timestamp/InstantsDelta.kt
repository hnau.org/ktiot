package hnau.ktiot.client.model.common.timestamp

import arrow.core.NonEmptyList
import arrow.core.toNonEmptyListOrNull
import kotlinx.datetime.Instant

data class InstantsDelta(
    val isPositive: Boolean,
    val parts: NonEmptyList<Part>,
) {

    data class Part(
        val value: Int,
        val field: Field,
    ) {

        enum class Field { Seconds, Minutes, Hours, Days }
    }

    companion object {

        fun build(
            value: Instant,
            target: Instant,
            partsLimit: Int = 2,
        ): InstantsDelta {
            val rawDelta = value - target
            val delta = rawDelta.absoluteValue
            return InstantsDelta(
                isPositive = rawDelta.isPositive(),
                parts = delta
                    .toComponents { days: Long, hours: Int, minutes: Int, seconds: Int, _: Int ->
                        listOf(
                            Part(
                                field = Part.Field.Days,
                                value = days.toInt(),
                            ),
                            Part(
                                field = Part.Field.Hours,
                                value = hours,
                            ),
                            Part(
                                field = Part.Field.Minutes,
                                value = minutes,
                            ),
                            Part(
                                field = Part.Field.Seconds,
                                value = seconds,
                            ),
                        )
                    }
                    .filter { it.value > 0 }
                    .take(partsLimit)
                    .toNonEmptyListOrNull()
                    ?: NonEmptyList(
                        head = Part(
                            field = Part.Field.Seconds,
                            value = 0,
                        ),
                        tail = emptyList(),
                    )
            )
        }
    }
}