package hnau.ktiot.client.model.common

sealed interface ValueOrFinished<out T> {

    data class Value<out T>(
        val value: T,
    ) : ValueOrFinished<T>

    data object Finished : ValueOrFinished<Nothing>
}
