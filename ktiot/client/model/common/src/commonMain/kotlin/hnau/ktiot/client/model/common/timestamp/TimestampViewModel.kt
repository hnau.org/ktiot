package hnau.ktiot.client.model.common.timestamp

import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.mapStateLite
import hnau.ktiot.client.model.common.NowProvider
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.LocalTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

class TimestampViewModel(
    scope: CoroutineScope,
    dependencies: Dependencies,
    value: StateFlow<Instant>,
) {

    @Shuffle
    interface Dependencies {

        val nowProvider: NowProvider
    }

    private val dateTime: StateFlow<LocalDateTime> = value.mapState(scope) { instant ->
        instant.toLocalDateTime(TimeZone.currentSystemDefault())
    }

    val time: StateFlow<LocalTime> =
        dateTime.mapState(scope) { it.time }

    val date: StateFlow<LocalDate?> = combineState(
        scope = scope,
        a = dateTime.mapStateLite { it.date },
        b = dependencies.nowProvider.now.mapStateLite { it.toLocalDateTime(TimeZone.currentSystemDefault()).date },
    ) { date, currentDate ->
        date.takeIf { it != currentDate }
    }

    val delta: StateFlow<InstantsDelta> = combineState(
        scope = scope,
        a = value,
        b = dependencies.nowProvider.now,
    ) { value, now ->
        InstantsDelta.build(
            value = value,
            target = now,
        )
    }
}