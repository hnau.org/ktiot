package hnau.ktiot.client.model.common

import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.nullable
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.mapper.toMapper
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.util.UUID

@Serializable
data class MqttConnectionInfo(
    val address: String = defaultAddress,
    val protocol: Protocol = Protocol.default,
    val port: Int = defaultPort,
    val clientId: String = "",
) {

    enum class Protocol(
        val uriName: String,
    ) {
        TCP(
            uriName = "tcp",
        ),
        ;

        companion object {

            val default: Protocol
                get() = TCP
        }
    }


    companion object {

        const val defaultPort = 1883
        const val defaultAddress = "127.0.0.1"

        fun generateRandomClientId(): String =
            UUID.randomUUID().toString()

        val optionalStringMapper: Mapper<String, MqttConnectionInfo?> =
            Mapper.nullable("") + Json.toMapper(serializer()).nullable
    }
}
