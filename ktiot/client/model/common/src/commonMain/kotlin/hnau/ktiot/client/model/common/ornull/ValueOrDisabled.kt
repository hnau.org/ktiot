package hnau.ktiot.client.model.common.ornull

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

sealed interface ValueOrDisabled<out T> {

    data object Disabled : ValueOrDisabled<Nothing>

    data class Value<out T>(
        val value: T,
    ) : ValueOrDisabled<T>
}

inline fun <I, O> ValueOrDisabled<I>.map(
    transform: (I) -> O,
): ValueOrDisabled<O> = when (this) {

    ValueOrDisabled.Disabled ->
        ValueOrDisabled.Disabled

    is ValueOrDisabled.Value ->
        ValueOrDisabled.Value(transform(value))
}

fun <T> ValueOrDisabled<T>.toOption(): Option<T> = when (this) {
    ValueOrDisabled.Disabled -> None
    is ValueOrDisabled.Value -> Some(value)
}