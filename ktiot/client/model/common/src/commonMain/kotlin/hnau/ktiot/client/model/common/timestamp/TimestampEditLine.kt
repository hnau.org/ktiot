package hnau.ktiot.client.model.common.timestamp

enum class TimestampEditLine { Date, Time }