package hnau.ktiot.client.model.common

import arrow.core.Either

fun <T> T.orTitle(
    title: String?,
): Either<T, String> = when (title) {
    null -> Either.Left(this)
    else -> Either.Right(title)
}
