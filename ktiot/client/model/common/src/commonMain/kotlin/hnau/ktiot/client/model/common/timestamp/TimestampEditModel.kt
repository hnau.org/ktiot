package hnau.ktiot.client.model.common.timestamp

import arrow.core.Option
import arrow.core.fold
import hnau.common.app.EditingString
import hnau.common.app.toEditingString
import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.filterSet
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.LocalTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.format.DateTimeFormat
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.Serializable

class TimestampEditModel(
    scope: CoroutineScope,
    private val skeleton: Skeleton = Skeleton(),
) {

    @Serializable
    data class Skeleton(

        val values: TimestampEditFieldValues<@Serializable(MutableStateFlowSerializer::class) MutableStateFlow<EditingString>> =
            TimestampEditFieldValues.build { MutableStateFlow(EditingString.empty) },
    ) {

        companion object {

            fun create(
                instant: Instant,
            ): Skeleton = Skeleton(
                values = instant
                    .toLocalDateTime(TimeZone.currentSystemDefault())
                    .let
                    { dateTime ->
                        TimestampEditFieldValues(
                            day = dateTime.dayOfMonth,
                            month = dateTime.monthNumber,
                            year = dateTime.year,
                            hours = dateTime.hour,
                            minutes = dateTime.minute,
                            seconds = dateTime.second,
                        ).combine(lengths) { value, length ->
                            value
                                .toString()
                                .padStart(length, '0')
                                .toEditingString()
                                .let(::MutableStateFlow)
                        }
                    }
            )
        }
    }

    val values: TimestampEditFieldValues<MutableStateFlow<EditingString>>
        get() = skeleton.values.mapFull { field, value ->
            value.filterSet { it.text.length <= lengths[field] }
        }

    val timestampOrNone: StateFlow<Option<Instant>> = values
        .toMap()
        .fold<_, _, StateFlow<Map<TimestampEditField, EditingString>>>(
            initial = MutableStateFlow(emptyMap()),
        ) { acc, (field, value) ->
            combineState(
                scope = scope,
                acc,
                value,
            ) { map, value ->
                map + (field to value)
            }
        }
        .mapState(scope) { map ->
            val values = TimestampEditFieldValues.build(map::getValue)
            Option
                .catch {
                    LocalDateTime(
                        date = LocalDate.parse(
                            input = values.getLine(
                                line = TimestampEditLine.Date,
                                separator = dateSeparator,
                            ),
                            format = dateFormat,
                        ),
                        time = LocalTime.parse(
                            input = values.getLine(
                                line = TimestampEditLine.Time,
                                separator = timeSeparator,
                            ),
                            format = timeFormat,
                        ),
                    )
                }
                .map { it.toInstant(TimeZone.currentSystemDefault()) }
        }

    private fun TimestampEditFieldValues<EditingString>.getLine(
        line: TimestampEditLine,
        separator: String,
    ): String = TimestampEditField
        .entries
        .filter { it.line == line }
        .joinToString(separator = separator) { field ->
            get(field).text.padStart(lengths[field], '0')
        }

    companion object {

        const val dateSeparator = "/"

        private val dateFormat: DateTimeFormat<LocalDate> = LocalDate.Format {
            dayOfMonth()
            chars(dateSeparator)
            monthNumber()
            chars(dateSeparator)
            year()
        }

        const val timeSeparator = ":"

        private val timeFormat: DateTimeFormat<LocalTime> = LocalTime.Format {
            hour()
            chars(timeSeparator)
            minute()
            chars(timeSeparator)
            second()
        }

        val lengths: TimestampEditFieldValues<Int> = TimestampEditFieldValues(
            day = 2,
            month = 2,
            year = 4,
            hours = 2,
            minutes = 2,
            seconds = 2,
        )
    }
}