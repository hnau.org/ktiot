package hnau.ktiot.client.model.common

fun interface SettingsOpener {

    fun openSettings()
}
