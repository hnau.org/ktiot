package hnau.ktiot.client.model.common.timestamp

enum class TimestampEditField(
    val line: TimestampEditLine,
) {
    Day(
        line = TimestampEditLine.Date,
    ),

    Month(
        line = TimestampEditLine.Date,
    ),

    Year(
        line = TimestampEditLine.Date,
    ),

    Hours(
        line = TimestampEditLine.Time,
    ),

    Minutes(
        line = TimestampEditLine.Time,
    ),

    Seconds(
        line = TimestampEditLine.Time,
    ),
}