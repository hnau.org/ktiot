package hnau.ktiot.client.model.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

class NowProvider(
    scope: CoroutineScope,
) {

    @OptIn(ObsoleteCoroutinesApi::class)
    val now: StateFlow<Instant> = ticker(
        delayMillis = 1.seconds.inWholeMilliseconds,
        initialDelayMillis = 0L,
    )
        .consumeAsFlow()
        .let { tics ->
            val getNow: () -> Instant = { Clock.System.now() }
            tics
                .map { getNow() }
                .stateIn(
                    scope = scope,
                    started = SharingStarted.Eagerly,
                    initialValue = getNow(),
                )
        }
}