package hnau.ktiot.client.model.common.ornull

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

sealed interface ValueOrReadOnly<out T> {

    data object ReadOnly : ValueOrReadOnly<Nothing>

    data class Value<out T>(
        val value: T,
    ) : ValueOrReadOnly<T>
}

inline fun <I, O> ValueOrReadOnly<I>.map(
    transform: (I) -> O,
): ValueOrReadOnly<O> = when (this) {

    ValueOrReadOnly.ReadOnly ->
        ValueOrReadOnly.ReadOnly

    is ValueOrReadOnly.Value ->
        ValueOrReadOnly.Value(transform(value))
}

fun <T> ValueOrReadOnly<T>.toOption(): Option<T> = when (this) {
    ValueOrReadOnly.ReadOnly -> None
    is ValueOrReadOnly.Value -> Some(value)
}