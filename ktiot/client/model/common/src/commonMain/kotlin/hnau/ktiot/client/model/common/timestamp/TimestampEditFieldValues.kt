package hnau.ktiot.client.model.common.timestamp

import kotlinx.serialization.Serializable

@Serializable
data class TimestampEditFieldValues<T>(
    val day: T,
    val month: T,
    val year: T,
    val hours: T,
    val minutes: T,
    val seconds: T,
) {

    operator fun get(
        field: TimestampEditField,
    ): T = when (field) {
        TimestampEditField.Day -> day
        TimestampEditField.Month -> month
        TimestampEditField.Year -> year
        TimestampEditField.Hours -> hours
        TimestampEditField.Minutes -> minutes
        TimestampEditField.Seconds -> seconds
    }

    fun toMap(): Map<TimestampEditField, T> = mapOf(
        TimestampEditField.Day to day,
        TimestampEditField.Month to month,
        TimestampEditField.Year to year,
        TimestampEditField.Hours to hours,
        TimestampEditField.Minutes to minutes,
        TimestampEditField.Seconds to seconds,
    )

    inline fun <R> mapFull(
        transform: (TimestampEditField, T) -> R,
    ): TimestampEditFieldValues<R> = build { field ->
        val value = get(field)
        transform(field, value)
    }

    inline fun <R> map(transform: (T) -> R): TimestampEditFieldValues<R> =
        mapFull { _, value -> transform(value) }

    inline fun <O, R> combineFull(
        with: TimestampEditFieldValues<O>,
        combine: (TimestampEditField, T, O) -> R,
    ): TimestampEditFieldValues<R> = mapFull { field, value ->
        val otherValue = with[field]
        combine(field, value, otherValue)
    }

    inline fun <O, R> combine(
        with: TimestampEditFieldValues<O>,
        combine: (T, O) -> R,
    ): TimestampEditFieldValues<R> = combineFull(with) { _, value, otherValue ->
        combine(value, otherValue)
    }

    companion object {

        inline fun <T> build(
            builder: (TimestampEditField) -> T,
        ): TimestampEditFieldValues<T> = TimestampEditFieldValues(
            day = builder(TimestampEditField.Day),
            month = builder(TimestampEditField.Month),
            year = builder(TimestampEditField.Year),
            hours = builder(TimestampEditField.Hours),
            minutes = builder(TimestampEditField.Minutes),
            seconds = builder(TimestampEditField.Seconds),
        )
    }
}