package hnau.ktiot.client.model.common

fun interface DoLogout {

    suspend fun logout()
}
