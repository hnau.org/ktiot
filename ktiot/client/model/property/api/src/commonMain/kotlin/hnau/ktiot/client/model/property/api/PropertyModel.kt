package hnau.ktiot.client.model.property.api

import arrow.core.Either
import hnau.common.kotlin.Mutable
import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.ValueModel
import hnau.ktiot.client.model.property.api.value.ValueSkeleton
import hnau.ktiot.client.model.settings.item.SettingsAllowEditCalculatedProperties
import hnau.ktiot.client.model.settings.item.SettingsAllowEditHardwareProperties
import hnau.ktiot.scheme.property.PropertyInfo
import hnau.ktiot.scheme.property.ValueSource
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.serialization.Serializable

interface PropertyModel<T> {

    @Serializable
    data class Skeleton(
        val value: Mutable<ValueSkeleton?> = Mutable(null),
    )

    @Shuffle
    interface Dependencies {

        val connection: MqttConnection

        fun events(): EventsDependencies

        fun state(): StateModel.Dependencies

        val settingsAllowEditCalculatedProperties: SettingsAllowEditCalculatedProperties

        val settingsAllowEditHardwareProperties: SettingsAllowEditHardwareProperties
    }

    val keyOrTitle: Either<MqttTopic, String>

    val value: ValueModel<T>

    val source: ValueSource


    interface Factory {

        fun <T> createPropertyModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
            parenTopic: MqttTopic.Absolute,
            info: PropertyInfo<T>,
        ): PropertyModel<T>

        companion object
    }
}
