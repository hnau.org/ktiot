package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.app.EditingString
import hnau.common.color.RGBABytes
import hnau.common.color.gradient.Gradient
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

sealed interface RGBPassivePageModel {

    data class TextField(
        val value: MutableStateFlow<EditingString>,
        val prefix: String,
    ) : RGBPassivePageModel {

        @Serializable
        data class Skeleton(
            @Serializable(MutableStateFlowSerializer::class)
            val value: MutableStateFlow<EditingString>,
        )
    }

    data class Sliders(
        val sliders: List<Slider>,
    ) : RGBPassivePageModel {

        data class Slider(
            val prefix: String,
            val state: StateFlow<State>,
            val updateFraction: (Float) -> Unit,
        ) {

            data class State(
                val fraction: Float,
                val gradient: Gradient<RGBABytes>,
            )
        }
    }
}