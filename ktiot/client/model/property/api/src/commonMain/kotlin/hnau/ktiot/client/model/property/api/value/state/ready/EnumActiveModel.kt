package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.NonEmptyList
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface EnumActiveModel : ActiveModel<String> {

    @Serializable
    @SerialName("enum")
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val isOpened: MutableStateFlow<Boolean> = MutableStateFlow(false),

        var sendingKey: String? = null,
    ) : ActiveModel.Skeleton


    sealed interface State {

        data class Closed(
            val selectedVariant: PropertyViewType.State.Enum.Variant,
            val open: (() -> Unit)?,
        ) : State

        data class Opened(
            val items: NonEmptyList<Item>,
        ) : State {

            data class Item(
                val variant: PropertyViewType.State.Enum.Variant,
                val state: StateFlow<State>,
            ) {

                sealed interface State {

                    data class Selected(
                        val closeIfNotSelecting: (() -> Unit)?,
                    ) : State

                    data class NotSelected(
                        val selectIfNotSelecting: (() -> Unit)?,
                    ) : State

                    data class Selecting(
                        val cancel: () -> Unit,
                    ) : State
                }

            }
        }
    }

    val state: StateFlow<State>
}