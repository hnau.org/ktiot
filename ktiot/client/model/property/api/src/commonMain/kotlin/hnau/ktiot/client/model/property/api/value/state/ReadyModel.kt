package hnau.ktiot.client.model.property.api.value.state

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.serialization.Serializable

interface ReadyModel<T> : StateStateModel<T> {

    @Shuffle
    interface Dependencies {

        fun active(): ActiveModel.Dependencies
    }

    @Serializable
    data class Skeleton(
        @Serializable(OptionSerializer::class)
        var active: Option<ActiveModel.Skeleton> = None,
    ) : StateStateModel.Skeleton

    val active: ActiveModel<T>
}