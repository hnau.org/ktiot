package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.shuffler.annotations.Shuffle
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface ViewBasedOnActiveModel<T> : ViewModel<T> {

    @Shuffle
    interface Dependencies {

        val active: ActiveModel.Dependencies
    }

    @Serializable
    @SerialName("view_based_on_active")
    data class Skeleton(

        @Serializable(OptionSerializer::class)
        var active: Option<ActiveModel.Skeleton> = None,
    ) : ViewModel.Skeleton


    val model: ActiveModel<T>
}