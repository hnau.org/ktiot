package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.shuffler.annotations.Shuffle
import kotlinx.datetime.Instant
import hnau.ktiot.client.model.common.timestamp.TimestampViewModel as DelegateViewModel

interface TimestampViewModel : ViewModel<Instant> {

    @Shuffle
    interface Dependencies {

        fun delegate(): DelegateViewModel.Dependencies
    }

    val delegate: DelegateViewModel
}