package hnau.ktiot.client.model.property.api.value.state

import kotlinx.coroutines.flow.StateFlow

interface ReadingFirstValueModel<T> : StateStateModel<T> {

    val initOrNull: StateFlow<(() -> Unit)?>
}