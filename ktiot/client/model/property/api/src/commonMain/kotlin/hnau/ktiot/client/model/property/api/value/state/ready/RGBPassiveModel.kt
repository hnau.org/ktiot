package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.color.RGBABytes
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface RGBPassiveModel : PassiveModel<RGBABytes> {

    enum class Tab { HEX, RGB, HSL }

    @Serializable
    @SerialName("rgb")
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val selectedPage: MutableStateFlow<RGBPassivePage.Skeleton>,
    ) : PassiveModel.Skeleton

    val selectedTabWithPage: StateFlow<Pair<Tab, RGBPassivePageModel>>

    fun selectTab(tab: Tab)

    fun tryComplete()

    val isError: StateFlow<Boolean>
}