package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrDisabled
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface EditModel<out T> {

    @Shuffle
    interface Dependencies {

        fun passive(): PassiveModel.Dependencies
    }

    @Serializable
    data class Skeleton(

        @Serializable(OptionSerializer::class)
        var passive: Option<PassiveModel.Skeleton> = None,
    )

    data class Actions(
        val cancel: () -> Unit,
        val send: StateFlow<ValueOrDisabled<() -> Unit>>,
    )

    val passive: PassiveModel<T>

    val actions: StateFlow<ValueOrCancel<Actions>>
}