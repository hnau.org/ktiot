package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.ktiot.client.model.common.timestamp.TimestampEditModel
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface TimestampPassiveModel : PassiveModel<Instant> {

    @Serializable
    @SerialName("timestamp")
    data class Skeleton(
        val delegate: TimestampEditModel.Skeleton,
    ) : PassiveModel.Skeleton

    val delegate: TimestampEditModel

    fun tryComplete()
}