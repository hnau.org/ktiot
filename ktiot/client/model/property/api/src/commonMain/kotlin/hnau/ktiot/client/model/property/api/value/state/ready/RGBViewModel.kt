package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.color.RGBABytes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

interface RGBViewModel : ViewModel<RGBABytes> {

    val scope: CoroutineScope

    val color: StateFlow<RGBABytes>

    val hex: StateFlow<String>
}