package hnau.ktiot.client.model.property.api.value

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.property.api.value.state.InitBeforeFirstValueModel
import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.model.property.api.value.state.StateStateModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface StateModel<T> : ValueModel<T> {

    @Shuffle
    interface Dependencies {

        fun ready(): ReadyModel.Dependencies

        fun initBeforeFirstValue(): InitBeforeFirstValueModel.Dependencies
    }

    @Serializable
    @SerialName("state")
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val isInitializingBeforeFirstValue: MutableStateFlow<Boolean> = MutableStateFlow(false),

        @Serializable(OptionSerializer::class)
        var state: Option<StateStateModel.Skeleton> = None,
    ) : ValueSkeleton

    val state: StateFlow<StateStateModel<T>>
}
