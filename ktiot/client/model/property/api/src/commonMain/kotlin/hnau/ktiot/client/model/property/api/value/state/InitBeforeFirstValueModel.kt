package hnau.ktiot.client.model.property.api.value.state

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.serialization.Serializable

interface InitBeforeFirstValueModel<T> : StateStateModel<T> {

    val delegate: EditModel<T>

    @Shuffle
    interface Dependencies {

        fun active(): ActiveModel.Dependencies

        fun edit(
            active: ActiveModel.Dependencies,
        ): EditModel.Dependencies
    }

    @Serializable
    data class Skeleton(

        @Serializable(OptionSerializer::class)
        var edit: Option<EditModel.Skeleton> = None,
    ) : StateStateModel.Skeleton
}