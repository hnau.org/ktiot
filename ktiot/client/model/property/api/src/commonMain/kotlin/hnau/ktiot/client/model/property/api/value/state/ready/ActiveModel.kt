package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.Option
import hnau.common.kotlin.MutableAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

sealed interface ActiveModel<T> {

    @Shuffle
    interface Dependencies {

        fun option(
            active: Dependencies,
        ): OptionActiveModel.Dependencies

        fun activeBasedOnPassive(
            active: Dependencies,
        ): ActiveBasedOnPassiveModel.Dependencies
    }

    @Serializable
    sealed interface Skeleton

    fun interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: Dependencies,
            value: StateFlow<T>,
            sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
            skeleton: MutableAccessor<Option<Skeleton>>,
        ): ActiveModel<T>
    }

    companion object
}