package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.Timestamped
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.timestamp.TimestampEditModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface TimestampedPassiveModel<T> : PassiveModel<Timestamped<T>> {

    sealed interface State<out T> {

        data class Timestamp(
            val model: TimestampEditModel,
        ) : State<Nothing>

        data class Value<T>(
            val model: PassiveModel<T>,
        ) : State<T>
    }

    enum class Tab {
        Timestamp,
        Value;

        companion object {

            val default: Tab = Timestamp
        }
    }

    @Shuffle
    interface Dependencies {

        val passive: PassiveModel.Dependencies
    }

    @Serializable
    @SerialName("timestamped")
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val selectedTab: MutableStateFlow<Tab> = MutableStateFlow(Tab.default),

        @Serializable(OptionSerializer::class)
        var timestamp: Option<TimestampEditModel.Skeleton> = None,

        @Serializable(OptionSerializer::class)
        var value: Option<PassiveModel.Skeleton> = None,
    ) : PassiveModel.Skeleton

    val state: StateFlow<State<T>>

    fun selectTab(newTab: Tab)

    fun tryComplete()
}