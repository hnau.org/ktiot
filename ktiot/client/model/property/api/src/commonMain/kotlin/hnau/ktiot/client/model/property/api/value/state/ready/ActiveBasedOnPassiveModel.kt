package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.EitherSerializer
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.ornull.ValueOrDisabled
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface ActiveBasedOnPassiveModel<T> : ActiveModel<T> {

    @Shuffle
    interface Dependencies {

        fun view(): ViewModel.Dependencies

        fun edit(): EditModel.Dependencies
    }

    @Serializable
    data class Skeleton(

        @Serializable(OptionSerializer::class)
        var editOrView: Option<@Serializable(EitherSerializer::class) Either<EditModel.Skeleton, ViewModel.Skeleton>> = None,

        @Serializable(MutableStateFlowSerializer::class)
        val isEditing: MutableStateFlow<Boolean> = MutableStateFlow(false),
    ) : ActiveModel.Skeleton

    sealed interface State<out T> {

        data class View<T>(
            val model: ViewModel<T>,
            val edit: StateFlow<ValueOrReadOnly<StateFlow<ValueOrDisabled<() -> Unit>>>>,
        ) : State<T>

        data class Edit<T>(
            val model: EditModel<T>,
        ) : State<T>
    }

    val state: StateFlow<State<T>>
}