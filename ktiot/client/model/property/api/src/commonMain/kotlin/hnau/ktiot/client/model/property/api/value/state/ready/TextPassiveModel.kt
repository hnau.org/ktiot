package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.app.EditingString
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface TextPassiveModel : PassiveModel<String> {

    @Serializable
    @SerialName("text")
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val text: MutableStateFlow<EditingString>,
    ) : PassiveModel.Skeleton

    val text: MutableStateFlow<EditingString>

    fun tryComplete()
}