package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.flow.StateFlow

interface NumberViewModel : ViewModel<Float> {

    val number: StateFlow<Float>

    val viewType: PropertyViewType.State.Number
}