package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.Option
import hnau.common.kotlin.MutableAccessor
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

sealed interface ViewModel<T> {

    @Shuffle
    interface Dependencies {

        fun timestampView(): TimestampViewModel.Dependencies

        fun basedOnActive(): ViewBasedOnActiveModel.Dependencies

        fun timestampedView(
            view: Dependencies,
        ): TimestampedViewModel.Dependencies
    }

    @Serializable
    sealed interface Skeleton

    fun interface Factory<T> {

        fun createViewModel(
            scope: CoroutineScope,
            dependencies: Dependencies,
            value: StateFlow<T>,
            skeleton: MutableAccessor<Option<Skeleton>>,
        ): ViewModel<T>
    }

    companion object
}