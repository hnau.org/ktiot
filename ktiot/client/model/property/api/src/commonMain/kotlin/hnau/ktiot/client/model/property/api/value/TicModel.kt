package hnau.ktiot.client.model.property.api.value

import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import kotlinx.coroutines.flow.StateFlow

interface TicModel : ValueModel<Unit> {

    val isTicking: StateFlow<Boolean>

    val sendTicOrNullOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<() -> Unit>>>>
}
