package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface OptionActiveModel<T> : ActiveModel<Option<T>> {

    @Shuffle
    interface Dependencies {

        val active: ActiveModel.Dependencies

        fun edit(): EditModel.Dependencies
    }

    @Serializable
    @SerialName("option")
    data class Skeleton(

        @Serializable(MutableStateFlowSerializer::class)
        val isInitializing: MutableStateFlow<Boolean> = MutableStateFlow(false),

        @Serializable(OptionSerializer::class)
        var init: Option<EditModel.Skeleton> = None,

        @Serializable(OptionSerializer::class)
        var active: Option<ActiveModel.Skeleton> = None,
    ) : ActiveModel.Skeleton

    val sendIsSomeOrNullOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>>>

    val state: StateFlow<Either<EditModel<T>, ActiveModel<T>>?>

    val isSome: StateFlow<Boolean>
}