package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.client.model.common.ValueOrFinished
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

interface FractionActiveModel : ActiveModel<Float> {


    @Serializable
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val overwriteValue: MutableStateFlow<Float?> = MutableStateFlow(null),
    ) : ActiveModel.Skeleton


    val viewType: PropertyViewType.State.Fraction

    val value: StateFlow<Float>

    val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(ValueOrFinished<Float>) -> Unit>>>>
}