package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.Option
import hnau.common.kotlin.MutableAccessor
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.Serializable

sealed interface PassiveModel<out T> {

    @Shuffle
    interface Dependencies {

        fun basedOnActive(): PassiveBasedOnActiveModel.Dependencies

        fun timestamped(
            passive: Dependencies,
        ): TimestampedPassiveModel.Dependencies
    }

    @Serializable
    sealed interface Skeleton

    fun interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: Dependencies,
            skeleton: MutableAccessor<Option<Skeleton>>,
            tryGetInitialValue: () -> Option<T>,
            tryComplete: () -> Unit,
        ): PassiveModel<T>
    }

    val currentOrNone: StateFlow<Option<T>>

    companion object
}