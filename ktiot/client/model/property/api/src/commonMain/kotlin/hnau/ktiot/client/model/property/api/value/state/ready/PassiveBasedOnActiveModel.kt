package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface PassiveBasedOnActiveModel<T> : PassiveModel<T> {

    @Shuffle
    interface Dependencies {

        val active: ActiveModel.Dependencies
    }

    @Serializable
    @SerialName("passive_based_on_active")
    data class Skeleton<T>(

        @Serializable(MutableStateFlowSerializer::class)
        val currentValue: MutableStateFlow<T>,

        @Serializable(OptionSerializer::class)
        var activeModelSkeleton: Option<ActiveModel.Skeleton> = None,
    ) : PassiveModel.Skeleton {

        constructor(
            initialValue: T,
        ) : this(
            currentValue = MutableStateFlow(initialValue),
        )
    }


    val model: ActiveModel<T>
}