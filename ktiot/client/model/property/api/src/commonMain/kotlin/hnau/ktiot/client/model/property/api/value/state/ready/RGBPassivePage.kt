package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.color.HSLFloats
import hnau.common.color.RGBFloats
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed interface RGBPassivePage<MODEL : RGBPassivePageModel> {

    sealed interface Skeleton

    val tab: RGBPassiveModel.Tab

    val model: MODEL

    data class HEX(
        override val model: RGBPassivePageModel.TextField,
    ) : RGBPassivePage<RGBPassivePageModel.TextField> {

        @Serializable
        @SerialName("hex")
        data class Skeleton(
            val model: RGBPassivePageModel.TextField.Skeleton,
        ) : RGBPassivePage.Skeleton

        override val tab: RGBPassiveModel.Tab
            get() = RGBPassiveModel.Tab.HEX
    }

    data class RGB(
        override val model: RGBPassivePageModel.Sliders,
    ) : RGBPassivePage<RGBPassivePageModel.Sliders> {

        @Serializable
        @SerialName("rgb")
        data class Skeleton(
            @Serializable(MutableStateFlowSerializer::class)
            val rgb: MutableStateFlow<RGBFloats>,
        ) : RGBPassivePage.Skeleton

        override val tab: RGBPassiveModel.Tab
            get() = RGBPassiveModel.Tab.RGB
    }

    data class HSL(
        override val model: RGBPassivePageModel.Sliders,
    ) : RGBPassivePage<RGBPassivePageModel.Sliders> {

        @Serializable
        @SerialName("hsl")
        data class Skeleton(
            @Serializable(MutableStateFlowSerializer::class)
            val hsl: MutableStateFlow<HSLFloats>,
        ) : RGBPassivePage.Skeleton

        override val tab: RGBPassiveModel.Tab
            get() = RGBPassiveModel.Tab.HSL
    }
}