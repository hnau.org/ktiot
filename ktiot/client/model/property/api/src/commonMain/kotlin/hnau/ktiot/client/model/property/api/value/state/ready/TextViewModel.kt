package hnau.ktiot.client.model.property.api.value.state.ready

import kotlinx.coroutines.flow.StateFlow

interface TextViewModel : ViewModel<String> {

    val text: StateFlow<String>
}