package hnau.ktiot.client.model.property.api.value.state.ready

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.kotlin.Timestamped
import hnau.ktiot.client.model.common.timestamp.TimestampViewModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface TimestampedViewModel<T> : ViewModel<Timestamped<T>> {

    @Shuffle
    interface Dependencies {

        fun timestamp(): TimestampViewModel.Dependencies

        val view: ViewModel.Dependencies
    }

    @Serializable
    @SerialName("timestamped")
    data class Skeleton(

        @Serializable(OptionSerializer::class)
        var value: Option<ViewModel.Skeleton> = None,
    ) : ViewModel.Skeleton

    val timestamp: TimestampViewModel

    val value: ViewModel<T>
}