package hnau.ktiot.client.model.property.api.value

import kotlinx.serialization.Serializable

@Serializable
sealed interface ValueSkeleton
