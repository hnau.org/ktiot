package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import kotlinx.coroutines.flow.StateFlow

interface FlagActiveModel : ActiveModel<Boolean> {

    val value: StateFlow<Boolean>

    val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>>>
}