package hnau.ktiot.client.model.property.api.value.state.ready

import hnau.common.app.EditingString
import hnau.common.kotlin.serialization.MutableStateFlowSerializer
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface NumberPassiveModel : PassiveModel<Float> {

    @Serializable
    @SerialName("number")
    data class Skeleton(
        @Serializable(MutableStateFlowSerializer::class)
        val number: MutableStateFlow<EditingString>,
    ) : PassiveModel.Skeleton

    val viewType: PropertyViewType.State.Number

    fun tryComplete()

    val text: MutableStateFlow<EditingString>

    val isError: StateFlow<Boolean>
}