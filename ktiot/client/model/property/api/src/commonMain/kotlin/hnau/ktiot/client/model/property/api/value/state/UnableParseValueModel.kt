package hnau.ktiot.client.model.property.api.value.state

interface UnableParseValueModel<T> : StateStateModel<T> {
    val error: Throwable
}