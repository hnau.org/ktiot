plugins {
    alias(libs.plugins.ksp)
    alias(libs.plugins.kotlin.serialization)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(libs.kotlin.datetime)
            implementation(project(":common:app"))
            implementation(project(":common:color"))
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:settings:item"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
