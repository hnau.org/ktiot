package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.Option
import arrow.core.toOption
import hnau.common.app.EditingString
import hnau.common.app.toEditingString
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.mapStateLite
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.ifNull
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.property.api.value.state.ready.NumberPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.NumberViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class NumberPassiveModelImpl(
    scope: CoroutineScope,
    private val skeleton: NumberPassiveModel.Skeleton,
    override val viewType: PropertyViewType.State.Number,
    private val tryComplete: () -> Unit,
) : NumberPassiveModel {
    override fun tryComplete() = tryComplete.invoke()

    override val text: MutableStateFlow<EditingString>
        get() = skeleton.number

    override val currentOrNone: StateFlow<Option<Float>> = skeleton
        .number
        .mapState(scope) { editingNumber ->
            editingNumber
                .text
                .toFloatOrNull()
                .toOption()
        }

    override val isError: StateFlow<Boolean> =
        currentOrNone.mapStateLite(Option<Float>::isNone)


    companion object {


        fun factory(
            viewType: PropertyViewType.State.Number,
        ): PassiveModel.Factory<Float> =
            PassiveModel.Factory { scope, _, skeleton, tryGetInitialValue, tryComplete ->
                NumberPassiveModelImpl(
                    scope = scope,
                    skeleton = skeleton
                        .shrinkType<_, NumberPassiveModel.Skeleton>()
                        .getOrInit {
                            NumberPassiveModel.Skeleton(
                                number = MutableStateFlow(
                                    tryGetInitialValue()
                                        .getOrNull()
                                        .ifNull { 0f }
                                        .toString()
                                        .toEditingString()
                                )
                            )
                        },
                    viewType = viewType,
                    tryComplete = tryComplete,
                )
            }
    }
}

data class NumberViewModelImpl(
    override val number: StateFlow<Float>,
    override val viewType: PropertyViewType.State.Number,
) : NumberViewModel {

    companion object {

        fun factory(
            viewType: PropertyViewType.State.Number,
        ): ViewModel.Factory<Float> =
            ViewModel.Factory { _, _, value, _ ->
                NumberViewModelImpl(
                    number = value,
                    viewType = viewType,
                )
            }
    }
}

internal fun StateModelFactories.Companion.number(
    viewType: PropertyViewType.State.Number,
): StateModelFactories<Float> = create(
    passive = NumberPassiveModelImpl.factory(
        viewType = viewType,
    ),
    view = NumberViewModelImpl.factory(
        viewType = viewType
    ),
)