package hnau.ktiot.client.model.property.impl.state

import arrow.core.Either
import arrow.core.Option
import hnau.common.kotlin.MutableAccessor
import hnau.common.kotlin.coroutines.Stickable
import hnau.common.kotlin.coroutines.predeterminated
import hnau.common.kotlin.coroutines.stateFlow
import hnau.common.kotlin.coroutines.stick
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.model.property.api.value.state.StateStateModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal object StateModelReadyDelegate {

    fun <T> createReadyOrParseErrorModel(
        scope: CoroutineScope,
        type: PropertyViewType.State<T>,
        skeleton: MutableAccessor<Option<ReadyModel.Skeleton>>,
        dependencies: StateModel.Dependencies,
        value: StateFlow<Either<Throwable, T>>,
        sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
    ): StateFlow<StateStateModel<T>> = value
        .stick(scope) { stickableScope, errorOrValue ->
            when (errorOrValue) {
                is Either.Left -> Stickable.predeterminated(
                    UnableParseValueModelImpl(
                        error = errorOrValue.value,
                    )
                )

                is Either.Right -> Stickable.stateFlow(
                    initial = errorOrValue.value,
                    tryUseNext = { it.getOrNone() },
                    createResult = { parsedValues ->
                        ReadyModelImpl(
                            scope = stickableScope,
                            dependencies = dependencies.ready(),
                            value = parsedValues,
                            type = type,
                            skeleton = skeleton,
                            sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
                        )
                    },
                )
            }
        }
}
