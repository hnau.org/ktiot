package hnau.ktiot.client.model.property.impl.state.value

import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.common.ValueOrFinished
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.map
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.FractionActiveModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class FractionActiveModelImpl(
    scope: CoroutineScope,
    value: StateFlow<Float>,
    private val skeleton: FractionActiveModel.Skeleton,
    sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Float) -> Unit>>>>,
    override val viewType: PropertyViewType.State.Fraction,
) : FractionActiveModel {

    init {
        scope.launch {
            value.collect {
                skeleton.overwriteValue.value = null
            }
        }
    }

    override val value: StateFlow<Float> =
        skeleton.overwriteValue.flatMapState(scope) { overwriteValue ->
            when (overwriteValue) {
                null -> value
                else -> MutableStateFlow(overwriteValue)
            }
        }

    override val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(ValueOrFinished<Float>) -> Unit>>>> =
        sendOrCancelOrReadOnly
            .scopedInState(scope)
            .mapState(scope) { (sendOrCancelFlowOrNullScope, sendOrCancelFlowOrNull) ->
                sendOrCancelFlowOrNull.map { sendOrCancelFlow: StateFlow<ValueOrCancel<(Float) -> Unit>> ->
                    sendOrCancelFlow.mapState(sendOrCancelFlowOrNullScope) { sendOrCancel ->
                        when (sendOrCancel) {
                            is ValueOrCancel.Cancel -> ValueOrCancel.Cancel {
                                skeleton.overwriteValue.value = null
                                sendOrCancel.cancel()
                            }

                            is ValueOrCancel.Value -> ValueOrCancel.Value { floatOrFinished ->
                                when (floatOrFinished) {
                                    ValueOrFinished.Finished -> {
                                        val lastValue = skeleton.overwriteValue.value!!
                                        sendOrCancel.value(lastValue)
                                    }

                                    is ValueOrFinished.Value<Float> ->
                                        skeleton.overwriteValue.value = floatOrFinished.value
                                }
                            }
                        }
                    }
                }
            }

    companion object {


        fun factory(
            viewType: PropertyViewType.State.Fraction,
        ): ActiveModel.Factory<Float> =
            ActiveModel.Factory { scope, _, value, sendOrCancelOrReadOnly, skeleton ->
                FractionActiveModelImpl(
                    scope = scope,
                    value = value,
                    sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
                    viewType = viewType,
                    skeleton = skeleton
                        .shrinkType<_, FractionActiveModel.Skeleton>()
                        .getOrInit { FractionActiveModel.Skeleton() },
                )
            }
    }
}

internal fun StateModelFactories.Companion.fraction(
    viewType: PropertyViewType.State.Fraction,
): StateModelFactories<Float> = create(
    active = FractionActiveModelImpl.factory(
        viewType = viewType,
    ),
    createInitialValue = { 0f },
)