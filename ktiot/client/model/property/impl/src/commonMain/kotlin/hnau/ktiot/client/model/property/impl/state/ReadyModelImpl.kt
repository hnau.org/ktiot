package hnau.ktiot.client.model.property.impl.state

import arrow.core.Option
import hnau.common.kotlin.MutableAccessor
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.impl.state.value.get
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class ReadyModelImpl<T>(
    scope: CoroutineScope,
    dependencies: ReadyModel.Dependencies,
    skeleton: MutableAccessor<Option<ReadyModel.Skeleton>>,
    type: PropertyViewType.State<T>,
    sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
    value: StateFlow<T>,
) : ReadyModel<T> {

    private val actualSkeleton: ReadyModel.Skeleton = skeleton
        .getOrInit { ReadyModel.Skeleton() }

    override val active: ActiveModel<T> = ActiveModel[type].create(
        scope = scope,
        dependencies = dependencies.active(),
        value = value,
        sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
        skeleton = actualSkeleton::active.toAccessor(),
    )
}