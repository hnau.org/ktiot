package hnau.ktiot.client.model.property.impl

import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.TicModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.transformLatest
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

class TicModelImpl(
    scope: CoroutineScope,
    data: Flow<Unit>,
    override val sendTicOrNullOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<() -> Unit>>>>,
) : TicModel {

    override val isTicking: StateFlow<Boolean> = data
        .transformLatest {
            emit(true)
            delay(tickingDuration)
            emit(false)
        }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = false,
        )

    companion object {

        private val tickingDuration: Duration = 300.milliseconds
    }
}