package hnau.ktiot.client.model.property.impl

import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.scheme.property.PropertyInfo
import kotlinx.coroutines.CoroutineScope

fun PropertyModel.Factory.Companion.impl(): PropertyModel.Factory = object : PropertyModel.Factory {

    override fun <T> createPropertyModel(
        scope: CoroutineScope,
        skeleton: PropertyModel.Skeleton,
        dependencies: PropertyModel.Dependencies,
        parenTopic: MqttTopic.Absolute,
        info: PropertyInfo<T>,
    ): PropertyModel<T> = PropertyModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        parenTopic = parenTopic,
        info = info,
    )
}