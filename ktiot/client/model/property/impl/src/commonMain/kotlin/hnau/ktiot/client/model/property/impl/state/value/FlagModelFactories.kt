package hnau.ktiot.client.model.property.impl.state.value

import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.FlagActiveModel
import kotlinx.coroutines.flow.StateFlow

data class FlagActiveModelImpl(
    override val value: StateFlow<Boolean>,
    override val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>>>,
) : FlagActiveModel {

    companion object {


        val Factory =
            ActiveModel.Factory { _, _, value, sendOrCancelOrReadOnly, _ ->
                FlagActiveModelImpl(
                    value = value,
                    sendOrCancelOrReadOnly = sendOrCancelOrReadOnly
                )
            }
    }
}


private val flagModelFactories: StateModelFactories<Boolean> = StateModelFactories.create(
    active = FlagActiveModelImpl.Factory,
    createInitialValue = { false },
)

internal val StateModelFactories.Companion.flag: StateModelFactories<Boolean>
    get() = flagModelFactories