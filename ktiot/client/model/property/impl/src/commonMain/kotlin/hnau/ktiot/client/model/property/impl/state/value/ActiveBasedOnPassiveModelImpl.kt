package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.MutableAccessor
import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.leftOrNone
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrDisabled
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.map
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveBasedOnPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class ActiveBasedOnPassiveModelImpl<T>(
    private val scope: CoroutineScope,
    private val dependencies: ActiveBasedOnPassiveModel.Dependencies,
    private val skeleton: ActiveBasedOnPassiveModel.Skeleton,
    private val passiveModelFactory: PassiveModel.Factory<T>,
    private val view: ViewModel.Factory<T>,
    private val value: StateFlow<T>,
    private val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
) : ActiveBasedOnPassiveModel<T> {

    private val isEditingOrSending: StateFlow<Boolean> = combineState(
        scope = scope,
        a = skeleton.isEditing,
        b = sendOrCancelOrReadOnly,
    ) { isEditing, sendOrCancelOrReadOnly ->
        isEditing to sendOrCancelOrReadOnly
    }
        .scopedInState(scope)
        .flatMapState(scope) { (localScope, isEditingAndSendOrCancelOrReadOnly) ->
            val (isEditing, sendOrCancelOrReadOnly) = isEditingAndSendOrCancelOrReadOnly
            when (isEditing) {
                true -> MutableStateFlow(true)
                false -> when (sendOrCancelOrReadOnly) {
                    ValueOrReadOnly.ReadOnly -> MutableStateFlow(false)
                    is ValueOrReadOnly.Value -> sendOrCancelOrReadOnly.value.mapState(localScope) { sendOrCancel ->
                        when (sendOrCancel) {
                            is ValueOrCancel.Cancel -> true
                            is ValueOrCancel.Value -> false
                        }
                    }
                }
            }
        }

    override val state: StateFlow<ActiveBasedOnPassiveModel.State<T>> = isEditingOrSending
        .flatMapState(scope) { isEditingOrSending ->
            when (isEditingOrSending) {
                false -> MutableStateFlow(ValueOrReadOnly.ReadOnly)
                true -> sendOrCancelOrReadOnly
            }
        }
        .scopedInState(scope)
        .mapState(scope)
        { (sendOrCancelOrNullScope, sendOrCancelFlowOrNull) ->
            when (sendOrCancelFlowOrNull) {
                ValueOrReadOnly.ReadOnly -> createViewState(
                    scope = sendOrCancelOrNullScope,
                )

                is ValueOrReadOnly.Value -> createEditState(
                    scope = sendOrCancelOrNullScope,
                    sendOrCancelFlowOrNull = sendOrCancelFlowOrNull,
                )
            }
        }


    private fun createViewState(
        scope: CoroutineScope,
    ): ActiveBasedOnPassiveModel.State.View<T> {

        val edit: StateFlow<ValueOrReadOnly<StateFlow<ValueOrDisabled<() -> Unit>>>> =
            sendOrCancelOrReadOnly
                .scopedInState(this.scope)
                .mapState(this.scope) { (sendOrCancelFlowScope, sendOrCancelFlowOrReadOnly) ->
                    sendOrCancelFlowOrReadOnly.map { sendOrCancelFlow ->
                        sendOrCancelFlow.mapState(sendOrCancelFlowScope) { sendOrCancel ->
                            when (sendOrCancel) {
                                is ValueOrCancel.Cancel -> ValueOrDisabled.Disabled
                                is ValueOrCancel.Value -> ValueOrDisabled.Value(
                                    value = { setIsEditing(true) }
                                )
                            }
                        }
                    }
                }

        return ActiveBasedOnPassiveModel.State.View(
            model = view.createViewModel(
                scope = scope,
                dependencies = dependencies.view(),
                value = value,
                skeleton = getStateSkeleton(
                    extract = Either<*, ViewModel.Skeleton>::getOrNone,
                    assemble = ::Right,
                ),
            ),
            edit = edit,
        )
    }

    private fun createEditState(
        scope: CoroutineScope,
        sendOrCancelFlowOrNull: ValueOrReadOnly.Value<StateFlow<ValueOrCancel<(T) -> Unit>>>,
    ): ActiveBasedOnPassiveModel.State.Edit<T> {

        val sendOrCancel: StateFlow<ValueOrCancel<(T) -> Unit>> = sendOrCancelFlowOrNull
            .value
            .mapState(scope) { sendOrCancel ->
                when (sendOrCancel) {
                    is ValueOrCancel.Cancel -> ValueOrCancel.Cancel(
                        cancel = {
                            setIsEditing(true)
                            sendOrCancel.cancel()
                        }
                    )

                    is ValueOrCancel.Value -> ValueOrCancel.Value { newValue: T ->
                        sendOrCancel.value(newValue)
                        setIsEditing(false)
                    }
                }
            }

        return ActiveBasedOnPassiveModel.State.Edit(
            model = EditModelImpl(
                scope = scope,
                dependencies = dependencies.edit(),
                passiveModelFactory = passiveModelFactory,
                skeleton = getStateSkeleton(
                    extract = Either<EditModel.Skeleton, *>::leftOrNone,
                    assemble = ::Left,
                ),
                sendOrCancel = sendOrCancel,
                cancel = { setIsEditing(false) },
                tryGetCurrentValue = { Some(value.value) },
            )
        )
    }

    private inline fun <T> getStateSkeleton(
        crossinline extract: (Either<EditModel.Skeleton, ViewModel.Skeleton>) -> Option<T>,
        crossinline assemble: (T) -> Either<EditModel.Skeleton, ViewModel.Skeleton>,
    ): MutableAccessor<Option<T>> = skeleton::editOrView
        .toAccessor()
        .let { eitherAccessor ->
            val set = { valueOrNone: Option<T> ->
                valueOrNone
                    .map(assemble)
                    .let(eitherAccessor.set)
            }
            val value = eitherAccessor
                .get()
                .flatMap(extract)
                .also(set)
            MutableAccessor(
                get = { value },
                set = set,
            )
        }

    private fun setIsEditing(
        isEditing: Boolean,
    ) {
        skeleton.isEditing.value = isEditing
    }
}

fun <T> ActiveModel.Companion.fromPassiveAndView(
    passive: PassiveModel.Factory<T>,
    view: ViewModel.Factory<T>,
): ActiveModel.Factory<T> =
    ActiveModel.Factory { scope, dependencies, value, sendOrCancelOrReadOnly, skeleton ->
        ActiveBasedOnPassiveModelImpl(
            scope = scope,
            dependencies = dependencies.activeBasedOnPassive(dependencies),
            skeleton = skeleton
                .shrinkType<_, ActiveBasedOnPassiveModel.Skeleton>()
                .getOrInit { ActiveBasedOnPassiveModel.Skeleton() },
            passiveModelFactory = passive,
            view = view,
            value = value,
            sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
        )
    }
