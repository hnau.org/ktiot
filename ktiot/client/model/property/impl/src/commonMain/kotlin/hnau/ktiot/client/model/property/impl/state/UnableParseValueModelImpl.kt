package hnau.ktiot.client.model.property.impl.state

import hnau.ktiot.client.model.property.api.value.state.UnableParseValueModel

internal data class UnableParseValueModelImpl<T>(
    override val error: Throwable,
) : UnableParseValueModel<T>