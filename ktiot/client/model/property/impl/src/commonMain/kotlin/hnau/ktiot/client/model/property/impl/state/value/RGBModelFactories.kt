package hnau.ktiot.client.model.property.impl.state.value

import hnau.common.color.RGBABytes


private val rgbModelFactories: StateModelFactories<RGBABytes> =
    StateModelFactories.create(
        passive = RGBPassiveModelImpl.Factory,
        view = RGBViewModelImpl.Factory,
    )

internal val StateModelFactories.Companion.rgb: StateModelFactories<RGBABytes>
    get() = rgbModelFactories