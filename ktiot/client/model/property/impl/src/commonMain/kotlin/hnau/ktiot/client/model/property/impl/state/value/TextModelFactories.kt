package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.Option
import arrow.core.Some
import hnau.common.app.EditingString
import hnau.common.app.toEditingString
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.ifNull
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TextPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TextViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class TextPassiveModelImpl(
    scope: CoroutineScope,
    private val skeleton: TextPassiveModel.Skeleton,
    private val tryComplete: () -> Unit,
) : TextPassiveModel {

    override fun tryComplete() = tryComplete.invoke()

    override val text: MutableStateFlow<EditingString>
        get() = skeleton.text

    override val currentOrNone: StateFlow<Option<String>> = skeleton
        .text
        .mapState(scope) { editingString -> Some(editingString.text) }


    companion object {


        val Factory =
            PassiveModel.Factory<String> { scope, _, skeleton, tryGetInitialValue, tryComplete ->
                TextPassiveModelImpl(
                    scope = scope,
                    skeleton = skeleton
                        .shrinkType<_, TextPassiveModel.Skeleton>()
                        .getOrInit {
                            TextPassiveModel.Skeleton(
                                text = MutableStateFlow(
                                    tryGetInitialValue()
                                        .getOrNull()
                                        .ifNull { "" }
                                        .toEditingString()
                                )
                            )
                        },
                    tryComplete = tryComplete,
                )
            }
    }
}

data class TextViewModelImpl(
    override val text: StateFlow<String>,
) : TextViewModel {

    companion object {

        val Factory = ViewModel.Factory { _, _, value, _ ->
            TextViewModelImpl(
                text = value,
            )
        }
    }
}


private val textModelFactories: StateModelFactories<String> =
    StateModelFactories.create(
        passive = TextPassiveModelImpl.Factory,
        view = TextViewModelImpl.Factory,
    )

internal val StateModelFactories.Companion.text: StateModelFactories<String>
    get() = textModelFactories