package hnau.ktiot.client.model.property.impl

import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.mqtt.MqttData
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.map
import hnau.ktiot.client.model.property.api.EventsDependencies
import hnau.ktiot.client.model.property.api.value.TicModel
import hnau.ktiot.client.model.property.api.value.ValueModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal object PropertyModelEventsDelegate {

    @Suppress("UNCHECKED_CAST")
    fun <T> createEventsModel(
        scope: CoroutineScope,
        type: PropertyViewType.Events<T>,
        dependencies: EventsDependencies,
        mapper: SerializableMapper<MqttData, *>,
        sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
        toEventsSubscriber: ToEventsSubscriber,
    ): ValueModel<T> = when (type) {
        PropertyViewType.Events.Tic -> createTicModel(
            scope = scope,
            mapper = mapper as SerializableMapper<MqttData, Unit>,
            sendOrCancelOrReadOnly = sendOrCancelOrReadOnly as StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Unit) -> Unit>>>>,
            toEventsSubscriber = toEventsSubscriber,
        ) as ValueModel<T>
    }

    private fun createTicModel(
        scope: CoroutineScope,
        mapper: SerializableMapper<MqttData, Unit>,
        sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Unit) -> Unit>>>>,
        toEventsSubscriber: ToEventsSubscriber,
    ): TicModel = TicModelImpl(
        scope = scope,
        sendTicOrNullOrReadOnly = sendOrCancelOrReadOnly
            .scopedInState(scope)
            .mapState(scope) { (sendOrCancelOrReadOnlyScope, sendOrCancelOrReadOnly) ->
                sendOrCancelOrReadOnly.map { sendOrCancelFlow ->
                    sendOrCancelFlow.mapState(sendOrCancelOrReadOnlyScope) { sendOrCancel ->
                        sendOrCancel.map { send -> { send(Unit) } }
                    }
                }
            },
        data = toEventsSubscriber.subscribeToEvents(mapper),
    )
}
