package hnau.ktiot.client.model.property.impl.state.value

import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType

operator fun <T> PassiveModel.Companion.get(
    viewType: PropertyViewType.State<T>,
): PassiveModel.Factory<T> = StateModelFactories[viewType].passive