package hnau.ktiot.client.model.property.impl.state.value

import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType

operator fun <T> ActiveModel.Companion.get(
    viewType: PropertyViewType.State<T>,
): ActiveModel.Factory<T> = StateModelFactories[viewType].active