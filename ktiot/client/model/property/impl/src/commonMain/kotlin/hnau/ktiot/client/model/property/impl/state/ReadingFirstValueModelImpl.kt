package hnau.ktiot.client.model.property.impl.state

import hnau.common.kotlin.coroutines.mapState
import hnau.ktiot.client.model.property.api.value.state.ReadingFirstValueModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class ReadingFirstValueModelImpl<T>(
    scope: CoroutineScope,
    isWritable: StateFlow<Boolean>,
    switchToInit: () -> Unit,
) : ReadingFirstValueModel<T> {

    override val initOrNull: StateFlow<(() -> Unit)?> = isWritable
        .mapState(scope) { isWritable -> switchToInit.takeIf { isWritable } }
}