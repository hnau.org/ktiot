package hnau.ktiot.client.model.property.impl

import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.subscribeToEventsTyped
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.coroutines.flow.Flow

internal interface ToEventsSubscriber {

    fun <T> subscribeToEvents(
        mapper: SerializableMapper<MqttData, T>,
    ): Flow<T>

    companion object {

        fun create(
            topic: MqttTopic.Absolute,
            connection: MqttConnection,
        ): ToEventsSubscriber = object : ToEventsSubscriber {

            override fun <T> subscribeToEvents(
                mapper: SerializableMapper<MqttData, T>,
            ): Flow<T> = connection.subscribeToEventsTyped(
                topic = topic,
                deserializer = mapper.mapper.direct,
            )
        }
    }
}
