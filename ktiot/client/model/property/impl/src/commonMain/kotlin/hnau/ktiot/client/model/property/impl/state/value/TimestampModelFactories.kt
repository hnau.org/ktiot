package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.common.timestamp.TimestampEditModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.datetime.Instant
import hnau.ktiot.client.model.common.timestamp.TimestampViewModel as DelegateViewModel

class TimestampPassiveModelImpl(
    scope: CoroutineScope,
    skeleton: TimestampPassiveModel.Skeleton,
    private val tryComplete: () -> Unit,
) : TimestampPassiveModel {

    override fun tryComplete() = tryComplete.invoke()

    override val delegate = TimestampEditModel(
        scope = scope,
        skeleton = skeleton.delegate,
    )

    override val currentOrNone: StateFlow<Option<Instant>>
        get() = delegate.timestampOrNone


    companion object {


        val Factory =
            PassiveModel.Factory { scope, _, skeleton, tryGetInitialValue, tryComplete ->
                TimestampPassiveModelImpl(
                    scope = scope,
                    skeleton = skeleton
                        .shrinkType<_, TimestampPassiveModel.Skeleton>()
                        .getOrInit {
                            TimestampPassiveModel.Skeleton(
                                delegate = when (val initial: Option<Instant> =
                                    tryGetInitialValue()) {
                                    None -> TimestampEditModel.Skeleton()
                                    is Some -> TimestampEditModel.Skeleton.create(initial.value)
                                },
                            )
                        },
                    tryComplete = tryComplete,
                )
            }
    }
}

class TimestampViewModelImpl(
    scope: CoroutineScope,
    timestamp: StateFlow<Instant>,
    dependencies: TimestampViewModel.Dependencies,
) : TimestampViewModel {

    override val delegate = DelegateViewModel(
        scope = scope,
        value = timestamp,
        dependencies = dependencies.delegate(),
    )

    companion object {

        val Factory =
            ViewModel.Factory { scope, dependencies, value, _ ->
                TimestampViewModelImpl(
                    scope = scope,
                    timestamp = value,
                    dependencies = dependencies.timestampView(),
                )
            }
    }
}


private val timestampModelFactories: StateModelFactories<Instant> = StateModelFactories.create(
    passive = TimestampPassiveModelImpl.Factory,
    view = TimestampViewModelImpl.Factory,
)

internal val StateModelFactories.Companion.timestamp: StateModelFactories<Instant>
    get() = timestampModelFactories