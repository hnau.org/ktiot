package hnau.ktiot.client.model.property.impl

import arrow.core.Either
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrPutTyped
import hnau.common.kotlin.remindType
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.publishTyped
import hnau.common.mqtt.topic.MqttTopic
import hnau.common.mqtt.topic.toAbsolute
import hnau.ktiot.client.model.common.orTitle
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.ValueModel
import hnau.ktiot.client.model.property.impl.state.StateModelImpl
import hnau.ktiot.client.model.settings.item.SettingsAllowEditCalculatedProperties
import hnau.ktiot.client.model.settings.item.SettingsAllowEditHardwareProperties
import hnau.ktiot.scheme.property.PropertyInfo
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

internal class PropertyModelImpl<T>(
    private val scope: CoroutineScope,
    private val skeleton: PropertyModel.Skeleton,
    private val dependencies: PropertyModel.Dependencies,
    parenTopic: MqttTopic.Absolute,
    private val info: PropertyInfo<T>,
) : PropertyModel<T> {

    private val subscribeTopic: MqttTopic.Absolute = info
        .topic
        .toAbsolute(parenTopic)

    private val publishTopic: MqttTopic.Absolute = info
        .publishTopic
        ?.toAbsolute(parenTopic)
        ?: subscribeTopic

    override val keyOrTitle: Either<MqttTopic, String> =
        info.topic.orTitle(info.title)

    private val isWritable: StateFlow<Boolean> = when (info.valueSource) {
        ValueSource.Hardware -> dependencies
            .settingsAllowEditHardwareProperties
            .remindType<SettingsAllowEditHardwareProperties>()
            .value
            .value

        ValueSource.Calculated -> dependencies
            .settingsAllowEditCalculatedProperties
            .remindType<SettingsAllowEditCalculatedProperties>()
            .value
            .value

        ValueSource.Manual ->
            MutableStateFlow(true)
    }

    private val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>> =
        run {
            val sendJob: MutableStateFlow<Job?> = MutableStateFlow(null)
            val retained = when (info.type.viewType) {
                is PropertyViewType.Events -> false
                is PropertyViewType.State -> true
            }
            isWritable
                .scopedInState(scope)
                .mapState(scope) { (isWritableScope, isWritable) ->
                    when (isWritable) {
                        false -> ValueOrReadOnly.ReadOnly
                        true -> ValueOrReadOnly.Value(
                            value = sendJob.mapState(isWritableScope) { sendJobOrNull ->
                                when (sendJobOrNull) {
                                    null -> ValueOrCancel.Value { data: T ->
                                        sendJob.value = isWritableScope.launch {
                                            delay(1000)
                                            dependencies.connection.publishTyped(
                                                topic = publishTopic,
                                                retained = retained,
                                                payload = data,
                                                serializer = info.type.mapper.mapper.reverse,
                                            )
                                            sendJob.value = null
                                        }
                                    }

                                    else -> ValueOrCancel.Cancel(
                                        cancel = {
                                            sendJob.value = null
                                            sendJobOrNull.cancel()
                                        },
                                    )
                                }
                            }
                        )
                    }
                }
        }

    override val value: ValueModel<T> = when (val viewType = info.type.viewType) {
        is PropertyViewType.Events -> PropertyModelEventsDelegate.createEventsModel(
            scope = scope,
            type = viewType,
            dependencies = dependencies.events(),
            mapper = info.type.mapper,
            sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
            toEventsSubscriber = ToEventsSubscriber.create(
                topic = subscribeTopic,
                connection = dependencies.connection,
            ),
        )

        is PropertyViewType.State -> createStateModel(
            viewType = viewType,
            mapper = info.type.mapper,
            stateDependencies = dependencies.state(),
        )
    }

    override val source: ValueSource
        get() = info.valueSource

    @Suppress("UNCHECKED_CAST")
    private fun createStateModel(
        viewType: PropertyViewType.State<T>,
        mapper: SerializableMapper<MqttData, *>,
        stateDependencies: StateModel.Dependencies,
    ): StateModel<T> = createStateModelTyped(
        viewType = viewType,
        mapper = mapper as SerializableMapper<MqttData, T>,
        stateDependencies = stateDependencies,
    )

    private fun createStateModelTyped(
        viewType: PropertyViewType.State<T>,
        mapper: SerializableMapper<MqttData, T>,
        stateDependencies: StateModel.Dependencies,
    ): StateModel<T> = StateModelImpl(
        scope = scope,
        dependencies = stateDependencies,
        skeleton = skeleton.value.getOrPutTyped { StateModel.Skeleton() },
        type = viewType,
        valueOrParsingErrorOrNull = dependencies
            .connection
            .subscribeToState(
                topic = subscribeTopic,
            )
            .mapState(scope) { mqttDataOrNull ->
                mqttDataOrNull?.let { mqttData ->
                    Either.catch { mapper.mapper.direct(mqttData) }
                }
            },
        isWritable = isWritable,
        sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
    )
}