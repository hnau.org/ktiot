package hnau.ktiot.client.model.property.impl.state.value

import hnau.common.color.RGBABytes
import hnau.common.color.getStringMapper
import hnau.common.kotlin.coroutines.mapState
import hnau.ktiot.client.model.property.api.value.state.ready.RGBViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

data class RGBViewModelImpl(
    override val scope: CoroutineScope,
    override val color: StateFlow<RGBABytes>,
) : RGBViewModel {

    override val hex: StateFlow<String> = color.mapState(scope) { color ->
        RGBABytes
            .getStringMapper(useAlpha = false)
            .reverse(color)
    }

    companion object {

        val Factory = ViewModel.Factory { scope, _, value, _ ->
            RGBViewModelImpl(
                scope = scope,
                color = value,
            )
        }
    }
}