package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.coroutines.Stickable
import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.mapStateLite
import hnau.common.kotlin.coroutines.nullable
import hnau.common.kotlin.coroutines.predeterminated
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.coroutines.stateFlow
import hnau.common.kotlin.coroutines.stick
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.map
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.model.property.api.value.state.ready.OptionActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

data class OptionActiveModelImpl<T>(
    private val scope: CoroutineScope,
    private val dependencies: OptionActiveModel.Dependencies,
    private val contentType: PropertyViewType.State<T>,
    private val value: StateFlow<Option<T>>,
    private val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Option<T>) -> Unit>>>>,
    private val skeleton: OptionActiveModel.Skeleton,
) : OptionActiveModel<T> {

    private var isWaitingNewValue = false

    init {
        scope.launch {
            value.collect {
                if (isWaitingNewValue) {
                    isWaitingNewValue = false
                    skeleton.isInitializing.value = false
                }
            }
        }
    }

    override val sendIsSomeOrNullOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>>> =
        sendOrCancelOrReadOnly
            .scopedInState(scope)
            .mapState(scope) { (sendOrCancelFlowScope, sendOrCancelFlowOrReadOnly) ->
                sendOrCancelFlowOrReadOnly.map { sendOrCancelFlow ->
                    sendOrCancelFlow.mapState(sendOrCancelFlowScope) { sendOrCancel ->
                        sendOrCancel.map { send ->
                            { some: Boolean ->
                                when (some) {
                                    false -> {
                                        when (skeleton.isInitializing.value) {
                                            false -> send(None)
                                            true -> skeleton.isInitializing.value = false
                                        }
                                    }

                                    true -> skeleton.isInitializing.value = true
                                }
                            }
                        }
                    }
                }
            }

    private val sendSomeOrNullOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>> =
        sendOrCancelOrReadOnly
            .scopedInState(scope)
            .mapState(scope) { (sendOrCancelFlowOrNullScope, sendOrCancelFlowOrReadOnly) ->
                sendOrCancelFlowOrReadOnly.map { sendOrCancelFlow ->
                    sendOrCancelFlow.mapState(sendOrCancelFlowOrNullScope) { sendOrCancel ->
                        sendOrCancel.map { sendOption ->
                            { value: T -> sendOption(Some(value)) }
                        }
                    }
                }
            }

    override val state: StateFlow<Either<EditModel<T>, ActiveModel<T>>?> = value
        .scopedInState(scope)
        .flatMapState(scope) { (valueOrNullScope, valueOrNone) ->
            when (valueOrNone) {
                None -> combineState(
                    scope = valueOrNullScope,
                    a = skeleton.isInitializing,
                    b = sendSomeOrNullOrReadOnly,
                ) { isInitializing, sendSomeOrNullFlowOrReadOnly ->
                    val sendSomeOrNullFlowOrNull = when (sendSomeOrNullFlowOrReadOnly) {
                        ValueOrReadOnly.ReadOnly -> null
                        is ValueOrReadOnly.Value -> sendSomeOrNullFlowOrReadOnly.value
                    }
                    sendSomeOrNullFlowOrNull
                        ?.takeIf { isInitializing }
                        ?.let { sendSomeOrNull: StateFlow<ValueOrCancel<(T) -> Unit>> ->
                            Either.Left(sendSomeOrNull)
                        }
                }

                is Some -> MutableStateFlow(Either.Right(valueOrNone.value))
            }
        }
        .stick(
            scope = scope,
            createStickable = { stickableScope, initOrValueOrNull ->
                when (initOrValueOrNull) {
                    is Either.Right -> Stickable.stateFlow(
                        initial = initOrValueOrNull.value,
                        tryUseNext = { nextData ->
                            when (nextData) {
                                null, is Either.Left -> None
                                is Either.Right -> Some(nextData.value)
                            }
                        },
                        createResult = { values ->
                            Either.Right(
                                ActiveModel[contentType].create(
                                    value = values,
                                    scope = stickableScope,
                                    dependencies = dependencies.active,
                                    skeleton = skeleton::active.toAccessor(),
                                    sendOrCancelOrReadOnly = sendSomeOrNullOrReadOnly,
                                )
                            )
                        },
                    )

                    is Either.Left -> Stickable.predeterminated(
                        Either.Left(
                            run {
                                val sendOrCancel: StateFlow<ValueOrCancel<(T) -> Unit>> =
                                    initOrValueOrNull
                                        .value
                                        .mapState(stickableScope) { sendOrCancel ->
                                            sendOrCancel.map { send ->
                                                { valueToInit: T ->
                                                    isWaitingNewValue = true
                                                    send(valueToInit)
                                                }
                                            }
                                        }
                                EditModelImpl(
                                    scope = stickableScope,
                                    dependencies = dependencies.edit(),
                                    skeleton = skeleton::init.toAccessor(),
                                    passiveModelFactory = PassiveModel[contentType],
                                    sendOrCancel = sendOrCancel,
                                    cancel = { skeleton.isInitializing.value = false },
                                    tryGetCurrentValue = { None },
                                )
                            }
                        )
                    )

                    null -> Stickable.nullable
                }
            },
        )

    override val isSome: StateFlow<Boolean> = state
        .mapStateLite { content -> content != null }

    companion object {

        fun <T> factory(
            contentType: PropertyViewType.State<T>,
        ) =
            ActiveModel.Factory { scope, dependencies, value, sendOrCancelOrReadOnly, skeletonOrInit ->
                OptionActiveModelImpl(
                    scope = scope,
                    dependencies = dependencies.option(dependencies),
                    contentType = contentType,
                    value = value,
                    sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
                    skeleton = skeletonOrInit
                        .shrinkType<_, OptionActiveModel.Skeleton>()
                        .getOrInit { OptionActiveModel.Skeleton() },
                )
            }
    }
}

internal fun <T> StateModelFactories.Companion.option(
    contentType: PropertyViewType.State<T>,
): StateModelFactories<Option<T>> = create(
    active = OptionActiveModelImpl.factory(
        contentType = contentType,
    ),
    createInitialValue = { None },
)