package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.Option
import arrow.core.Some
import arrow.core.getOrElse
import hnau.common.app.toEditingString
import hnau.common.color.HSLFloats
import hnau.common.color.RGBABytes
import hnau.common.color.RGBFloats
import hnau.common.color.getStringMapper
import hnau.common.color.gradient.Gradient
import hnau.common.color.gradient.create
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.mapStateLite
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.ifNull
import hnau.common.kotlin.mapper.Mapper
import hnau.common.kotlin.mapper.plus
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassivePage
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassivePageModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

data class RGBPassiveModelImpl(
    private val scope: CoroutineScope,
    private val skeleton: RGBPassiveModel.Skeleton,
    private val tryComplete: () -> Unit,
) : RGBPassiveModel {

    private data class Page(
        val page: RGBPassivePage<*>,
        val currentColorOrNone: StateFlow<Option<RGBABytes>>,
    )

    private val page: StateFlow<Page> = skeleton
        .selectedPage
        .scopedInState(scope)
        .mapState(scope) { (pageScope, pageSkeleton) ->
            when (pageSkeleton) {
                is RGBPassivePage.HEX.Skeleton -> createTextFieldPage(
                    scope = pageScope,
                    skeleton = pageSkeleton.model,
                    prefix = "#",
                    tryCreateColor = { text ->
                        Option.catch {
                            bytesStringToColorMapper.direct(text)
                        }
                    },
                ) { delegate ->
                    RGBPassivePage.HEX(delegate)
                }

                is RGBPassivePage.RGB.Skeleton -> createSlidersPage(
                    scope = pageScope,
                    value = pageSkeleton.rgb,
                    valueToColor = RGBFloats.rgbaBytesMapper.reverse,
                    sliders = listOf(
                        Slider(
                            prefix = "R",
                            extractValue = RGBFloats::r,
                            applyValue = { copy(r = it) },
                        ),
                        Slider(
                            prefix = "G",
                            extractValue = RGBFloats::g,
                            applyValue = { copy(g = it) },
                        ),
                        Slider(
                            prefix = "B",
                            extractValue = RGBFloats::b,
                            applyValue = { copy(b = it) },
                        ),
                    )
                ) { delegate ->
                    RGBPassivePage.RGB(delegate)
                }

                is RGBPassivePage.HSL.Skeleton -> createSlidersPage(
                    scope = pageScope,
                    value = pageSkeleton.hsl,
                    valueToColor = rgbaBytesToHSLFloatsMapper.reverse,
                    sliders = listOf(
                        Slider(
                            prefix = "H",
                            extractValue = HSLFloats::h,
                            applyValue = { copy(h = it) },
                        ),
                        Slider(
                            prefix = "S",
                            extractValue = HSLFloats::s,
                            applyValue = { copy(s = it) },
                        ),
                        Slider(
                            prefix = "L",
                            extractValue = HSLFloats::l,
                            applyValue = { copy(l = it) },
                        ),
                    )
                ) { delegate ->
                    RGBPassivePage.HSL(delegate)
                }
            }
        }

    override val selectedTabWithPage: StateFlow<Pair<RGBPassiveModel.Tab, RGBPassivePageModel>> =
        page.mapState(scope) { currentPage -> currentPage.page.tab to currentPage.page.model }

    override fun selectTab(tab: RGBPassiveModel.Tab) {
        val color = page.value.currentColorOrNone.value.getOrElse { RGBABytes.Black }
        skeleton.selectedPage.value = createPageSkeleton(
            tab = tab,
            initialColor = color,
        )
    }

    private data class Slider<T>(
        val prefix: String,
        val extractValue: (T) -> Float,
        val applyValue: T.(Float) -> T,
    )

    private inline fun <T> createSlidersPage(
        scope: CoroutineScope,
        value: MutableStateFlow<T>,
        crossinline valueToColor: (T) -> RGBABytes,
        sliders: List<Slider<T>>,
        createPage: (RGBPassivePageModel.Sliders) -> RGBPassivePage<RGBPassivePageModel.Sliders>,
    ): Page = Page(
        page = createPage(
            RGBPassivePageModel.Sliders(
                sliders = sliders.map { slider ->
                    RGBPassivePageModel.Sliders.Slider(
                        prefix = slider.prefix,
                        state = value.mapState(scope) { currentValue ->
                            RGBPassivePageModel.Sliders.Slider.State(
                                fraction = slider.extractValue(currentValue),
                                gradient = Gradient.create(
                                    stops = GradientPoints,
                                ) { fraction ->
                                    val gradientValue = slider.applyValue(currentValue, fraction)
                                    valueToColor(gradientValue)
                                },
                            )
                        },
                        updateFraction = { newFraction ->
                            value.value = slider.applyValue(value.value, newFraction)
                        }
                    )
                }
            )
        ),
        currentColorOrNone = value.mapState(scope) { Some(valueToColor(it)) },
    )

    private inline fun createTextFieldPage(
        scope: CoroutineScope,
        prefix: String,
        skeleton: RGBPassivePageModel.TextField.Skeleton,
        crossinline tryCreateColor: (String) -> Option<RGBABytes>,
        createPage: (RGBPassivePageModel.TextField) -> RGBPassivePage<RGBPassivePageModel.TextField>,
    ): Page = Page(
        page = createPage(
            RGBPassivePageModel.TextField(
                value = skeleton.value,
                prefix = prefix,
            ),
        ),
        currentColorOrNone = skeleton.value.mapState(scope) { editingString ->
            tryCreateColor(editingString.text)
        },
    )

    override fun tryComplete() = tryComplete.invoke()

    override val currentOrNone: StateFlow<Option<RGBABytes>> = page
        .flatMapState(scope) { currentPage -> currentPage.currentColorOrNone }

    override val isError: StateFlow<Boolean> =
        currentOrNone.mapStateLite(Option<RGBABytes>::isNone)


    companion object {

        private val rgbaBytesToHSLFloatsMapper: Mapper<RGBABytes, HSLFloats> =
            RGBFloats.rgbaBytesMapper + HSLFloats.rgbFloatsMapper

        private const val GradientPoints: Int = 32

        private val bytesStringToColorMapper: Mapper<String, RGBABytes> = Mapper<String, String>(
            direct = { bytes -> "#$bytes" },
            reverse = { hex -> hex.drop(1) }
        ) + RGBABytes.getStringMapper(useAlpha = false)

        private fun createPageSkeleton(
            tab: RGBPassiveModel.Tab,
            initialColor: RGBABytes,
        ): RGBPassivePage.Skeleton = when (tab) {
            RGBPassiveModel.Tab.HEX -> RGBPassivePage.HEX.Skeleton(
                model = RGBPassivePageModel.TextField.Skeleton(
                    value = MutableStateFlow(
                        bytesStringToColorMapper
                            .reverse(initialColor)
                            .toEditingString(),
                    )
                )
            )

            RGBPassiveModel.Tab.RGB -> RGBPassivePage.RGB.Skeleton(
                MutableStateFlow(RGBFloats.rgbaBytesMapper.direct(initialColor))
            )

            RGBPassiveModel.Tab.HSL -> RGBPassivePage.HSL.Skeleton(
                MutableStateFlow(rgbaBytesToHSLFloatsMapper.direct(initialColor))
            )
        }


        val Factory =
            PassiveModel.Factory<RGBABytes> { scope, _, skeleton, tryGetInitialValue, tryComplete ->
                RGBPassiveModelImpl(
                    scope = scope,
                    skeleton = skeleton
                        .shrinkType<_, RGBPassiveModel.Skeleton>()
                        .getOrInit {
                            RGBPassiveModel.Skeleton(
                                selectedPage = MutableStateFlow(
                                    createPageSkeleton(
                                        tab = RGBPassiveModel.Tab.entries.first(),
                                        initialColor = tryGetInitialValue()
                                            .getOrNull()
                                            .ifNull { RGBABytes.Black },
                                    )
                                ),
                            )
                        },
                    tryComplete = tryComplete,
                )
            }
    }
}