package hnau.ktiot.client.model.property.impl.state.value

import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewBasedOnActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

internal class ViewBasedOnActiveModelImpl<T>(
    scope: CoroutineScope,
    value: StateFlow<T>,
    skeleton: ViewBasedOnActiveModel.Skeleton,
    dependencies: ViewBasedOnActiveModel.Dependencies,
    active: ActiveModel.Factory<T>,
) : ViewBasedOnActiveModel<T> {

    override val model: ActiveModel<T> = active.create(
        scope = scope,
        dependencies = dependencies.active,
        value = value,
        skeleton = skeleton::active.toAccessor(),
        sendOrCancelOrReadOnly = MutableStateFlow(ValueOrReadOnly.ReadOnly),
    )
}


internal fun <T> ActiveModel.Factory<T>.toView(): ViewModel.Factory<T> =
    ViewModel.Factory { scope, dependencies, value, skeleton ->
        ViewBasedOnActiveModelImpl(
            scope = scope,
            value = value,
            skeleton = skeleton
                .shrinkType<_, ViewBasedOnActiveModel.Skeleton>()
                .getOrInit { ViewBasedOnActiveModel.Skeleton() },
            dependencies = dependencies.basedOnActive(),
            active = this,
        )
    }