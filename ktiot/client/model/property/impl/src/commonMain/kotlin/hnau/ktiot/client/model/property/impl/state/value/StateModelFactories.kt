package hnau.ktiot.client.model.property.impl.state.value

import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType

data class StateModelFactories<T>(
    val active: ActiveModel.Factory<T>,
    val passive: PassiveModel.Factory<T>,
    val view: ViewModel.Factory<T>,
) {

    companion object {

        fun <T> create(
            passive: PassiveModel.Factory<T>,
            view: ViewModel.Factory<T>,
        ): StateModelFactories<T> = StateModelFactories(
            passive = passive,
            view = view,
            active = ActiveModel.fromPassiveAndView(
                passive = passive,
                view = view,
            ),
        )

        fun <T> create(
            active: ActiveModel.Factory<T>,
            createInitialValue: () -> T,
        ): StateModelFactories<T> = StateModelFactories(
            passive = active.toPassive(
                createInitialValue = createInitialValue,
            ),
            active = active,
            view = active.toView(),
        )

        operator fun <T> get(
            viewType: PropertyViewType.State<T>,
        ): StateModelFactories<T> = when (viewType) {
            is PropertyViewType.State.Enum -> enum(viewType)
            PropertyViewType.State.Flag -> flag
            PropertyViewType.State.RGB -> rgb
            is PropertyViewType.State.Fraction -> fraction(viewType)
            is PropertyViewType.State.Number -> number(viewType)
            is PropertyViewType.State.Option<*> -> option(viewType.contentType)
            PropertyViewType.State.Text -> text
            PropertyViewType.State.Timestamp -> timestamp
            is PropertyViewType.State.Timestamped<*> -> timestamped(viewType.contentType)
        } as StateModelFactories<T>
    }
}