package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.MutableAccessor
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrDisabled
import hnau.ktiot.client.model.common.ornull.map
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class EditModelImpl<out T>(
    scope: CoroutineScope,
    dependencies: EditModel.Dependencies,
    skeleton: MutableAccessor<Option<EditModel.Skeleton>>,
    passiveModelFactory: PassiveModel.Factory<T>,
    tryGetCurrentValue: () -> Option<T>,
    sendOrCancel: StateFlow<ValueOrCancel<(T) -> Unit>>,
    cancel: () -> Unit,
) : EditModel<T> {

    override val passive: PassiveModel<T> = passiveModelFactory.create(
        scope = scope,
        dependencies = dependencies.passive(),
        skeleton = skeleton
            .getOrInit { EditModel.Skeleton() }::passive
            .toAccessor(),
        tryGetInitialValue = tryGetCurrentValue,
        tryComplete = ::tryComplete,
    )

    override val actions: StateFlow<ValueOrCancel<EditModel.Actions>> = sendOrCancel
        .scopedInState(scope)
        .mapState(scope) { (sendOrCancelScope, sendOrCancel) ->
            sendOrCancel.map { send ->
                EditModel.Actions(
                    cancel = cancel,
                    send = passive
                        .currentOrNone
                        .mapState(sendOrCancelScope) { currentOrNone ->
                            when (currentOrNone) {
                                None -> ValueOrDisabled.Disabled
                                is Some -> ValueOrDisabled.Value {
                                    val newValue = currentOrNone.value
                                    val valueToSend =
                                        when (val currentValue = tryGetCurrentValue()) {
                                            None -> Some(newValue)
                                            is Some -> when (newValue) {
                                                currentValue.value -> None
                                                else -> Some(newValue)
                                            }
                                        }
                                    when (valueToSend) {
                                        None -> cancel()
                                        is Some -> send(valueToSend.value)
                                    }
                                }
                            }
                        }
                )
            }
        }

    private fun tryComplete() {
        val complete = when (val valueOrCancel = actions.value) {
            is ValueOrCancel.Cancel -> null
            is ValueOrCancel.Value -> when (val valueOrDisabled = valueOrCancel.value.send.value) {
                ValueOrDisabled.Disabled -> null
                is ValueOrDisabled.Value -> valueOrDisabled.value
            }
        }
        complete?.invoke()
    }
}