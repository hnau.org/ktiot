package hnau.ktiot.client.model.property.impl.state

import arrow.core.Either
import arrow.core.identity
import arrow.core.toOption
import hnau.common.kotlin.coroutines.Stickable
import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.flatMapStateLite
import hnau.common.kotlin.coroutines.predeterminated
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.coroutines.stateFlow
import hnau.common.kotlin.coroutines.stick
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.state.InitBeforeFirstValueModel
import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.model.property.api.value.state.StateStateModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class StateModelImpl<T>(
    scope: CoroutineScope,
    private val skeleton: StateModel.Skeleton,
    private val dependencies: StateModel.Dependencies,
    private val isWritable: StateFlow<Boolean>,
    private val type: PropertyViewType.State<T>,
    private val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>>,
    valueOrParsingErrorOrNull: StateFlow<Either<Throwable, T>?>,
) : StateModel<T> {

    private fun setInitBeforeFirstValue(
        initBeforeFirstValue: Boolean,
    ) {
        skeleton.isInitializingBeforeFirstValue.value = initBeforeFirstValue
    }

    private val initBeforeFirstValueSendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>> =
        combineState(
            scope = scope,
            a = this.sendOrCancelOrReadOnly,
            b = combineState(
                scope = scope,
                a = skeleton.isInitializingBeforeFirstValue,
                b = valueOrParsingErrorOrNull,
            ) { isInitializingBeforeFirstValue, valueOrParsingErrorOrNull ->
                when {
                    valueOrParsingErrorOrNull != null -> false
                    !isInitializingBeforeFirstValue -> false
                    else -> true
                }
            }
        ) { sendOrCancelOrReadOnly: ValueOrReadOnly<StateFlow<ValueOrCancel<(T) -> Unit>>>, isInitializingBeforeFirstValue ->
            when (isInitializingBeforeFirstValue) {
                true -> sendOrCancelOrReadOnly
                false -> ValueOrReadOnly.ReadOnly
            }
        }

    override val state: StateFlow<StateStateModel<T>> = initBeforeFirstValueSendOrCancelOrReadOnly
        .scopedInState(scope)
        .flatMapState(scope)
        { (initBeforeFirstValueSendOrCancelOrReadOnlyScope, initBeforeFirstValueSendOrCancelOrReadOnly) ->
            when (initBeforeFirstValueSendOrCancelOrReadOnly) {
                ValueOrReadOnly.ReadOnly -> valueOrParsingErrorOrNull
                    .stick(
                        scope = initBeforeFirstValueSendOrCancelOrReadOnlyScope,
                        createStickable = { stickableScope, initialValue: Either<Throwable, T>? ->
                            when (initialValue) {
                                null -> Stickable.predeterminated(
                                    MutableStateFlow(
                                        ReadingFirstValueModelImpl(
                                            scope = stickableScope,
                                            isWritable = isWritable,
                                            switchToInit = { setInitBeforeFirstValue(true) },
                                        )
                                    ),
                                )

                                else -> Stickable.stateFlow(
                                    initial = initialValue,
                                    tryUseNext = Either<Throwable, T>?::toOption,
                                    createResult = { valueOrParsingErrorFlow ->
                                        StateModelReadyDelegate.createReadyOrParseErrorModel(
                                            scope = stickableScope,
                                            type = type,
                                            skeleton = skeleton::state.toAccessor()
                                                .shrinkType<_, ReadyModel.Skeleton>(),
                                            dependencies = dependencies,
                                            value = valueOrParsingErrorFlow,
                                            sendOrCancelOrReadOnly = this.sendOrCancelOrReadOnly,
                                        )
                                    },
                                )
                            }
                        },
                    )
                    .flatMapStateLite(::identity)

                is ValueOrReadOnly.Value -> MutableStateFlow(
                    InitBeforeFirstValueModelImpl(
                        scope = initBeforeFirstValueSendOrCancelOrReadOnlyScope,
                        dependencies = dependencies.initBeforeFirstValue(),
                        skeleton = skeleton::state
                            .toAccessor()
                            .shrinkType<_, InitBeforeFirstValueModel.Skeleton>()
                            .getOrInit { InitBeforeFirstValueModel.Skeleton() },
                        sendOrCancel = initBeforeFirstValueSendOrCancelOrReadOnly.value,
                        cancel = { setInitBeforeFirstValue(false) },
                        type = type,
                    )
                )
            }
        }
}