package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.raise.option
import hnau.common.kotlin.Timestamped
import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.mapStateLite
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.timestamp.TimestampEditModel
import hnau.ktiot.client.model.common.timestamp.TimestampViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class TimestampedPassiveModelImpl<T>(
    scope: CoroutineScope,
    dependencies: TimestampedPassiveModel.Dependencies,
    private val skeleton: TimestampedPassiveModel.Skeleton,
    contentType: PropertyViewType.State<T>,
    tryGetInitialValue: () -> Option<Timestamped<T>>,
    private val tryComplete: () -> Unit,
) : TimestampedPassiveModel<T> {

    private val initialValueOrNone: Option<Timestamped<T>>
            by lazy { tryGetInitialValue() }

    private val timestamp: TimestampEditModel = TimestampEditModel(
        scope = scope,
        skeleton = skeleton::timestamp
            .toAccessor()
            .getOrInit {
                when (val initialOrNone = initialValueOrNone) {
                    None ->
                        TimestampEditModel.Skeleton()

                    is Some ->
                        TimestampEditModel.Skeleton.create(
                            instant = initialOrNone.value.timestamp,
                        )
                }
            },
    )

    private val value: PassiveModel<T> = PassiveModel[contentType].create(
        scope = scope,
        dependencies = dependencies.passive,
        skeleton = skeleton::value.toAccessor(),
        tryGetInitialValue = { initialValueOrNone.map(Timestamped<T>::value) },
        tryComplete = tryComplete,
    )

    override val state: StateFlow<TimestampedPassiveModel.State<T>> = skeleton
        .selectedTab
        .mapState(scope) { tab ->
            when (tab) {
                TimestampedPassiveModel.Tab.Timestamp ->
                    TimestampedPassiveModel.State.Timestamp(timestamp)

                TimestampedPassiveModel.Tab.Value ->
                    TimestampedPassiveModel.State.Value(value)
            }
        }


    override val currentOrNone: StateFlow<Option<Timestamped<T>>> = combineState(
        scope,
        timestamp.timestampOrNone,
        value.currentOrNone,
    ) { timestampOrNone, valueOrNone ->
        option {
            Timestamped(
                timestamp = timestampOrNone.bind(),
                value = valueOrNone.bind()
            )
        }
    }

    override fun selectTab(
        newTab: TimestampedPassiveModel.Tab,
    ) {
        skeleton.selectedTab.value = newTab
    }

    override fun tryComplete() {
        tryComplete.invoke()
    }

    companion object {

        fun <T> factory(
            contentType: PropertyViewType.State<T>,
        ): PassiveModel.Factory<Timestamped<T>> =
            PassiveModel.Factory { scope, dependencies, skeleton, tryGetInitialValue, tryComplete ->
                TimestampedPassiveModelImpl(
                    scope = scope,
                    dependencies = dependencies.timestamped(dependencies),
                    skeleton = skeleton
                        .shrinkType<_, TimestampedPassiveModel.Skeleton>()
                        .getOrInit { TimestampedPassiveModel.Skeleton() },
                    contentType = contentType,
                    tryGetInitialValue,
                    tryComplete,
                )
            }
    }
}

internal class TimestampedViewModelImpl<T>(
    scope: CoroutineScope,
    dependencies: TimestampedViewModel.Dependencies,
    skeleton: TimestampedViewModel.Skeleton,
    value: StateFlow<Timestamped<T>>,
    contentType: PropertyViewType.State<T>,
) : TimestampedViewModel<T> {

    override val timestamp: TimestampViewModel = TimestampViewModel(
        scope = scope,
        dependencies = dependencies.timestamp(),
        value = value.mapStateLite(Timestamped<T>::timestamp),
    )
    override val value: ViewModel<T> = StateModelFactories[contentType].view.createViewModel(
        scope = scope,
        dependencies = dependencies.view,
        value = value.mapStateLite(Timestamped<T>::value),
        skeleton = skeleton::value.toAccessor(),
    )

    companion object {

        fun <T> factory(
            contentType: PropertyViewType.State<T>,
        ): ViewModel.Factory<Timestamped<T>> =
            ViewModel.Factory { scope, dependencies, value, skeleton ->
                TimestampedViewModelImpl(
                    scope = scope,
                    dependencies = dependencies.timestampedView(dependencies),
                    skeleton = skeleton
                        .shrinkType<_, TimestampedViewModel.Skeleton>()
                        .getOrInit { TimestampedViewModel.Skeleton() },
                    value = value,
                    contentType = contentType,
                )
            }
    }
}

internal fun <T> StateModelFactories.Companion.timestamped(
    contentType: PropertyViewType.State<T>,
): StateModelFactories<Timestamped<T>> = create(
    passive = TimestampedPassiveModelImpl.factory(contentType),
    view = TimestampedViewModelImpl.factory(contentType),
)