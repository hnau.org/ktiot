package hnau.ktiot.client.model.property.impl.state.value

import hnau.common.kotlin.coroutines.combineState
import hnau.common.kotlin.coroutines.flatMapState
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EnumActiveModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

data class EnumActiveModelImpl(
    private val scope: CoroutineScope,
    private val value: StateFlow<String>,
    private val sendOrCancelOrReadOnly: StateFlow<ValueOrReadOnly<StateFlow<ValueOrCancel<(String) -> Unit>>>>,
    private val skeleton: EnumActiveModel.Skeleton,
    private val viewType: PropertyViewType.State.Enum,
) : EnumActiveModel {

    private sealed interface SendState {

        data object Disabled : SendState

        data class Closed(
            val open: () -> Unit,
        ) : SendState

        data class Opened(
            val sendOrCancel: StateFlow<ValueOrCancel<(String) -> Unit>>,
        ) : SendState
    }

    private val sendState: StateFlow<SendState> = sendOrCancelOrReadOnly
        .scopedInState(scope)
        .flatMapState(scope) { (sendOrCancelOrDisabledScope, sendOrCancelOrDisabled) ->
            when (sendOrCancelOrDisabled) {
                ValueOrReadOnly.ReadOnly -> MutableStateFlow(SendState.Disabled)
                is ValueOrReadOnly.Value -> {
                    val sendOrCancelFlow: StateFlow<ValueOrCancel<(String) -> Unit>> =
                        sendOrCancelOrDisabled.value

                    val isOpenedOrSending = combineState(
                        scope = sendOrCancelOrDisabledScope,
                        a = skeleton.isOpened,
                        b = sendOrCancelFlow,
                    ) { isOpened, sendOrCancel ->
                        isOpened || when (sendOrCancel) {
                            is ValueOrCancel.Cancel -> true
                            is ValueOrCancel.Value -> false
                        }
                    }

                    isOpenedOrSending
                        .scopedInState(sendOrCancelOrDisabledScope)
                        .mapState(sendOrCancelOrDisabledScope) { (isOpenedOrSendingScope, isOpenedOrSending) ->
                            when (isOpenedOrSending) {
                                false -> SendState.Closed(
                                    open = { skeleton.isOpened.value = true },
                                )

                                true -> SendState.Opened(
                                    sendOrCancel = sendOrCancelFlow
                                        .mapState(isOpenedOrSendingScope) { sendOrCancel ->
                                            when (sendOrCancel) {
                                                is ValueOrCancel.Cancel -> ValueOrCancel.Cancel {
                                                    skeleton.isOpened.value = true
                                                    sendOrCancel.cancel()
                                                }

                                                is ValueOrCancel.Value -> ValueOrCancel.Value { key: String ->
                                                    sendOrCancel.value(key)
                                                    skeleton.isOpened.value = false
                                                }
                                            }
                                        }
                                )
                            }
                        }
                }
            }
        }

    override val state: StateFlow<EnumActiveModel.State> = combineState(
        scope = scope,
        a = value,
        b = sendState,
    )
        .scopedInState(scope)
        .mapState(scope) { (stateScope, selectedKeyWithSendState) ->
            val (selectedKey, sendState) = selectedKeyWithSendState

            fun createClosed(
                open: (() -> Unit)? = null,
            ): EnumActiveModel.State.Closed = EnumActiveModel.State.Closed(
                selectedVariant = viewType.variants.find { it.key == selectedKey }!!,
                open = open,
            )

            when (sendState) {
                SendState.Disabled ->
                    createClosed()

                is SendState.Closed ->
                    createClosed(open = sendState.open)

                is SendState.Opened -> EnumActiveModel.State.Opened(
                    items = viewType.variants.map { variant ->
                        EnumActiveModel.State.Opened.Item(
                            variant = variant,
                            state = sendState
                                .sendOrCancel
                                .mapState(stateScope) { sendOrCancel ->
                                    when (sendOrCancel) {
                                        is ValueOrCancel.Cancel -> when (skeleton.sendingKey) {
                                            variant.key -> EnumActiveModel.State.Opened.Item.State.Selecting(
                                                cancel = sendOrCancel.cancel
                                            )

                                            else -> when (variant.key) {
                                                selectedKey -> EnumActiveModel.State.Opened.Item.State.Selected(
                                                    closeIfNotSelecting = null
                                                )

                                                else -> EnumActiveModel.State.Opened.Item.State.NotSelected(
                                                    selectIfNotSelecting = null,
                                                )
                                            }
                                        }


                                        is ValueOrCancel.Value ->
                                            when (variant.key) {
                                                selectedKey -> EnumActiveModel.State.Opened.Item.State.Selected(
                                                    closeIfNotSelecting = {
                                                        skeleton.isOpened.value = false
                                                    },
                                                )

                                                else -> EnumActiveModel.State.Opened.Item.State.NotSelected(
                                                    selectIfNotSelecting = {
                                                        skeleton.sendingKey = variant.key
                                                        sendOrCancel.value(variant.key)
                                                    },
                                                )
                                            }
                                    }
                                }
                        )
                    }
                )
            }
        }

    companion object {


        fun factory(viewType: PropertyViewType.State.Enum): ActiveModel.Factory<String> =
            ActiveModel.Factory { scope, _, value, sendOrCancelOrReadOnly, skeleton ->
                EnumActiveModelImpl(
                    scope = scope,
                    value = value,
                    sendOrCancelOrReadOnly = sendOrCancelOrReadOnly,
                    skeleton = skeleton
                        .shrinkType<_, EnumActiveModel.Skeleton>()
                        .getOrInit { EnumActiveModel.Skeleton() },
                    viewType = viewType,
                )
            }
    }
}


internal fun StateModelFactories.Companion.enum(
    viewType: PropertyViewType.State.Enum,
): StateModelFactories<String> = create(
    active = EnumActiveModelImpl.factory(viewType),
    createInitialValue = { viewType.variants.head.key },
)