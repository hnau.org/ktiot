package hnau.ktiot.client.model.property.impl.state.value

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveBasedOnActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

internal class PassiveBasedOnActiveModelImpl<T>(
    scope: CoroutineScope,
    skeleton: PassiveBasedOnActiveModel.Skeleton<T>,
    dependencies: PassiveBasedOnActiveModel.Dependencies,
    active: ActiveModel.Factory<T>,
) : PassiveBasedOnActiveModel<T> {

    override val model: ActiveModel<T> = active.create(
        scope = scope,
        dependencies = dependencies.active,
        value = skeleton.currentValue,
        sendOrCancelOrReadOnly = MutableStateFlow(
            ValueOrReadOnly.Value(
                value = MutableStateFlow(
                    ValueOrCancel.Value(
                        value = skeleton.currentValue::value::set,
                    ),
                ),
            ),
        ),
        skeleton = skeleton::activeModelSkeleton.toAccessor(),
    )


    override val currentOrNone: StateFlow<Option<T>> =
        skeleton.currentValue.mapState(scope, ::Some)
}

internal fun <T> ActiveModel.Factory<T>.toPassive(
    createInitialValue: () -> T,
): PassiveModel.Factory<T> =
    PassiveModel.Factory { scope, dependencies, skeleton, tryGetInitialValue, _ ->
        PassiveBasedOnActiveModelImpl(
            scope = scope,
            skeleton = skeleton
                .shrinkType<_, PassiveBasedOnActiveModel.Skeleton<T>>()
                .getOrInit {
                    PassiveBasedOnActiveModel.Skeleton(
                        initialValue = when (val initialValue = tryGetInitialValue()) {
                            None -> createInitialValue()
                            is Some -> initialValue.value
                        },
                    )
                },
            active = this,
            dependencies = dependencies.basedOnActive(),
        )
    }