package hnau.ktiot.client.model.property.impl.state

import arrow.core.None
import hnau.common.kotlin.toAccessor
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.property.api.value.state.InitBeforeFirstValueModel
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.impl.state.value.EditModelImpl
import hnau.ktiot.client.model.property.impl.state.value.get
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class InitBeforeFirstValueModelImpl<T>(
    scope: CoroutineScope,
    skeleton: InitBeforeFirstValueModel.Skeleton,
    dependencies: InitBeforeFirstValueModel.Dependencies,
    type: PropertyViewType.State<T>,
    sendOrCancel: StateFlow<ValueOrCancel<(T) -> Unit>>,
    cancel: () -> Unit,
) : InitBeforeFirstValueModel<T> {

    override val delegate: EditModel<T> = EditModelImpl(
        scope = scope,
        dependencies = dependencies.edit(dependencies.active()),
        skeleton = skeleton::edit.toAccessor(),
        tryGetCurrentValue = { None },
        passiveModelFactory = PassiveModel[type],
        sendOrCancel = sendOrCancel,
        cancel = cancel,
    )
}