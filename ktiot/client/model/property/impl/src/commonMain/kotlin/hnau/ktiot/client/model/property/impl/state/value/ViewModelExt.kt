package hnau.ktiot.client.model.property.impl.state.value

import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.scheme.property.presentation.PropertyViewType

operator fun <T> ViewModel.Companion.get(
    viewType: PropertyViewType.State<T>,
): ViewModel.Factory<T> = StateModelFactories[viewType].view