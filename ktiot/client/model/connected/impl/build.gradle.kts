plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:connected:api"))
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
