package hnau.ktiot.client.model.connected.impl

import arrow.core.Either
import hnau.common.app.goback.GoBackHandler
import hnau.common.app.goback.stateGoBackHandler
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.common.mqtt.subscribeToStateTyped
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.scheme.SchemeConstants
import hnau.ktiot.scheme.SchemeElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class ConnectedModelImpl(
    scope: CoroutineScope,
    private val skeleton: ConnectedModel.Skeleton,
    private val dependencies: ConnectedModel.Dependencies,
    private val mainStackModelFactory: MainStackModel.Factory,
) : ConnectedModel {

    override val state: StateFlow<ConnectedModel.State> = dependencies
        .connection
        .subscribeToStateTyped(
            scope = scope,
            topic = SchemeConstants.schemeTopic,
            deserializer = SchemeElement.serializer.direct,
        )
        .map { it.getOrNull() }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = null,
        )
        .scopedInState(scope)
        .mapState(scope) { (schemeScope, errorOrSchemeOrNull) ->
            when (errorOrSchemeOrNull) {
                null -> ConnectedModel.State.ReceivingScheme(
                    ReceivingSchemeModelImpl(
                        scope = schemeScope,
                        dependencies = dependencies.receiveScheme(),
                    )
                )

                is Either.Left -> ConnectedModel.State.UnableParseScheme(
                    UnableParseSchemeModelImpl(
                        scope = schemeScope,
                        error = errorOrSchemeOrNull.value,
                        dependencies = dependencies.unableParseScheme(),
                    )
                )

                is Either.Right -> ConnectedModel.State.MainStack(
                    mainStackModelFactory.createMainStackModel(
                        scope = schemeScope,
                        skeleton = skeleton::state
                            .toAccessor()
                            .shrinkType<_, ConnectedModel.State.Skeleton.MainStack>()
                            .getOrInit { ConnectedModel.State.Skeleton.MainStack() }
                            .mainStack,
                        dependencies = dependencies.mainStack(),
                        scheme = errorOrSchemeOrNull.value,
                    )
                )
            }
        }

    override val goBackHandler: GoBackHandler =
        state.stateGoBackHandler(scope)
}
