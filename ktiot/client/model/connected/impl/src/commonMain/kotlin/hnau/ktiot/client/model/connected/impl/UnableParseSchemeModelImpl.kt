package hnau.ktiot.client.model.connected.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.kotlin.coroutines.actionOrNullIfExecuting
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.connected.api.UnableParseSchemeModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class UnableParseSchemeModelImpl(
    scope: CoroutineScope,
    dependencies: UnableParseSchemeModel.Dependencies,
    override val error: Throwable,
) : UnableParseSchemeModel {

    override val logout: StateFlow<(() -> Unit)?> = actionOrNullIfExecuting(
        scope = scope,
        action = dependencies.doLogout.remindType<DoLogout>()::logout,
    )

    override val goBackHandler: GoBackHandler
        get() = logout
}
