package hnau.ktiot.client.model.connected.impl

import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.mainstack.api.MainStackModel

fun ConnectedModel.Factory.Companion.impl(
    mainStackModelFactory: MainStackModel.Factory,
): ConnectedModel.Factory = ConnectedModel.Factory { scope, skeleton, dependencies ->
    ConnectedModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        mainStackModelFactory = mainStackModelFactory,
    )
}