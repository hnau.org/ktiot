package hnau.ktiot.client.model.connected.api

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.mqtt.MqttConnection
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface ConnectedModel : GoBackHandlerProvider {

    sealed interface State : GoBackHandlerProvider {

        data class ReceivingScheme(
            val receivingScheme: ReceivingSchemeModel,
        ) : State, GoBackHandlerProvider by receivingScheme

        data class UnableParseScheme(
            val unableParseScheme: UnableParseSchemeModel,
        ) : State, GoBackHandlerProvider by unableParseScheme

        data class MainStack(
            val mainStack: MainStackModel,
        ) : State, GoBackHandlerProvider by mainStack

        @Serializable
        sealed interface Skeleton {

            @Serializable
            @SerialName("main_stack")
            data class MainStack(
                val mainStack: MainStackModel.Skeleton = MainStackModel.Skeleton(),
            ) : Skeleton
        }

    }


    @Serializable
    data class Skeleton(
        @Serializable(OptionSerializer::class)
        var state: Option<State.Skeleton> = None,
    )

    @Shuffle
    interface Dependencies {

        val connection: MqttConnection

        fun receiveScheme(): ReceivingSchemeModel.Dependencies

        fun unableParseScheme(): UnableParseSchemeModel.Dependencies

        fun mainStack(): MainStackModel.Dependencies
    }

    val state: StateFlow<State>

    fun interface Factory {

        fun createConnectedModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
        ): ConnectedModel

        companion object
    }
}