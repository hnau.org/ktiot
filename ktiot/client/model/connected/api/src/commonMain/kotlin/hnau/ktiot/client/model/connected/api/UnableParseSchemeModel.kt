package hnau.ktiot.client.model.connected.api

import hnau.common.app.goback.GoBackHandlerProvider
import hnau.ktiot.client.model.common.DoLogout
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.StateFlow

interface UnableParseSchemeModel : GoBackHandlerProvider {

    val error: Throwable

    @Shuffle
    interface Dependencies {

        val doLogout: DoLogout
    }

    val logout: StateFlow<(() -> Unit)?>
}
