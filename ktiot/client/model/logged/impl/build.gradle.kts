plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(libs.paho)
            implementation(project(":common:app"))
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:connected:api"))
            implementation(project(":ktiot:client:model:logged:api"))
        }
    }
}
