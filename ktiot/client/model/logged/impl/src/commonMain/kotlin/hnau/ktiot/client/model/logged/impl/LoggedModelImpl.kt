package hnau.ktiot.client.model.logged.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.app.goback.stateGoBackHandler
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.getOrInit
import hnau.common.kotlin.shrinkType
import hnau.common.kotlin.toAccessor
import hnau.common.mqtt.MqttClientParameters
import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.MqttConnectionState
import hnau.common.mqtt.flow
import hnau.ktiot.client.model.common.MqttConnectionInfo
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.logged.api.LoggedModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import org.eclipse.paho.client.mqttv3.MqttClientPersistence
import org.eclipse.paho.client.mqttv3.MqttPingSender
import java.util.concurrent.ScheduledExecutorService

internal class LoggedModelImpl(
    scope: CoroutineScope,
    private val skeleton: LoggedModel.Skeleton,
    private val dependencies: LoggedModel.Dependencies,
    private val connectedModelFactory: ConnectedModel.Factory,
    credentials: MqttConnectionInfo,
) : LoggedModel {

    override val state: StateFlow<LoggedModel.State> = MqttConnection
        .flow(
            clientParameters = credentials.toClientParameters(),
        )
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = MqttConnectionState.Connecting,
        )
        .scopedInState(scope)
        .mapState(scope) { (connectionStateScope, connectionState) ->
            when (connectionState) {
                is MqttConnectionState.Disconnected -> LoggedModel.State.Disconnected(
                    DisconnectedModelImpl(
                        scope = connectionStateScope,
                        dependencies = dependencies.disconnected(),
                    )
                )

                MqttConnectionState.Connecting -> LoggedModel.State.Connecting(
                    ConnectingModelImpl(
                        scope = connectionStateScope,
                        dependencies = dependencies.connecting(),
                    )
                )

                is MqttConnectionState.Connected -> LoggedModel.State.Connected(
                    connectedModelFactory.createConnectedModel(
                        scope = connectionStateScope,
                        skeleton = skeleton::connected
                            .toAccessor()
                            .shrinkType<_, LoggedModel.State.Skeleton.Connected>()
                            .getOrInit { LoggedModel.State.Skeleton.Connected() }
                            .connected,
                        dependencies = dependencies.connected(
                            connection = connectionState.connection,
                        ),
                    )
                )
            }
        }

    override val goBackHandler: GoBackHandler = state.stateGoBackHandler(scope)

    private fun MqttConnectionInfo.toClientParameters(
        persistence: MqttClientPersistence = MqttClientParameters.defaultPersistence,
        executorService: ScheduledExecutorService? = MqttClientParameters.defaultScheduledExecutorService,
        pingSender: MqttPingSender = MqttClientParameters.defaultPingSender,
    ): MqttClientParameters = MqttClientParameters(
        serverUri = "${protocol.uriName}://$address:$port",
        clientId = clientId,
        persistence = persistence,
        executorService = executorService,
        pingSender = pingSender,
    )
}
