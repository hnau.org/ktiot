package hnau.ktiot.client.model.logged.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.kotlin.coroutines.actionOrNullIfExecuting
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.logged.api.DisconnectedModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class DisconnectedModelImpl(
    scope: CoroutineScope,
    dependencies: DisconnectedModel.Dependencies,
) : DisconnectedModel {

    override val logout: StateFlow<(() -> Unit)?> = actionOrNullIfExecuting(
        scope = scope,
        action = dependencies.doLogout.remindType<DoLogout>()::logout,
    )

    override val goBackHandler: GoBackHandler
        get() = logout
}
