package hnau.ktiot.client.model.logged.impl

import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.logged.api.LoggedModel

fun LoggedModel.Factory.Companion.impl(
    connectedModelFactory: ConnectedModel.Factory,
): LoggedModel.Factory = LoggedModel.Factory { scope, skeleton, dependencies, credentials ->
    LoggedModelImpl(
        scope = scope,
        skeleton = skeleton,
        dependencies = dependencies,
        connectedModelFactory = connectedModelFactory,
        credentials = credentials,
    )
}