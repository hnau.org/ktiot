package hnau.ktiot.client.model.logged.impl

import hnau.common.app.goback.GoBackHandler
import hnau.common.kotlin.coroutines.actionOrNullIfExecuting
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.common.DoLogout
import hnau.ktiot.client.model.logged.api.ConnectingModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class ConnectingModelImpl(
    scope: CoroutineScope,
    dependencies: ConnectingModel.Dependencies,
) : ConnectingModel {

    override val logout: StateFlow<(() -> Unit)?> = actionOrNullIfExecuting(
        scope = scope,
        action = dependencies.doLogout.remindType<DoLogout>()::logout,
    )

    override val goBackHandler: GoBackHandler
        get() = logout
}
