package hnau.ktiot.client.model.logged.api

import hnau.common.app.goback.GoBackHandlerProvider
import hnau.ktiot.client.model.common.DoLogout
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.StateFlow

interface ConnectingModel : GoBackHandlerProvider {

    @Shuffle
    interface Dependencies {

        val doLogout: DoLogout
    }

    val logout: StateFlow<(() -> Unit)?>
}
