package hnau.ktiot.client.model.logged.api

import arrow.core.None
import arrow.core.Option
import arrow.core.serialization.OptionSerializer
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.mqtt.MqttConnection
import hnau.ktiot.client.model.common.MqttConnectionInfo
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface LoggedModel : GoBackHandlerProvider {

    sealed interface State : GoBackHandlerProvider {

        data class Connecting(
            val connecting: ConnectingModel,
        ) : State, GoBackHandlerProvider by connecting

        data class Disconnected(
            val disconnected: DisconnectedModel,
        ) : State, GoBackHandlerProvider by disconnected

        data class Connected(
            val connected: ConnectedModel,
        ) : State, GoBackHandlerProvider by connected

        @Serializable
        sealed interface Skeleton {

            @Serializable
            @SerialName("connected")
            data class Connected(
                val connected: ConnectedModel.Skeleton = ConnectedModel.Skeleton(),
            ) : Skeleton
        }

    }


    @Serializable
    data class Skeleton(
        @Serializable(OptionSerializer::class)
        var connected: Option<State.Skeleton> = None,
    )

    @Shuffle
    interface Dependencies {

        fun connecting(): ConnectingModel.Dependencies

        fun disconnected(): DisconnectedModel.Dependencies

        fun connected(
            connection: MqttConnection,
        ): ConnectedModel.Dependencies
    }

    val state: StateFlow<State>

    fun interface Factory {

        fun createLoggedModel(
            scope: CoroutineScope,
            skeleton: Skeleton,
            dependencies: Dependencies,
            credentials: MqttConnectionInfo,
        ): LoggedModel

        companion object
    }
}