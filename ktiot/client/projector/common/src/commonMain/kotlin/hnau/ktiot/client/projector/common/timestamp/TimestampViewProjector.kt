package hnau.ktiot.client.projector.common.timestamp

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedNullableVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.ktiot.client.model.common.timestamp.InstantsDelta
import hnau.ktiot.client.model.common.timestamp.TimestampViewModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalTime
import kotlinx.datetime.format
import kotlinx.datetime.format.DateTimeFormat

class TimestampViewProjector(
    private val model: TimestampViewModel,
    private val dependencies: Dependencies,
) {

    @Shuffle
    interface Dependencies {

        val localizer: Localizer
    }

    private val dateFormat: DateTimeFormat<LocalDate> =
        dateFormat(dependencies.localizer)

    @Composable
    fun Content(
        modifier: Modifier = Modifier,
    ) = Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
    ) {
        Column(
            modifier = Modifier.weight(1f)
        ) {
            val time by model.time.collectAsState()
            Text(text = remember(time) { time.format(timeFormat) })
            val dateOrNull by model.date.collectAsState()
            AnimatedNullableVisibility(
                value = dateOrNull,
                transitions = AnimatedVisibilityTransitions.vertical,
            ) { date ->
                Text(text = remember(date) { date.format(dateFormat) })
            }
        }

        val delta by model.delta.collectAsState()
        Text(text = remember(delta) { delta.formatted })
    }

    private val InstantsDelta.formatted
        get() = parts.joinToString(
            separator = " ",
            prefix = when (isPositive) {
                true -> ""
                false -> "-"
            },
        ) { (value, field) ->
            val suffix = with(dependencies.localizer.common.duration) {
                when (field) {
                    InstantsDelta.Part.Field.Seconds -> shortSeconds
                    InstantsDelta.Part.Field.Minutes -> shortMinutes
                    InstantsDelta.Part.Field.Hours -> shortHours
                    InstantsDelta.Part.Field.Days -> shortDays
                }
            }
            value.toString() + suffix
        }

    companion object {

        private val timeFormat: DateTimeFormat<LocalTime> = LocalTime.Format {
            hour()
            chars(":")
            minute()
            chars(":")
            second()
        }

        private val dateFormat: (Localizer) -> DateTimeFormat<LocalDate> = { localizer ->
            LocalDate.Format {
                dayOfMonth()
                chars(" ")
                monthName(localizer.common.shortMonths.toMonthNames())
                chars(" ")
                year()
            }
        }
    }
}