package hnau.ktiot.client.projector.common.timestamp

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.input.key.type
import androidx.compose.ui.text.input.KeyboardType
import hnau.common.app.EditingString
import hnau.common.compose.uikit.TextInput
import hnau.common.compose.uikit.shape.HnauShapeInRow
import hnau.common.compose.uikit.shape.center
import hnau.common.compose.uikit.shape.end
import hnau.common.compose.uikit.shape.start
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.kotlin.coroutines.onSet
import hnau.ktiot.client.model.common.timestamp.TimestampEditField
import hnau.ktiot.client.model.common.timestamp.TimestampEditFieldValues
import hnau.ktiot.client.model.common.timestamp.TimestampEditModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import androidx.compose.ui.text.input.ImeAction as ComposeImeAction

class TimestampEditProjector(
    private val model: TimestampEditModel,
    dependencies: Dependencies,
    private val afterLastImeAction: StateFlow<ImeAction?> = MutableStateFlow(null),
    private val requestFocusOnLaunch: Boolean = false,
) {

    data class ImeAction(
        val type: ComposeImeAction = ComposeImeAction.Done,
        val action: () -> Unit,
    )

    @Shuffle
    interface Dependencies {

        val localizer: Localizer
    }

    private val focusRequesters: TimestampEditFieldValues<FocusRequester> =
        TimestampEditFieldValues.build { FocusRequester() }

    private val titles: TimestampEditFieldValues<String> =
        titles(dependencies.localizer)

    @Composable
    fun Content(
        modifier: Modifier = Modifier,
    ) = Column(
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(Dimens.separation),
    ) {
        LaunchedEffect(requestFocusOnLaunch) {
            if (requestFocusOnLaunch) {
                focusRequesters[TimestampEditField.Day].requestFocus()
            }
        }
        val afterLastImeAction by afterLastImeAction.collectAsState()
        InputsRow {
            Input(
                field = TimestampEditField.Day,
                shape = HnauShapeInRow.start,
                afterLastImeAction = afterLastImeAction,
            )
            Input(
                field = TimestampEditField.Month,
                shape = HnauShapeInRow.center,
                afterLastImeAction = afterLastImeAction,
            )
            Input(
                field = TimestampEditField.Year,
                shape = HnauShapeInRow.end,
                afterLastImeAction = afterLastImeAction,
            )
        }
        InputsRow {
            Input(
                field = TimestampEditField.Hours,
                shape = HnauShapeInRow.start,
                afterLastImeAction = afterLastImeAction,
            )
            Input(
                field = TimestampEditField.Minutes,
                shape = HnauShapeInRow.center,
                afterLastImeAction = afterLastImeAction,
            )
            Input(
                field = TimestampEditField.Seconds,
                shape = HnauShapeInRow.end,
                afterLastImeAction = afterLastImeAction,
            )
        }
    }

    @Composable
    private fun InputsRow(
        content: @Composable RowScope.() -> Unit,
    ) = Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(Dimens.chipsSeparation),
        content = content,
    )

    @Composable
    private fun RowScope.Input(
        field: TimestampEditField,
        afterLastImeAction: ImeAction?,
        shape: Shape,
    ) {
        val value = model.values[field]
        val imeAction: ImeAction? =
            afterLastImeAction.takeIf { field == TimestampEditField.entries.last() }
        val maxLength = TimestampEditModel.lengths[field]
        val currentValue by value.collectAsState()
        TextInput(
            value = remember(value, currentValue, field) {
                var previousNewValue: EditingString? = null
                value.onSet { newValue ->
                    if (newValue == previousNewValue) {
                        return@onSet
                    }
                    previousNewValue = newValue
                    if (currentValue.text.length >= maxLength) {
                        return@onSet
                    }
                    if (newValue.text.length != maxLength) {
                        return@onSet
                    }
                    if (newValue.selection.first != maxLength) {
                        return@onSet
                    }
                    if (newValue.selection.last != maxLength) {
                        return@onSet
                    }
                    val nextField = when (field) {
                        TimestampEditField.Day -> TimestampEditField.Month
                        TimestampEditField.Month -> TimestampEditField.Year
                        TimestampEditField.Year -> TimestampEditField.Hours
                        TimestampEditField.Hours -> TimestampEditField.Minutes
                        TimestampEditField.Minutes -> TimestampEditField.Seconds
                        TimestampEditField.Seconds -> null
                    } ?: return@onSet
                    focusRequesters[nextField].requestFocus()
                }
            },
            modifier = Modifier
                .weight(maxLength.toFloat())
                .focusRequester(focusRequesters[field])
                .onPreviewKeyEvent { event ->

                    fun tryHandleBackspace(): Boolean {
                        if (value.value.text.isNotEmpty()) {
                            return false
                        }
                        if (event.key != Key.Backspace) {
                            return false
                        }
                        if (event.type != KeyEventType.KeyDown) {
                            return false
                        }
                        val previousField = when (field) {
                            TimestampEditField.Day -> null
                            TimestampEditField.Month -> TimestampEditField.Day
                            TimestampEditField.Year -> TimestampEditField.Month
                            TimestampEditField.Hours -> TimestampEditField.Year
                            TimestampEditField.Minutes -> TimestampEditField.Hours
                            TimestampEditField.Seconds -> TimestampEditField.Minutes
                        } ?: return false
                        focusRequesters[previousField].requestFocus()
                        return true
                    }

                    if (tryHandleBackspace()) {
                        return@onPreviewKeyEvent true
                    }

                    false
                },
            shape = shape,
            label = {
                Text(
                    text = titles[field],
                    maxLines = 1,
                )
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = imeAction?.type ?: ComposeImeAction.Next,
            ),
            keyboardActions = imeAction
                ?.action
                ?.let { action -> KeyboardActions { action() } }
                ?: KeyboardActions(),
        )
    }

    companion object {

        private val titles: (Localizer) -> TimestampEditFieldValues<String> =
            { localizer: Localizer ->
                with(localizer.dateTimeEdit) {
                    TimestampEditFieldValues.build { field ->
                        when (field) {
                            TimestampEditField.Day -> day
                            TimestampEditField.Month -> month
                            TimestampEditField.Year -> year
                            TimestampEditField.Hours -> hours
                            TimestampEditField.Minutes -> minutes
                            TimestampEditField.Seconds -> seconds
                        }
                    }
                }
            }

    }
}