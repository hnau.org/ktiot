package hnau.ktiot.client.projector.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import hnau.common.compose.utils.AnimatedNullableVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import kotlinx.coroutines.flow.StateFlow

@Composable
fun <T> StateFlow<T?>.Content(
    transitions: AnimatedVisibilityTransitions,
    Content: @Composable (T) -> Unit,
) {
    val valueOrNull by collectAsState()
    AnimatedNullableVisibility(
        value = valueOrNull,
        transitions = transitions,
    ) { onClickOrNullLocal: T ->
        Content(onClickOrNullLocal)
    }
}