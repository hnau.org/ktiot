package hnau.ktiot.client.projector.common.timestamp

import hnau.ktiot.client.projector.common.Localizer
import kotlinx.datetime.format.MonthNames

fun Localizer.Common.Months.toMonthNames(): MonthNames = MonthNames(
    january = january,
    february = february,
    march = march,
    april = april,
    may = may,
    june = june,
    july = july,
    august = august,
    september = september,
    october = october,
    november = november,
    december = december
)