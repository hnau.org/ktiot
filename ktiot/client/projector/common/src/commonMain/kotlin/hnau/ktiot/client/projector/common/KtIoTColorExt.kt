package hnau.ktiot.client.projector.common

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Error
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import hnau.common.color.RGBABytes
import hnau.common.compose.uikit.color.compose
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedNullableVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.common.compose.utils.Icon
import hnau.common.compose.utils.option
import kotlin.math.abs

@Composable
fun RGBABytes?.Sign() = AnimatedContent(
    targetState = this,
    contentKey = { it != null },
) { colorLocal ->
    val colors = MaterialTheme.colors
    val color = when (colorLocal) {
        null -> {
            ColorSign(
                colorOrNull = null,
                borderColor = colors.onSurface,
                sign = { Error },
            )
            return@AnimatedContent
        }

        else -> colorLocal.compose
    }
    val borderColor = remember(colors, color) {
        val colorLuminance = color.luminance()
        listOf(
            colors.surface to false,
            colors.onSurface to true,
        ).maxBy { (candidate, _) ->
            abs(candidate.luminance() - colorLuminance)
        }
            .takeIf { (_, use) -> use }
            ?.first
            ?.copy(alpha = 0.5f)
            ?: Color.Transparent
    }
    ColorSign(
        colorOrNull = color,
        borderColor = borderColor,
        sign = null,
    )
}

@Composable
private fun ColorSign(
    colorOrNull: Color?,
    borderColor: Color,
    sign: (Icons.Filled.() -> ImageVector)? = null,
) {
    val shape = CircleShape
    Box(
        modifier = Modifier
            .size(40.dp)
            .option {
                colorOrNull?.let { color ->
                    val animatedColor by animateColorAsState(color)
                    background(
                        color = animatedColor,
                        shape = shape,
                    )
                }
            }
            .border(
                width = Dimens.border,
                color = animateColorAsState(borderColor).value,
                shape = shape,
            )
    ) {
        AnimatedNullableVisibility(
            value = sign,
            transitions = AnimatedVisibilityTransitions.static,
            modifier = Modifier.fillMaxSize(),
        ) { signLocal ->
            Box(
                contentAlignment = Alignment.Center,
            ) {
                Icon(chooseIcon = signLocal)
            }
        }
    }

}