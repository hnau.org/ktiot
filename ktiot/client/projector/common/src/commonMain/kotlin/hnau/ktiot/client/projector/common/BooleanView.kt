package hnau.ktiot.client.projector.common

import androidx.compose.animation.AnimatedContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun BooleanView(
    value: Boolean,
    localizer: Localizer,
    modifier: Modifier = Modifier,
) {
    AnimatedContent(
        modifier = modifier,
        targetState = value,
        contentAlignment = Alignment.Center,
    ) { valueLocal ->
        Text(
            text = when (valueLocal) {
                true -> localizer.common.yes
                false -> localizer.common.no
            },
            color = when (valueLocal) {
                true -> MaterialTheme.colors.primary
                false -> MaterialTheme.colors.onSurface
            },
        )
    }
}
