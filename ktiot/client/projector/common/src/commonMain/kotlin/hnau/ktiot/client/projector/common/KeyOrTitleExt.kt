package hnau.ktiot.client.projector.common

import arrow.core.Either
import hnau.common.mqtt.topic.MqttTopic
import hnau.common.mqtt.topic.toTitle

fun Either<MqttTopic, String>.title(
    localizer: Localizer,
): String = when (this) {
    is Either.Left -> {
        when (val topic = value) {
            is MqttTopic.Absolute -> topic
                .toTitle()

            is MqttTopic.Relative -> topic
                .toTitle()
                .takeIf { it.isNotEmpty() }
                ?: localizer.common.unknown
        }
    }

    is Either.Right -> value
}
