package hnau.ktiot.client.projector.scheme.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.common.TopAppBarDelegate
import hnau.ktiot.client.projector.common.backbutton.BackButtonWidthProvider
import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface SchemeProjector {

    @Shuffle
    interface Dependencies {

        val localizer: Localizer

        val backButtonWidthProvider: BackButtonWidthProvider

        fun element(): ElementProjector.Dependencies

        fun topAppBar(): TopAppBarDelegate.Dependencies
    }

    @Composable
    fun Content()


    fun interface Factory {

        fun createSchemeProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: SchemeModel,
        ): SchemeProjector

        companion object
    }
}