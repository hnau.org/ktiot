package hnau.ktiot.client.projector.scheme.impl

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.compose.uikit.chip.Chip
import hnau.common.compose.uikit.chip.ChipStyle
import hnau.common.compose.uikit.shape.HnauShape
import hnau.common.compose.uikit.shape.create
import hnau.common.compose.uikit.shape.inRow
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.Icon
import hnau.common.compose.utils.getTransitionSpecForHorizontalSlide
import hnau.common.compose.utils.toLazyListState
import hnau.common.kotlin.coroutines.createChild
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.runningFoldState
import hnau.common.kotlin.map
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.scheme.api.SchemeModelStack
import hnau.ktiot.client.projector.common.TopAppBarDelegate
import hnau.ktiot.client.projector.common.title
import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.ktiot.client.projector.scheme.api.SchemeProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.StateFlow

internal class SchemeProjectorImpl(
    private val scope: CoroutineScope,
    private val dependencies: SchemeProjector.Dependencies,
    private val model: SchemeModel,
    private val elementProjectorFactory: ElementProjector.Factory,
) : SchemeProjector, GoBackHandlerProvider {

    private data class TopElementProjector(
        val model: ElementModel,
        val projector: ElementProjector,
        val cancelScope: () -> Unit,
    )

    private fun SchemeModelStack<ElementModel>.toTopElementProjector(
        previous: TopElementProjector? = null,
    ): TopElementProjector {
        val model = tail.lastOrNull()?.value ?: head
        return previous
            ?.takeIf { it.model === model }
            ?: run {
                val topElementProjector = scope.createChild()
                TopElementProjector(
                    model = model,
                    projector = elementProjectorFactory.createElementProjector(
                        scope = topElementProjector,
                        dependencies = dependencies.element(),
                        model = model,
                    ),
                    cancelScope = { topElementProjector.cancel() },
                )
            }
    }

    private data class NavigationElement(
        val title: String?,
        val onClick: (() -> Unit)?,
    )

    private fun SchemeModelStack<ElementModel>.toNavigationElements(): List<NavigationElement> =
        tail
            .fold<_, Pair<List<NavigationElement>, String?>>(
                initial = emptyList<NavigationElement>() to null,
            ) { (previousElements, previousTitle), element ->
                val elements = previousElements + NavigationElement(
                    title = previousTitle,
                    onClick = element.onPreviousClick,
                )
                elements to element.keyOrTitle.title(dependencies.localizer)
            }
            .let { (acc, lastTitle) ->
                acc + NavigationElement(
                    title = lastTitle,
                    onClick = null,
                )
            }

    private val state: StateFlow<Pair<List<NavigationElement>, IndexedValue<ElementProjector>>> =
        model
            .stack
            .runningFoldState(
                scope = scope,
                createInitial = {
                    it.toNavigationElements() to IndexedValue(0, it.toTopElementProjector())
                },
            ) { (_, previousProjector), stack ->
                val index = stack.tail.size
                val topElementProjector = stack.toTopElementProjector(previousProjector.value)
                if (previousProjector.value.model !== topElementProjector.model) {
                    previousProjector.value.cancelScope()
                }
                val indexedTopElementProjector = IndexedValue(index, topElementProjector)
                val navigationElements = stack.toNavigationElements()
                navigationElements to indexedTopElementProjector
            }
            .mapState(scope) { (navigationElements, topElementProjector) ->
                navigationElements to topElementProjector.map(TopElementProjector::projector)
            }

    private val topAppBarDelegate = TopAppBarDelegate(
        dependencies = dependencies.topAppBar(),
    )

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    override fun Content() {
        val (navigationElements, topElementProjector) = state.collectAsState().value
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background),
        ) {
            topAppBarDelegate.Content { startContentPadding ->
                LazyRow(
                    modifier = Modifier.weight(1f),
                    horizontalArrangement = Arrangement.spacedBy(Dimens.chipsSeparation),
                    state = model.stackTitlesScrollState.toLazyListState(),
                    contentPadding = PaddingValues(
                        start = startContentPadding,
                        end = Dimens.separation,
                    ),
                ) {
                    itemsIndexed(navigationElements) { i, (title, onClickOrNull) ->
                        Chip(
                            shape = HnauShape.inRow.create(i, navigationElements.size),
                            onClick = onClickOrNull,
                            modifier = Modifier.animateItemPlacement(),
                            style = ChipStyle.chip,
                            content = {
                                when (title) {
                                    null -> Icon { Home }
                                    else -> Text(title)
                                }
                            },
                        )
                    }
                }
                IconButton(
                    onClick = model::openSettings,
                ) {
                    Icon { Settings }
                }
            }
            AnimatedContent(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                targetState = topElementProjector,
                transitionSpec = getTransitionSpecForHorizontalSlide {
                    val (previousIndex) = initialState
                    val (nextIndex) = targetState
                    if (nextIndex > previousIndex) 1 else -1
                },
            ) { (_, projector) ->
                projector.Content()
            }
        }
    }
}
