package hnau.ktiot.client.projector.scheme.impl

import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.ktiot.client.projector.scheme.api.SchemeProjector

fun SchemeProjector.Factory.Companion.impl(
    elementProjectorFactory: ElementProjector.Factory,
): SchemeProjector.Factory = SchemeProjector.Factory { scope, dependencies, model ->
    SchemeProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        elementProjectorFactory = elementProjectorFactory,
    )
}