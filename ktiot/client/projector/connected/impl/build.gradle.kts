plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:connected:api"))
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:connected:api"))
            implementation(project(":ktiot:client:projector:mainstack:api"))
        }
    }
}
