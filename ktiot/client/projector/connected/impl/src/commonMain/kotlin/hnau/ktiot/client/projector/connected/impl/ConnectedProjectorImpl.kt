package hnau.ktiot.client.projector.connected.impl

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.remindSupertype
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.ktiot.client.projector.mainstack.api.MainStackProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class ConnectedProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: ConnectedProjector.Dependencies,
    model: ConnectedModel,
    private val mainStackProjectorFactory: MainStackProjector.Factory,
) : ConnectedProjector {

    private sealed interface State {

        @Composable
        fun Content()

        data class ReceivingScheme(
            private val receivingScheme: ReceivingSchemeProjector,
        ) : State {

            @Composable
            override fun Content() =
                receivingScheme.Content()
        }

        data class UnableParseScheme(
            private val unableParseScheme: UnableParseSchemeProjector,
        ) : State {

            @Composable
            override fun Content() =
                unableParseScheme.Content()
        }

        data class MainStack(
            private val mainStack: MainStackProjector,
        ) : State {

            @Composable
            override fun Content() =
                mainStack.Content()
        }
    }


    private val stateProjector: StateFlow<State> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, state) ->
            when (state) {
                is ConnectedModel.State.ReceivingScheme -> State.ReceivingScheme(
                    ReceivingSchemeProjector(
                        dependencies = dependencies.receivingScheme(),
                        model = state.receivingScheme,
                    )
                )

                is ConnectedModel.State.UnableParseScheme -> State.UnableParseScheme(
                    UnableParseSchemeProjector(
                        dependencies = dependencies.unableParseScheme(),
                        model = state.unableParseScheme,
                    )
                )

                is ConnectedModel.State.MainStack -> State.MainStack(
                    mainStackProjectorFactory.createMainStackProjector(
                        scope = stateScope,
                        dependencies = dependencies.mainStack(),
                        model = state.mainStack
                            .remindType<MainStackModel>()
                            .remindSupertype<_, GoBackHandlerProvider>(),
                    )
                )
            }
        }

    @Composable
    override fun Content() {
        val currentStateProjector: State by stateProjector.collectAsState()
        AnimatedContent(
            targetState = currentStateProjector,
            contentKey = { stateProjector ->
                when (stateProjector) {
                    is State.ReceivingScheme -> 0
                    is State.UnableParseScheme -> 1
                    is State.MainStack -> 2
                }
            },
            modifier = Modifier.fillMaxSize(),
        ) { localStateProjector ->
            localStateProjector.Content()
        }
    }
}
