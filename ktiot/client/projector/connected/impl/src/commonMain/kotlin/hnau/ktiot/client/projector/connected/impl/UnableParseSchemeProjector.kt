package hnau.ktiot.client.projector.connected.impl

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.Space
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorPanel
import hnau.ktiot.client.model.connected.api.UnableParseSchemeModel
import hnau.ktiot.client.projector.connected.api.UnableParseSchemeProjectorDependencies

internal class UnableParseSchemeProjector(
    private val dependencies: UnableParseSchemeProjectorDependencies,
    private val model: UnableParseSchemeModel,
) {

    @Composable
    fun Content() = ProgressIndicatorPanel {
        Text(text = dependencies.localizer.logged.failParseSchemeWaitingNewScheme)
        Column(
            modifier = Modifier
                .heightIn(max = 256.dp)
                .verticalScroll(state = rememberScrollState()),
        ) {
            Text(
                text = dependencies.localizer.logged.schemeParsingError + ":\n" + model.error.stackTraceToString(),
                color = MaterialTheme.colors.error,
            )
        }
        Space()
        Button(
            onClickOrExecuting = model.logout,
        ) {
            Text(dependencies.localizer.logged.logout)
        }
    }
}
