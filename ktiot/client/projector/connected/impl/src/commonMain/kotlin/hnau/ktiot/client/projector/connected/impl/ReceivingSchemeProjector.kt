package hnau.ktiot.client.projector.connected.impl

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.Space
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorPanel
import hnau.ktiot.client.model.connected.api.ReceivingSchemeModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.connected.api.ReceivingSchemeProjectorDependencies

internal class ReceivingSchemeProjector(
    private val dependencies: ReceivingSchemeProjectorDependencies,
    private val model: ReceivingSchemeModel,
) {

    private val localizer: Localizer
        get() = dependencies.localizer

    @Composable
    fun Content() = ProgressIndicatorPanel {
        Text(text = localizer.logged.receivingScheme)
        Space()
        Button(
            onClickOrExecuting = model.logout,
        ) {
            Text(localizer.logged.logout)
        }
    }
}
