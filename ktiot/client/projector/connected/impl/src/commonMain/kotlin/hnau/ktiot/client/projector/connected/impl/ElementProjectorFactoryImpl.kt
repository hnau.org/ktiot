package hnau.ktiot.client.projector.connected.impl

import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.ktiot.client.projector.mainstack.api.MainStackProjector

fun ConnectedProjector.Factory.Companion.impl(
    mainStackProjectorFactory: MainStackProjector.Factory,
): ConnectedProjector.Factory = ConnectedProjector.Factory { scope, dependencies, model ->
    ConnectedProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        mainStackProjectorFactory = mainStackProjectorFactory,
    )
}