package hnau.ktiot.client.projector.connected.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.projector.mainstack.api.MainStackProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface ConnectedProjector {

    @Shuffle
    interface Dependencies {

        fun receivingScheme(): ReceivingSchemeProjectorDependencies

        fun unableParseScheme(): UnableParseSchemeProjectorDependencies

        fun mainStack(): MainStackProjector.Dependencies
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createConnectedProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: ConnectedModel,
        ): ConnectedProjector

        companion object
    }
}