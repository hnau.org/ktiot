package hnau.ktiot.client.projector.connected.api

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ReceivingSchemeProjectorDependencies {

    val localizer: Localizer
}