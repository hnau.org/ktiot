package hnau.ktiot.client.projector.initialized.api

import hnau.common.compose.uikit.bubble.BubblesShower
import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface LoginProjectorDependencies {

    val bubblesShower: BubblesShower

    val localizer: Localizer
}