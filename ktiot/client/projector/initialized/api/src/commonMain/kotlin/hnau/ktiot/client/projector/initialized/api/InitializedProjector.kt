package hnau.ktiot.client.projector.initialized.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.projector.logged.api.LoggedProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface InitializedProjector {

    @Shuffle
    interface Dependencies {

        fun login(): LoginProjectorDependencies

        fun logged(): LoggedProjector.Dependencies
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createInitializedProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: InitializedModel,
        ): InitializedProjector

        companion object
    }
}