package hnau.ktiot.client.projector.initialized.impl

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.TextInput
import hnau.common.compose.uikit.bubble.Bubble
import hnau.common.compose.uikit.bubble.showBubbles
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.common.MqttConnectionInfo
import hnau.ktiot.client.model.initialized.api.LoginModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.initialized.api.LoginProjectorDependencies
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.map

internal class LoginProjector(
    scope: CoroutineScope,
    private val dependencies: LoginProjectorDependencies,
    private val model: LoginModel,
) {

    private val localizer: Localizer
        get() = dependencies.localizer

    init {
        dependencies.bubblesShower.showBubbles(
            scope = scope,
            bubbles = model.messages.map { message ->
                Bubble(
                    text = when (message) {
                        LoginModel.Message.PortIsIncorrect ->
                            localizer.logging.portIsIncorrect
                    },
                )
            },
        )
    }

    @Composable
    fun Content() = Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 480.dp)
                .padding(Dimens.separation),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(
                space = Dimens.separation,
                alignment = Alignment.CenterVertically,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
            ) {
                Address(
                    modifier = Modifier.weight(1f),
                )
                Port(
                    modifier = Modifier.width(96.dp),
                )
            }
            ClientId(
                modifier = Modifier.fillMaxWidth(),
            )
            Login()
        }
    }

    @Composable
    private fun Address(
        modifier: Modifier = Modifier,
    ) {
        val focusRequester = remember { FocusRequester() }
        LaunchedEffect(Unit) { focusRequester.requestFocus() }
        TextInput(
            modifier = modifier.focusRequester(focusRequester),
            value = model.address,
            maxLines = 1,
            minLines = 1,
            label = { Text(localizer.logging.address) },
            placeholder = { Text(MqttConnectionInfo.defaultAddress) },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Uri,
                imeAction = ImeAction.Next,
            ),
        )
    }

    @Composable
    private fun Port(
        modifier: Modifier = Modifier,
    ) {
        TextInput(
            modifier = modifier,
            value = model.port,
            maxLines = 1,
            minLines = 1,
            label = { Text(localizer.logging.port) },
            placeholder = { Text(MqttConnectionInfo.defaultPort.toString()) },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Decimal,
                imeAction = ImeAction.Next,
            ),
        )
    }

    @Composable
    private fun ClientId(
        modifier: Modifier = Modifier,
    ) {
        TextInput(
            modifier = modifier,
            value = model.clientId,
            maxLines = 1,
            minLines = 1,
            label = { Text(localizer.logging.clientId) },
            placeholder = { Text(localizer.logging.randomClientId) },
            keyboardActions = KeyboardActions { model.loginOrNull.value?.invoke() },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Ascii,
                imeAction = ImeAction.Done,
            ),
        )
    }

    @Composable
    private fun Login() {
        Button(
            onClickOrExecuting = model.loginOrNull,
        ) {
            Text(localizer.logging.login)
        }
    }
}
