package hnau.ktiot.client.projector.initialized.impl

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.remindSupertype
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.model.logged.api.LoggedModel
import hnau.ktiot.client.projector.initialized.api.InitializedProjector
import hnau.ktiot.client.projector.logged.api.LoggedProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class InitializedProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: InitializedProjector.Dependencies,
    private val model: InitializedModel,
    private val loggedProjectorFactory: LoggedProjector.Factory,
) : InitializedProjector {

    private sealed interface State {

        @Composable
        fun Content()

        data class Login(
            private val login: LoginProjector,
        ) : State {

            @Composable
            override fun Content() = login.Content()
        }

        data class Logged(
            private val logged: LoggedProjector,
        ) : State {

            @Composable
            override fun Content() = logged.Content()
        }

    }

    private val stateProjector: StateFlow<State> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, state) ->
            when (state) {
                is InitializedModel.State.Login -> State.Login(
                    LoginProjector(
                        scope = stateScope,
                        dependencies = dependencies.login(),
                        model = state.login,
                    )
                )

                is InitializedModel.State.Logged -> State.Logged(
                    loggedProjectorFactory.createLoggedModel(
                        scope = stateScope,
                        dependencies = dependencies.logged(),
                        model = state
                            .logged
                            .remindType<LoggedModel>()
                            .remindSupertype<_, GoBackHandlerProvider>(),
                    )
                )
            }
        }

    @Composable
    override fun Content() {
        val stateProjector by stateProjector.collectAsState()
        AnimatedContent(
            targetState = stateProjector,
            contentKey = {
                when (it) {
                    is State.Login -> 0
                    is State.Logged -> 1
                }
            },
            modifier = Modifier.fillMaxSize(),
        ) { stateProjectorLocal ->
            stateProjectorLocal.Content()
        }
    }
}
