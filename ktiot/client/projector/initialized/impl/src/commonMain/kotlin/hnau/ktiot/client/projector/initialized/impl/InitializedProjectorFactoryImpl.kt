package hnau.ktiot.client.projector.initialized.impl

import hnau.ktiot.client.projector.initialized.api.InitializedProjector
import hnau.ktiot.client.projector.logged.api.LoggedProjector

fun InitializedProjector.Factory.Companion.impl(
    loggedProjectorFactory: LoggedProjector.Factory,
): InitializedProjector.Factory = InitializedProjector.Factory { scope, dependencies, model ->
    InitializedProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        loggedProjectorFactory = loggedProjectorFactory,
    )
}