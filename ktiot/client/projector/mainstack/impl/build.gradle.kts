plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:client:model:scheme:api"))
            implementation(project(":ktiot:client:model:settings:api"))
            implementation(project(":ktiot:client:projector:mainstack:api"))
            implementation(project(":ktiot:client:projector:scheme:api"))
            implementation(project(":ktiot:client:projector:settings:api"))
        }
    }
}
