package hnau.ktiot.client.projector.mainstack.impl

import hnau.ktiot.client.projector.mainstack.api.MainStackProjector
import hnau.ktiot.client.projector.scheme.api.SchemeProjector
import hnau.ktiot.client.projector.settings.api.SettingsProjector

fun MainStackProjector.Factory.Companion.impl(
    schemeProjectorFactory: SchemeProjector.Factory,
    settingsProjectorFactory: SettingsProjector.Factory,
): MainStackProjector.Factory = MainStackProjector.Factory { scope, dependencies, model ->
    MainStackProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        schemeProjectorFactory = schemeProjectorFactory,
        settingsProjectorFactory = settingsProjectorFactory,
    )
}