package hnau.ktiot.client.projector.mainstack.impl

import androidx.compose.runtime.Composable
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.compose.projector.Content
import hnau.common.compose.projector.StackProjectorTail
import hnau.common.kotlin.remindSupertype
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.mainstack.api.MainStackElementModel
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.model.scheme.api.SchemeModel
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.client.projector.mainstack.api.MainStackProjector
import hnau.ktiot.client.projector.scheme.api.SchemeProjector
import hnau.ktiot.client.projector.settings.api.SettingsProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class MainStackProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: MainStackProjector.Dependencies,
    model: MainStackModel,
    private val schemeProjectorFactory: SchemeProjector.Factory,
    private val settingsProjectorFactory: SettingsProjector.Factory,
) : MainStackProjector {

    sealed interface Element {

        @Composable
        fun Content()

        data class Scheme(
            private val scheme: SchemeProjector,
        ) : Element {

            @Composable
            override fun Content() =
                scheme.Content()
        }

        data class Settings(
            private val settings: SettingsProjector,
        ) : Element {

            @Composable
            override fun Content() =
                settings.Content()
        }
    }

    private val tail: StateFlow<StackProjectorTail<Element>> = StackProjectorTail(
        scope = scope,
        modelsStack = model
            .remindSupertype<_, GoBackHandlerProvider>()
            .stack,
        extractKey = {
            when (this) {
                is MainStackElementModel.Scheme -> 0
                is MainStackElementModel.Settings -> 1
            }
        },
    ) { stateScope, state ->
        when (state) {
            is MainStackElementModel.Scheme -> Element.Scheme(
                schemeProjectorFactory.createSchemeProjector(
                    scope = stateScope,
                    dependencies = dependencies.scheme(),
                    model = state.scheme.remindType<SchemeModel>(),
                )
            )

            is MainStackElementModel.Settings -> Element.Settings(
                settingsProjectorFactory.createSettingsProjector(
                    model = state.settings.remindType<SettingsModel>(),
                    dependencies = dependencies.settings(),
                )
            )
        }
    }

    @Composable
    override fun Content() {
        tail.Content { Content() }
    }
}
