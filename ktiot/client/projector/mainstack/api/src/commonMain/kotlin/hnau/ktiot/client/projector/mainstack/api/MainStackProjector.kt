package hnau.ktiot.client.projector.mainstack.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.mainstack.api.MainStackModel
import hnau.ktiot.client.projector.scheme.api.SchemeProjector
import hnau.ktiot.client.projector.settings.api.SettingsProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface MainStackProjector {

    @Shuffle
    interface Dependencies {

        fun scheme(): SchemeProjector.Dependencies

        fun settings(): SettingsProjector.Dependencies
    }

    @Composable
    fun Content()


    fun interface Factory {

        fun createMainStackProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: MainStackModel,
        ): MainStackProjector

        companion object
    }
}