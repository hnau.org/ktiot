plugins {
    alias(libs.plugins.compose.desktop)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":ktiot:client:model:mainstack:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:scheme:api"))
            implementation(project(":ktiot:client:projector:settings:api"))
        }
    }
}
