package hnau.ktiot.client.projector.logged.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.logged.api.LoggedModel
import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface LoggedProjector {

    @Shuffle
    interface Dependencies {

        fun disconnected(): DisconnectedProjectorDependencies

        fun connecting(): ConnectingProjectorDependencies

        fun connected(): ConnectedProjector.Dependencies
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createLoggedModel(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: LoggedModel,
        ): LoggedProjector

        companion object
    }
}