package hnau.ktiot.client.projector.logged.api

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ConnectingProjectorDependencies {

    val localizer: Localizer
}