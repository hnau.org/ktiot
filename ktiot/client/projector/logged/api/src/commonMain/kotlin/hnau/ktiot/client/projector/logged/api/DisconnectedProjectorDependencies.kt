package hnau.ktiot.client.projector.logged.api

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface DisconnectedProjectorDependencies {

    val localizer: Localizer
}