package hnau.ktiot.client.projector.logged.impl

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.Space
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorPanel
import hnau.ktiot.client.model.logged.api.ConnectingModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.logged.api.ConnectingProjectorDependencies

internal class ConnectingProjector(
    private val dependencies: ConnectingProjectorDependencies,
    private val model: ConnectingModel,
) {

    @Suppress("unused")
    private val goBackHandlerProvider: GoBackHandlerProvider
        get() = model

    private val localizer: Localizer
        get() = dependencies.localizer

    @Composable
    fun Content() = ProgressIndicatorPanel {
        Text(text = localizer.logged.connecting)
        Space()
        Button(
            onClickOrExecuting = model.logout,
        ) {
            Text(localizer.logged.logout)
        }
    }
}
