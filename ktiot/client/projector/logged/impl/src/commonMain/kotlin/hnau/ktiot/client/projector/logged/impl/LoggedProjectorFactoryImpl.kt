package hnau.ktiot.client.projector.logged.impl

import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.ktiot.client.projector.logged.api.LoggedProjector

fun LoggedProjector.Factory.Companion.impl(
    connectedProjectorFactory: ConnectedProjector.Factory,
): LoggedProjector.Factory = LoggedProjector.Factory { scope, dependencies, model ->
    LoggedProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        connectedProjectorFactory = connectedProjectorFactory,
    )
}