package hnau.ktiot.client.projector.logged.impl

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.connected.api.ConnectedModel
import hnau.ktiot.client.model.logged.api.LoggedModel
import hnau.ktiot.client.projector.connected.api.ConnectedProjector
import hnau.ktiot.client.projector.logged.api.LoggedProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class LoggedProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: LoggedProjector.Dependencies,
    model: LoggedModel,
    private val connectedProjectorFactory: ConnectedProjector.Factory,
) : LoggedProjector {

    private sealed interface State {

        @Composable
        fun Content()

        data class Connecting(
            private val connecting: ConnectingProjector,
        ) : State {

            @Composable
            override fun Content() = connecting.Content()
        }

        data class Disconnected(
            private val disconnected: DisconnectedProjector,
        ) : State {

            @Composable
            override fun Content() = disconnected.Content()
        }

        data class Connected(
            private val connected: ConnectedProjector,
        ) : State {

            @Composable
            override fun Content() = connected.Content()
        }
    }

    private val stateProjector: StateFlow<State> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, state) ->
            when (state) {
                is LoggedModel.State.Disconnected -> State.Disconnected(
                    DisconnectedProjector(
                        dependencies = dependencies.disconnected(),
                        model = state.disconnected,
                    )
                )

                is LoggedModel.State.Connecting -> State.Connecting(
                    ConnectingProjector(
                        dependencies = dependencies.connecting(),
                        model = state.connecting,
                    )
                )

                is LoggedModel.State.Connected -> State.Connected(
                    connectedProjectorFactory.createConnectedProjector(
                        dependencies = dependencies.connected(),
                        scope = stateScope,
                        model = state.connected.remindType<ConnectedModel>(),
                    )
                )
            }
        }

    @Composable
    override fun Content() {
        val currentStateProjector: State by stateProjector.collectAsState()
        AnimatedContent(
            targetState = currentStateProjector,
            contentKey = { stateProjector ->
                when (stateProjector) {
                    is State.Disconnected -> 0
                    is State.Connecting -> 1
                    is State.Connected -> 2
                }
            },
            modifier = Modifier.fillMaxSize(),
        ) { localStateProjector ->
            localStateProjector.Content()
        }
    }
}
