package hnau.ktiot.client.projector.logged.impl

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.Space
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorPanel
import hnau.ktiot.client.model.logged.api.DisconnectedModel
import hnau.ktiot.client.projector.logged.api.DisconnectedProjectorDependencies

internal class DisconnectedProjector(
    private val dependencies: DisconnectedProjectorDependencies,
    private val model: DisconnectedModel,
) {

    @Composable
    fun Content() = ProgressIndicatorPanel {
        Text(text = dependencies.localizer.logged.waitingForReconnection)
        Space()
        Button(
            onClickOrExecuting = model.logout,
        ) {
            Text(dependencies.localizer.logged.logout)
        }
    }
}
