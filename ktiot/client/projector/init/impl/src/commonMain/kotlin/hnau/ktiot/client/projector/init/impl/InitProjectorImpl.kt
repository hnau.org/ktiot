package hnau.ktiot.client.projector.init.impl

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.app.goback.GoBackHandler
import hnau.common.compose.uikit.bubble.Content
import hnau.common.compose.uikit.bubble.SharedBubblesHolder
import hnau.common.compose.uikit.progressindicator.ProgressIndicator
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorSize
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.common.kotlin.remindType
import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.model.initialized.api.InitializedModel
import hnau.ktiot.client.projector.common.backbutton.BackButtonDelegate
import hnau.ktiot.client.projector.init.api.InitProjector
import hnau.ktiot.client.projector.initialized.api.InitializedProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

internal class InitProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: InitProjector.Dependencies,
    model: InitModel,
    private val initializedProjectorFactory: InitializedProjector.Factory,
) : InitProjector {

    private val backButtonDelegate = BackButtonDelegate(
        scope = scope,
        goBackHandler = model
            .goBackHandler
            .remindType<GoBackHandler>(),
    )

    private val bubblesHolder = SharedBubblesHolder(
        scope = scope,
    )

    private val initializedProjector: StateFlow<InitializedProjector?> = model
        .initialized
        .remindType<StateFlow<InitializedModel?>>()
        .scopedInState(scope)
        .mapState(scope) { (initializedScope, initializedModelOrNull) ->
            initializedModelOrNull?.let { loggableModel ->
                initializedProjectorFactory.createInitializedProjector(
                    scope = initializedScope,
                    dependencies = dependencies.initialized(
                        bubblesShower = bubblesHolder,
                        backButtonWidthProvider = backButtonDelegate,
                    ),
                    model = loggableModel,
                )
            }
        }

    @Composable
    override fun Content() = Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopStart,
    ) {
        val loggableProjector by initializedProjector.collectAsState()
        AnimatedContent(
            targetState = loggableProjector,
            contentKey = { it != null },
            modifier = Modifier.fillMaxSize(),
        ) { loggableProjectorLocal ->
            when (loggableProjectorLocal) {
                null -> InitializeStorage()
                else -> loggableProjectorLocal.Content()
            }
        }
        backButtonDelegate.Content()
        bubblesHolder.Content()
    }

    @Composable
    private fun InitializeStorage() = Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        ProgressIndicator(
            size = ProgressIndicatorSize.large,
        )
    }
}
