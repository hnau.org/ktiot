package hnau.ktiot.client.projector.init.impl

import hnau.ktiot.client.projector.init.api.InitProjector
import hnau.ktiot.client.projector.initialized.api.InitializedProjector

fun InitProjector.Factory.Companion.impl(
    initializedProjectorFactory: InitializedProjector.Factory,
): InitProjector.Factory = InitProjector.Factory { scope, dependencies, model ->
    InitProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        initializedProjectorFactory = initializedProjectorFactory,
    )
}