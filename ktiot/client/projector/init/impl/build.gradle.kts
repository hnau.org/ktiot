plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:init:api"))
            implementation(project(":ktiot:client:model:initialized:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:init:api"))
            implementation(project(":ktiot:client:projector:initialized:api"))
        }
    }
}
