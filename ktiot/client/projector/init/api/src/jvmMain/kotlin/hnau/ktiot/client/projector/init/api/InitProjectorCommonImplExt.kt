package hnau.ktiot.client.projector.init.api

import hnau.ktiot.client.projector.common.Localizer

actual fun InitProjector.Dependencies.Companion.commonImpl(
    localizer: Localizer,
): InitProjector.Dependencies = InitProjector.Dependencies.impl(
    localizer = localizer,
)