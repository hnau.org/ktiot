package hnau.ktiot.client.projector.init.api

import androidx.compose.runtime.Composable
import hnau.common.compose.uikit.bubble.BubblesShower
import hnau.ktiot.client.model.init.api.InitModel
import hnau.ktiot.client.projector.common.backbutton.BackButtonWidthProvider
import hnau.ktiot.client.projector.initialized.api.InitializedProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface InitProjector {

    @Shuffle
    interface Dependencies {

        fun initialized(
            bubblesShower: BubblesShower,
            backButtonWidthProvider: BackButtonWidthProvider,
        ): InitializedProjector.Dependencies

        companion object
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createInitProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: InitModel,
        ): InitProjector

        companion object
    }
}