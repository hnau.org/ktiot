package hnau.ktiot.client.projector.init.api

import hnau.ktiot.client.projector.common.Localizer

expect fun InitProjector.Dependencies.Companion.commonImpl(
    localizer: Localizer,
): InitProjector.Dependencies