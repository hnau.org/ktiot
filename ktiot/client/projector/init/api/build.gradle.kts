plugins {
    alias(libs.plugins.compose.desktop)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        jvmMain.dependencies {
            implementation(project(":ktiot:client:projector:common"))
        }
        commonMain.dependencies {
            implementation(project(":ktiot:client:model:init:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:initialized:api"))
        }
    }
}
