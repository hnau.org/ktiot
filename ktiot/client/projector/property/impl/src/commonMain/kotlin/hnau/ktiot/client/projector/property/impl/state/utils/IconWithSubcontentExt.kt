package hnau.ktiot.client.projector.property.impl.state.utils

import androidx.compose.animation.AnimatedContent
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.flow.StateFlow

internal fun <T> IconWithContent.IconWithSubcontent.Companion.create(
    state: StateFlow<T>,
    contentKey: (T) -> Any?,
    extractReadingFirstValueProjector: (T) -> IconWithContent.IconWithSubcontent,
): IconWithContent.IconWithSubcontent = create(
    Icon = {
        val currentState by state.collectAsState()
        AnimatedContent(
            targetState = currentState,
            contentKey = contentKey,
        ) { localState ->
            extractReadingFirstValueProjector(localState).Icon()
        }
    },
    Subcontent = {
        val currentState by state.collectAsState()
        AnimatedContent(
            targetState = currentState,
            contentKey = contentKey,
        ) { localState ->
            extractReadingFirstValueProjector(localState).Subcontent()
        }
    }
)