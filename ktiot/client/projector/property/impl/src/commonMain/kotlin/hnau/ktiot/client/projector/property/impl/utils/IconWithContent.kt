package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent.Content

data class IconWithContent(
    val iconWithSubcontent: IconWithSubcontent,
    val content: Content,
) {

    fun interface Content {

        @Composable
        fun Content()

        companion object {

            fun create(
                iconWithSubcontent: IconWithSubcontent,
                iconTitle: String,
            ): Content = Content {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                    verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = Dimens.separation),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
                    ) {
                        Text(
                            text = iconTitle,
                            modifier = Modifier.weight(1f),
                        )
                        iconWithSubcontent.Icon()
                    }
                    iconWithSubcontent.Subcontent()
                }
            }

            fun create(
                iconWithSubcontent: IconWithSubcontent,
                localizer: Localizer,
            ): Content = create(
                iconWithSubcontent = iconWithSubcontent,
                iconTitle = localizer.property.value,
            )
        }
    }

    interface IconWithSubcontent {

        @Composable
        fun Icon()

        @Composable
        fun Subcontent()

        companion object {

            fun create(
                Icon: @Composable () -> Unit,
                Subcontent: @Composable () -> Unit,
            ): IconWithSubcontent = object : IconWithSubcontent {

                @Composable
                override fun Icon() = Icon.invoke()

                @Composable
                override fun Subcontent() = Subcontent.invoke()
            }

            fun create(
                content: Content,
            ): IconWithSubcontent = create(
                Icon = {},
                Subcontent = { content.Content() },
            )
        }
    }

    companion object
}