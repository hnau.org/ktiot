package hnau.ktiot.client.projector.property.impl

import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.projector.property.api.PropertyProjector
import kotlinx.coroutines.CoroutineScope

fun PropertyProjector.Factory.Companion.impl(): PropertyProjector.Factory =
    object : PropertyProjector.Factory {
        override fun <T> createPropertyProjector(
            scope: CoroutineScope,
            dependencies: PropertyProjector.Dependencies,
            model: PropertyModel<T>,
        ): PropertyProjector<T> = PropertyProjectorImpl(
            scope = scope,
            dependencies = dependencies,
            model = model
        )
    }