package hnau.ktiot.client.projector.property.impl.utils

import hnau.ktiot.client.model.common.ornull.ValueOrCancel

val ValueOrCancel<() -> Unit>.onClick: () -> Unit
    get() = when (this) {
        is ValueOrCancel.Cancel -> cancel
        is ValueOrCancel.Value -> value
    }

/*
val ValueOrCancel<(Unit) -> Unit>.onClick: () -> Unit
    get() = when (this) {
        is ValueOrCancel.Cancel -> cancel
        is ValueOrCancel.Value -> {
            { value(Unit) }
        }
    }*/
