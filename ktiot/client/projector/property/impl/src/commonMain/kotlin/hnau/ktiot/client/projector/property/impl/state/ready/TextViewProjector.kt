package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.TextViewModel
import hnau.ktiot.client.projector.property.api.state.ready.TextViewProjectorDependencies

class TextViewProjector(
    private val model: TextViewModel,
    private val dependencies: TextViewProjectorDependencies,
) : ViewProjector<String> {

    @Composable
    override fun Content() {
        val text by model.text.collectAsState()
        val normalizedText: String = remember(text) {
            text
                .takeIf { it.isNotEmpty() }
                ?: dependencies.localizer.property.empty
        }
        Text(
            text = normalizedText,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
        )
    }

    companion object {

        fun factory(
            model: TextViewModel,
        ): ViewProjector.Factory<String> = ViewProjector.Factory { _, dependencies ->
            TextViewProjector(
                model = model,
                dependencies = dependencies.text(),
            )
        }
    }
}

internal fun ViewProjector.Companion.text(
    model: TextViewModel,
): ViewProjector.Factory<String> = TextViewProjector.factory(
    model = model,
)