package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import hnau.common.compose.uikit.TextInput
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.NumberPassiveModel

class NumberPassiveProjector(
    private val model: NumberPassiveModel,
) : PassiveProjector<Float> {

    @Composable
    override fun Content() {
        val focusRequester = remember { FocusRequester() }
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
            horizontalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            TextInput(
                value = model.text,
                isError = model.isError.collectAsState().value,
                maxLines = 1,
                modifier = Modifier
                    .weight(1f)
                    .focusRequester(focusRequester),
                keyboardActions = KeyboardActions { model.tryComplete() },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Decimal,
                    imeAction = ImeAction.Done,
                ),
            )
            model
                .viewType
                .suffix
                .takeIf { it.isNotEmpty() }
                ?.let { suffix -> Text(suffix) }
        }
    }

    companion object {

        fun factory(
            model: NumberPassiveModel,
        ): PassiveProjector.Factory<Float> = PassiveProjector.Factory { _, _ ->
            NumberPassiveProjector(
                model = model,
            )
        }
    }
}

internal fun PassiveProjector.Companion.number(
    model: NumberPassiveModel,
): PassiveProjector.Factory<Float> = NumberPassiveProjector.factory(
    model = model,
)