package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.vector.ImageVector
import hnau.common.compose.utils.Icon

@Immutable
internal data class StateButtonChipContent(
    val content: @Composable () -> Unit,
    val leading: (@Composable () -> Unit)? = null,
) {

    companion object {

        fun create(
            icon: Icons.Filled.() -> ImageVector,
            title: String? = null,
        ): StateButtonChipContent {
            val iconComposable: @Composable () -> Unit = { Icon(chooseIcon = icon) }
            return StateButtonChipContent(
                leading = when (title) {
                    null -> null
                    else -> iconComposable
                },
                content = when (title) {
                    null -> iconComposable
                    else -> {
                        { Text(title) }
                    }
                },
            )
        }
    }

}