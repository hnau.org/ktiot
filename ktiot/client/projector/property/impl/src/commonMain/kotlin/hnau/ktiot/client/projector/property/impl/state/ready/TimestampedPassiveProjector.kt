package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.chip.Chip
import hnau.common.compose.uikit.chip.ChipStyle
import hnau.common.compose.uikit.row.ChipsRow
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.kotlin.Timestamped
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedPassiveModel
import hnau.ktiot.client.projector.common.timestamp.TimestampEditProjector
import hnau.ktiot.client.projector.property.api.state.ready.TimestampedPassiveProjectorDependencies
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class TimestampedPassiveProjector<T>(
    scope: CoroutineScope,
    private val model: TimestampedPassiveModel<T>,
    private val dependencies: TimestampedPassiveProjectorDependencies,
) : PassiveProjector<Timestamped<T>> {

    sealed interface Page<out T> {

        val tab: TimestampedPassiveModel.Tab

        data class Timestamp(
            val projector: TimestampEditProjector,
        ) : Page<Nothing> {

            override val tab: TimestampedPassiveModel.Tab
                get() = TimestampedPassiveModel.Tab.Timestamp
        }

        data class Value<T>(
            val projector: PassiveProjector<T>,
        ) : Page<T> {

            override val tab: TimestampedPassiveModel.Tab
                get() = TimestampedPassiveModel.Tab.Value
        }
    }

    private val state: StateFlow<Page<T>> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, state) ->
            when (state) {
                is TimestampedPassiveModel.State.Timestamp -> Page.Timestamp(
                    TimestampEditProjector(
                        model = state.model,
                        dependencies = dependencies.timestampEdit(),
                        afterLastImeAction = MutableStateFlow(
                            TimestampEditProjector.ImeAction {
                                model.selectTab(TimestampedPassiveModel.Tab.Value)
                            },
                        ),
                        requestFocusOnLaunch = true,
                    )
                )

                is TimestampedPassiveModel.State.Value -> Page.Value(
                    PassiveProjector[state.model].create(
                        scope = stateScope,
                        dependencies = dependencies.passive,
                    )
                )
            }
        }

    @Composable
    override fun Content() {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            val currentPage by state.collectAsState()
            val selectedTab = currentPage.tab
            ChipsRow(
                all = TimestampedPassiveModel.Tab.entries,
            ) { tab, shape ->
                Chip(
                    content = {
                        Text(
                            text = when (tab) {
                                TimestampedPassiveModel.Tab.Timestamp ->
                                    dependencies.localizer.property.timestamp

                                TimestampedPassiveModel.Tab.Value ->
                                    dependencies.localizer.property.value
                            }
                        )
                    },
                    shape = shape,
                    style = when (tab) {
                        selectedTab -> ChipStyle.chipSelected
                        else -> ChipStyle.chip
                    },
                    onClick = { model.selectTab(tab) },
                )
            }
            AnimatedContent(
                modifier = Modifier.fillMaxWidth(),
                targetState = currentPage,
                contentKey = { page ->
                    when (page) {
                        is Page.Timestamp -> 0
                        is Page.Value -> 1
                    }
                },
            ) { page ->
                when (page) {
                    is Page.Timestamp -> page.projector.Content(
                        modifier = Modifier
                            .padding(
                                start = Dimens.separation,
                                end = Dimens.separation,
                                bottom = Dimens.separation,
                            ),
                    )

                    is Page.Value -> page.projector.Content()
                }
            }
        }
    }

    companion object {

        fun <T> factory(
            model: TimestampedPassiveModel<T>,
        ): PassiveProjector.Factory<Timestamped<T>> =
            PassiveProjector.Factory { scope, dependencies ->
                TimestampedPassiveProjector(
                    scope = scope,
                    model = model,
                    dependencies = dependencies.timestamped(dependencies),
                )
            }
    }
}

internal fun <T> PassiveProjector.Companion.timestamped(
    model: TimestampedPassiveModel<T>,
): PassiveProjector.Factory<Timestamped<T>> = TimestampedPassiveProjector.factory(
    model = model,
)