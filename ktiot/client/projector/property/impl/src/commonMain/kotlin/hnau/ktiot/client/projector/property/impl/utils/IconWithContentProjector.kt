package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.runtime.Composable

interface IconWithContentProjector {

    @Composable
    fun Icon() {
    }

    @Composable
    fun Content() {
    }
}
