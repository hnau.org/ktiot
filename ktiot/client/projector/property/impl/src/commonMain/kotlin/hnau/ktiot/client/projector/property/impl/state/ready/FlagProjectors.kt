package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Checkbox
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.FlagActiveModel
import hnau.ktiot.client.projector.common.BooleanView
import hnau.ktiot.client.projector.property.api.state.ready.FlagProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.CancelContent
import hnau.ktiot.client.projector.property.impl.utils.Content
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import kotlinx.coroutines.flow.StateFlow

class FlagProjector(
    private val model: FlagActiveModel,
    private val dependencies: FlagProjectorDependencies,
) : ActiveProjector<Boolean> {

    override val iconWithContent: IconWithContent = run {
        val iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
            Icon = {
                val value by model.value.collectAsState()
                val sendOrCancel: ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>> by model.sendOrCancelOrReadOnly.collectAsState()
                AnimatedContent(
                    targetState = sendOrCancel,
                ) { sendOrCancelLocal: ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>> ->
                    when (sendOrCancelLocal) {
                        ValueOrReadOnly.ReadOnly -> BooleanView(
                            value = value,
                            localizer = dependencies.localizer,
                            modifier = Modifier.padding(
                                vertical = Dimens.smallSeparation,
                            )
                        )

                        is ValueOrReadOnly.Value -> WritableIcon(
                            value = value,
                            sendOrCancel = sendOrCancelLocal.value,
                        )
                    }
                }
            },
            Subcontent = {},
        )
        IconWithContent(
            iconWithSubcontent = iconWithSubcontent,
            content = IconWithContent.Content.create(
                iconWithSubcontent = iconWithSubcontent,
                localizer = dependencies.localizer,
            ),
        )
    }

    @Composable
    private fun WritableIcon(
        value: Boolean,
        sendOrCancel: StateFlow<ValueOrCancel<(Boolean) -> Unit>>,
    ) {
        val currentSendOrCancel: ValueOrCancel<(Boolean) -> Unit> by sendOrCancel.collectAsState()
        currentSendOrCancel.Content(
            cancel = { cancel ->
                StateButtonChip(
                    onClick = cancel,
                    content = StateButtonChipContent(
                        content = { CancelContent() },
                    ),
                )
            },
            value = { send ->
                Checkbox(
                    checked = value,
                    onCheckedChange = send,
                )
            }
        )
    }

    companion object {

        fun factory(
            model: FlagActiveModel,
        ): ActiveProjector.Factory<Boolean> = ActiveProjector.Factory { _, dependencies ->
            FlagProjector(
                model = model,
                dependencies = dependencies.flag(),
            )
        }
    }
}

internal fun ActiveProjector.Companion.flag(
    model: FlagActiveModel,
): ActiveProjector.Factory<Boolean> = FlagProjector.factory(
    model = model,
)