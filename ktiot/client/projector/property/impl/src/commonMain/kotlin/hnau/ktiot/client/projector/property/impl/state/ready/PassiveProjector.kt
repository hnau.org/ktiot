package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.property.api.value.state.ready.NumberPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveBasedOnActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TextPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedPassiveModel
import hnau.ktiot.client.projector.property.api.state.ready.PassiveProjectorDependencies
import kotlinx.coroutines.CoroutineScope

interface PassiveProjector<out T> {

    @Composable
    fun Content()

    fun interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: PassiveProjectorDependencies,
        ): PassiveProjector<T>
    }

    companion object {

        operator fun <T> get(
            model: PassiveModel<T>,
        ): Factory<T> = when (model) {
            is NumberPassiveModel -> number(model)
            is PassiveBasedOnActiveModel -> basedOnActive(model)
            is TextPassiveModel -> text(model)
            is TimestampPassiveModel -> timestamp(model)
            is TimestampedPassiveModel<*> -> timestamped(model)
            is RGBPassiveModel -> rgb(model)
        } as Factory<T>
    }
}