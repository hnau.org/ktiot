package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import hnau.common.compose.utils.AnimatedOptionVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.toOption
import kotlinx.coroutines.flow.StateFlow

@Composable
internal fun <T> StateFlow<ValueOrReadOnly<T>>.Content(
    transitions: AnimatedVisibilityTransitions,
    content: @Composable (T) -> Unit,
) {
    val valueOrReadOnly by collectAsState()
    AnimatedOptionVisibility(
        value = valueOrReadOnly.toOption(),
        transitions = transitions,
    ) { onClickOrNullLocal: T ->
        content(onClickOrNullLocal)
    }
}