package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import hnau.common.color.RGBABytes
import hnau.common.color.gradient.map
import hnau.common.compose.uikit.TextInput
import hnau.common.compose.uikit.chip.Chip
import hnau.common.compose.uikit.chip.ChipStyle
import hnau.common.compose.uikit.color.compose
import hnau.common.compose.uikit.gradient.GradientSlider
import hnau.common.compose.uikit.row.ChipsRow
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.getTransitionSpecForHorizontalSlide
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.RGBPassivePageModel
import hnau.ktiot.client.projector.common.Sign

class RGBPassiveProjector(
    private val model: RGBPassiveModel,
) : PassiveProjector<RGBABytes> {

    @Composable
    override fun Content() {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
            verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            val selectedTabWithDelegate by model.selectedTabWithPage.collectAsState()
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
            ) {
                ChipsRow(
                    modifier = Modifier.weight(1f),
                    all = RGBPassiveModel.Tab.entries,
                ) { tab, shape ->
                    val selectedTab = selectedTabWithDelegate.first
                    val isSelected by remember(selectedTab) { derivedStateOf { tab == selectedTab } }
                    Chip(
                        content = {
                            Text(
                                text = when (tab) {
                                    RGBPassiveModel.Tab.HEX -> "#"
                                    RGBPassiveModel.Tab.RGB -> "RGB"
                                    RGBPassiveModel.Tab.HSL -> "HSL"
                                }
                            )
                        },
                        shape = shape,
                        style = when (isSelected) {
                            true -> ChipStyle.chipSelected
                            false -> ChipStyle.chip
                        },
                        onClick = { model.selectTab(tab) },
                    )
                }
                val color by model.currentOrNone.collectAsState()
                color.getOrNull().Sign()
            }
            AnimatedContent(
                modifier = Modifier.fillMaxWidth(),
                targetState = selectedTabWithDelegate,
                transitionSpec = getTransitionSpecForHorizontalSlide {
                    val (previousIndex) = initialState
                    val (nextIndex) = targetState
                    if (nextIndex.ordinal > previousIndex.ordinal) 1 else -1
                }
            ) { (_, currentDelegate) ->
                when (currentDelegate) {
                    is RGBPassivePageModel.Sliders -> SlidersPage(currentDelegate)
                    is RGBPassivePageModel.TextField -> TextFieldPage(currentDelegate)
                }
            }
        }

    }

    @Composable
    private fun SlidersPage(
        page: RGBPassivePageModel.Sliders,
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            page.sliders.forEach { slider ->
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
                ) {
                    Text(text = slider.prefix)
                    val state by slider.state.collectAsState()
                    GradientSlider(
                        modifier = Modifier.weight(1f),
                        gradient = state
                            .gradient
                            .let { gradient ->
                                remember(gradient) { gradient.map(RGBABytes::compose) }
                            },
                        value = state.fraction,
                        onValueChange = slider.updateFraction,
                    )
                }
            }
        }
    }

    @Composable
    private fun TextFieldPage(
        page: RGBPassivePageModel.TextField,
    ) {
        val focusRequester = remember { FocusRequester() }
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            Text(page.prefix)
            TextInput(
                value = page.value,
                isError = model.isError.collectAsState().value,
                maxLines = 1,
                modifier = Modifier
                    .weight(1f)
                    .focusRequester(focusRequester),
                keyboardActions = KeyboardActions { model.tryComplete() },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Decimal,
                    imeAction = ImeAction.Done,
                ),
            )
        }
    }

    companion object {

        fun factory(
            model: RGBPassiveModel,
        ): PassiveProjector.Factory<RGBABytes> = PassiveProjector.Factory { _, _ ->
            RGBPassiveProjector(
                model = model,
            )
        }
    }
}

internal fun PassiveProjector.Companion.rgb(
    model: RGBPassiveModel,
): PassiveProjector.Factory<RGBABytes> = RGBPassiveProjector.factory(
    model = model,
)