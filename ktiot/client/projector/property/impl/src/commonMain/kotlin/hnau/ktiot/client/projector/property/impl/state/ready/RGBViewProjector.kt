package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.color.RGBFloats
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.RGBViewModel
import hnau.ktiot.client.projector.common.Sign

class RGBViewProjector(
    private val model: RGBViewModel,
) : ViewProjector<RGBFloats> {

    @Composable
    override fun Content() {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
        ) {
            val rgb by model.hex.collectAsState()
            Text(
                text = rgb,
                modifier = Modifier
                    .weight(1f),
            )
            val color by model.color.collectAsState()
            color.Sign()
        }

    }

    companion object {

        fun factory(
            model: RGBViewModel,
        ): ViewProjector.Factory<RGBFloats> = ViewProjector.Factory { _, _ ->
            RGBViewProjector(
                model = model,
            )
        }
    }
}

internal fun ViewProjector.Companion.rgb(
    model: RGBViewModel,
): ViewProjector.Factory<RGBFloats> = RGBViewProjector.factory(
    model = model,
)