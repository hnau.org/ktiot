package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.animation.AnimatedContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import hnau.ktiot.client.model.common.ornull.ValueOrCancel

@Composable
internal fun <T> ValueOrCancel<T>.Content(
    modifier: Modifier = Modifier,
    cancel: @Composable (cancel: () -> Unit) -> Unit = { CancelContent() },
    value: @Composable (T) -> Unit,
) = AnimatedContent(
    modifier = modifier,
    targetState = this,
    contentKey = { sendOrCancel ->
        when (sendOrCancel) {
            is ValueOrCancel.Value -> 0
            is ValueOrCancel.Cancel -> 1
        }
    }
) { sendOrCancel ->
    when (sendOrCancel) {
        is ValueOrCancel.Value ->
            value(sendOrCancel.value)

        is ValueOrCancel.Cancel ->
            cancel(sendOrCancel.cancel)
    }
}