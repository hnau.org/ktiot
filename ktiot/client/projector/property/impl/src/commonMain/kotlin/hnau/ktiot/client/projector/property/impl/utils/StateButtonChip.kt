package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.material.ContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Shape
import hnau.common.compose.uikit.chip.Chip
import hnau.common.compose.uikit.chip.ChipStyle
import hnau.common.compose.uikit.shape.HnauShape

@Composable
internal fun StateButtonChip(
    onClick: (() -> Unit)?,
    content: StateButtonChipContent,
    modifier: Modifier = Modifier,
    primary: Boolean = true,
    shape: Shape = HnauShape(),
) {
    val enabled = onClick != null
    val alpha = when (enabled) {
        true -> 1f
        false -> ContentAlpha.disabled
    }
    Chip(
        onClick = onClick,
        modifier = modifier.alpha(alpha),
        activeColor = MaterialTheme.colors.primary,
        style = when (primary) {
            true -> ChipStyle.button
            false -> ChipStyle.chipHighlighted
        },
        shape = shape,
        leading = content.leading,
        content = content.content,
    )
}