package hnau.ktiot.client.projector.property.impl.state

import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.projector.property.api.state.ReadyProjectorDependencies
import hnau.ktiot.client.projector.property.impl.state.ready.ActiveProjector
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.CoroutineScope

internal class ReadyProjector<T>(
    scope: CoroutineScope,
    dependencies: ReadyProjectorDependencies,
    model: ReadyModel<T>,
) : StateStateProjector<T> {

    private val active: ActiveProjector<T> = ActiveProjector[model.active].create(
        scope = scope,
        dependencies = dependencies.active(),
    )

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent
        get() = active.iconWithContent.iconWithSubcontent
}