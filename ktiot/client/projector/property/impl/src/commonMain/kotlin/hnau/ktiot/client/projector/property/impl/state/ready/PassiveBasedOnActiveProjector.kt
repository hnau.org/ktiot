package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.property.api.value.state.ready.PassiveBasedOnActiveModel
import hnau.ktiot.client.projector.property.api.state.ready.PassiveBasedOnActiveProjectorDependencies
import kotlinx.coroutines.CoroutineScope

class PassiveBasedOnActiveProjector<T>(
    scope: CoroutineScope,
    dependencies: PassiveBasedOnActiveProjectorDependencies,
    private val model: PassiveBasedOnActiveModel<T>,
) : PassiveProjector<T> {

    private val delegate: ActiveProjector<T> = ActiveProjector[model.model].create(
        scope = scope,
        dependencies = dependencies.active,
    )

    @Composable
    override fun Content() {
        delegate.iconWithContent.content.Content()
    }

    companion object {

        fun <T> factory(
            model: PassiveBasedOnActiveModel<T>,
        ): PassiveProjector.Factory<T> = PassiveProjector.Factory { scope, dependencies ->
            PassiveBasedOnActiveProjector(
                model = model,
                scope = scope,
                dependencies = dependencies.basedOnActive(),
            )
        }
    }
}

internal fun <T> PassiveProjector.Companion.basedOnActive(
    model: PassiveBasedOnActiveModel<T>,
): PassiveProjector.Factory<T> = PassiveBasedOnActiveProjector.factory(
    model = model,
)