package hnau.ktiot.client.projector.property.impl.state

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.Edit
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.progressindicator.ProgressIndicator
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorSize
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.ktiot.client.model.property.api.value.state.ReadingFirstValueModel
import hnau.ktiot.client.projector.common.Content
import hnau.ktiot.client.projector.property.api.state.ReadingFirstValueProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent

internal class ReadingFirstValueProjector<T>(
    model: ReadingFirstValueModel<T>,
    private val dependencies: ReadingFirstValueProjectorDependencies,
) : StateStateProjector<T> {

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent =
        IconWithContent.IconWithSubcontent.create(
            Icon = {
                model.initOrNull.Content(
                    transitions = AnimatedVisibilityTransitions.vertical,
                ) { init ->
                    StateButtonChip(
                        onClick = init,
                        content = StateButtonChipContent.create(
                            icon = { Edit },
                        ),
                    )
                }
            },
            Subcontent = {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .padding(
                            bottom = Dimens.separation,
                            start = Dimens.separation,
                            end = Dimens.separation,
                        ),
                ) {
                    ProgressIndicator(
                        size = ProgressIndicatorSize.small,
                    )
                    Text(
                        text = dependencies.localizer.property.loading,
                    )
                }
            },
        )
}