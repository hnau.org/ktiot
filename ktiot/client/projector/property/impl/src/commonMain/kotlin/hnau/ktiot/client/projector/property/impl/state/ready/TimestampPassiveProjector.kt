package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampPassiveModel
import hnau.ktiot.client.projector.common.timestamp.TimestampEditProjector
import hnau.ktiot.client.projector.property.api.state.ready.TimestampPassiveProjectorDependencies
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.datetime.Instant

class TimestampPassiveProjector(
    private val model: TimestampPassiveModel,
    private val dependencies: TimestampPassiveProjectorDependencies,
) : PassiveProjector<Instant> {

    private val delegate = TimestampEditProjector(
        model = model.delegate,
        dependencies = dependencies.delegate(),
        requestFocusOnLaunch = true,
        afterLastImeAction = MutableStateFlow(
            TimestampEditProjector.ImeAction(
                action = model::tryComplete,
            )
        ),
    )

    @Composable
    override fun Content() {
        delegate.Content(
            modifier = Modifier.padding(
                start = Dimens.separation,
                end = Dimens.separation,
                bottom = Dimens.separation,
            )
        )
    }

    companion object {

        fun factory(
            model: TimestampPassiveModel,
        ): PassiveProjector.Factory<Instant> = PassiveProjector.Factory { _, dependencies ->
            TimestampPassiveProjector(
                model = model,
                dependencies = dependencies.timestamp(),
            )
        }
    }
}

internal fun PassiveProjector.Companion.timestamp(
    model: TimestampPassiveModel,
): PassiveProjector.Factory<Instant> = TimestampPassiveProjector.factory(
    model = model,
)