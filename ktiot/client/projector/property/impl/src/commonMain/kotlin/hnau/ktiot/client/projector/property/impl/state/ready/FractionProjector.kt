package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import hnau.common.color.RGBABytes
import hnau.common.color.gradient.Gradient
import hnau.common.color.gradient.map
import hnau.common.compose.uikit.color.compose
import hnau.common.compose.uikit.gradient.GradientSlider
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedNullableVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.ktiot.client.model.common.ValueOrFinished
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.FractionActiveModel
import hnau.ktiot.client.projector.property.api.state.ready.FractionProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.CancelContent
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import kotlinx.coroutines.flow.StateFlow
import kotlin.math.pow
import kotlin.math.round

class FractionProjector(
    private val model: FractionActiveModel,
    private val dependencies: FractionProjectorDependencies,
) : ActiveProjector<Float> {

    private val viewType: PropertyViewType.State.Fraction
        get() = model.viewType

    private val value: StateFlow<Float>
        get() = model.value

    private val gradient: Gradient<Color> =
        viewType.gradient.map(RGBABytes::compose)

    override val iconWithContent: IconWithContent = run {
        val iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
            Icon = {
                val value by value.collectAsState()
                Value(value)
            },
            Subcontent = { Content() },
        )
        IconWithContent(
            iconWithSubcontent = iconWithSubcontent,
            content = IconWithContent.Content.create(
                iconWithSubcontent = iconWithSubcontent,
                localizer = dependencies.localizer,
            ),
        )
    }

    @Composable
    private fun Content() = Column(
        modifier = Modifier.fillMaxWidth(),
    ) {
        val value by value.collectAsState()
        val sendOrCancel by model.sendOrCancelOrReadOnly.collectAsState()
        AnimatedContent(
            targetState = sendOrCancel,
            modifier = Modifier
                .fillMaxWidth(),
        ) { sendOrCancelLocal ->
            when (sendOrCancelLocal) {
                ValueOrReadOnly.ReadOnly -> ShowReadOnlyContent(
                    value = value,
                )

                is ValueOrReadOnly.Value -> ShowWriteableContent(
                    value = value,
                    sendOrCancel = sendOrCancelLocal.value,
                )
            }
        }
    }

    @Composable
    private fun minMax() {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Value(
                value = viewType.min,
                style = MaterialTheme.typography.caption,

                )
            Spacer(Modifier.weight(1f))
            Value(
                value = viewType.max,
                style = MaterialTheme.typography.caption,
            )
        }
    }

    @Composable
    private fun Value(
        value: Float,
        style: TextStyle = LocalTextStyle.current,
    ) {
        val text = remember(value, viewType) {
            when (val display = viewType.display) {
                PropertyViewType.State.Fraction.Display.Percent -> {
                    val percentValue =
                        round(100 * calcPercentage0to1(value)).toInt()
                    "$percentValue%"
                }

                is PropertyViewType.State.Fraction.Display.Raw -> {
                    val factor = 10f.pow(display.decimalPlaces)
                    val textValue = round(value * factor) / factor
                    textValue.toString() + display.suffix
                }
            }
        }
        Text(
            text = text,
            style = style,
        )
    }

    private fun calcPercentage0to1(value: Float): Float =
        (value - viewType.min) / (viewType.max - viewType.min)

    @Composable
    private fun ShowReadOnlyContent(
        value: Float,
    ) {
        Column {
            val normalizedValue = calcPercentage0to1(value)
            val animatedValue by animateFloatAsState(
                targetValue = normalizedValue,
            )
            GradientSlider(
                value = animatedValue,
                onValueChange = {},
                enabled = false,
                gradient = gradient,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = Dimens.smallSeparation),
            )
            minMax()
        }
    }

    @Composable
    private fun ShowWriteableContent(
        value: Float,
        sendOrCancel: StateFlow<ValueOrCancel<(ValueOrFinished<Float>) -> Unit>>,
    ) = Row(
        verticalAlignment = Alignment.CenterVertically,
    ) {
        val currentSendOrCancel by sendOrCancel.collectAsState()
        val (send, cancel) = when (val localCurrentSendOrCancel = currentSendOrCancel) {
            is ValueOrCancel.Cancel -> null to localCurrentSendOrCancel.cancel
            is ValueOrCancel.Value -> localCurrentSendOrCancel.value to null
        }
        Column(
            modifier = Modifier
                .weight(1f),
        ) {
            GradientSlider(
                value = value,
                valueRange = viewType.min..viewType.max,
                onValueChange = { newValue -> send?.invoke(ValueOrFinished.Value(newValue)) },
                onValueChangeFinished = { send?.invoke(ValueOrFinished.Finished) },
                enabled = send != null,
                modifier = Modifier
                    .padding(horizontal = Dimens.smallSeparation),
                gradient = gradient,
            )
            minMax()
        }
        AnimatedNullableVisibility(
            value = cancel,
            transitions = AnimatedVisibilityTransitions.horizontal,
        ) { localCancel ->
            StateButtonChip(
                modifier = Modifier
                    .padding(end = Dimens.smallSeparation),
                onClick = localCancel,
                content = StateButtonChipContent(
                    content = { CancelContent() },
                ),
            )
        }
    }

    companion object {

        fun factory(
            model: FractionActiveModel,
        ): ActiveProjector.Factory<Float> = ActiveProjector.Factory { _, dependencies ->
            FractionProjector(
                model = model,
                dependencies = dependencies.fraction(),
            )
        }
    }
}

internal fun ActiveProjector.Companion.fraction(
    model: FractionActiveModel,
): ActiveProjector.Factory<Float> = FractionProjector.factory(
    model = model,
)