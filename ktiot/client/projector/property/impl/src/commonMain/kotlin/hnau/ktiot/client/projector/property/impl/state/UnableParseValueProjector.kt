package hnau.ktiot.client.projector.property.impl.state

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.UnableParseValueModel
import hnau.ktiot.client.projector.property.api.state.UnableParseValueProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent

class UnableParseValueProjector<T>(
    model: UnableParseValueModel<T>,
    private val dependencies: UnableParseValueProjectorDependencies,
) : StateStateProjector<T> {

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent =
        IconWithContent.IconWithSubcontent.create(
            Icon = {},
            Subcontent = {
                Text(
                    text = dependencies.localizer.property.unableParseValue,
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            start = Dimens.separation,
                            end = Dimens.separation,
                            bottom = Dimens.separation,
                        ),
                )
            }
        )
}