package hnau.ktiot.client.projector.property.impl

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.BackHand
import androidx.compose.material.icons.filled.Calculate
import androidx.compose.material.icons.filled.Memory
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.Space
import hnau.common.compose.uikit.shape.HnauShape
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.Icon
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.projector.common.title
import hnau.ktiot.client.projector.property.api.PropertyProjector
import hnau.ktiot.scheme.property.ValueSource
import kotlinx.coroutines.CoroutineScope

internal class PropertyProjectorImpl<T>(
    scope: CoroutineScope,
    private val dependencies: PropertyProjector.Dependencies,
    private val model: PropertyModel<T>,
) : PropertyProjector<T> {


    private val valueProjector = ValueProjector[model.value].create(
        scope = scope,
        dependencies = dependencies.value(),
    )

    @Composable
    override fun Content() = Card(
        shape = HnauShape(),
    ) {
        Column {
            Row(
                modifier = Modifier
                    .height(Dimens.rowHeight),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.size(Dimens.rowHeight),
                ) {
                    Icon(
                        modifier = Modifier.size(24.dp),
                    ) {
                        when (model.source) {
                            ValueSource.Hardware -> Memory
                            ValueSource.Calculated -> Calculate
                            ValueSource.Manual -> BackHand
                        }
                    }
                }
                Text(
                    text = model.keyOrTitle.title(dependencies.localizer),
                    modifier = Modifier.weight(1f),
                )
                Space(Dimens.separation)
                valueProjector.iconWithSubcontent.Icon()
                Space(Dimens.separation)
            }
            valueProjector.iconWithSubcontent.Subcontent()
        }
    }
}
