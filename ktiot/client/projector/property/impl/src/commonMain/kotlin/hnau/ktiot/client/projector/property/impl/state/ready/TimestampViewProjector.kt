package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampViewModel
import hnau.ktiot.client.projector.property.api.state.ready.TimestampViewProjectorDependencies
import kotlinx.datetime.Instant
import hnau.ktiot.client.projector.common.timestamp.TimestampViewProjector as DelegateProjector

class TimestampViewProjector(
    private val model: TimestampViewModel,
    private val dependencies: TimestampViewProjectorDependencies,
) : ViewProjector<Instant> {

    private val delegate = DelegateProjector(
        model = model.delegate,
        dependencies = dependencies.delegate(),
    )

    @Composable
    override fun Content() {
        delegate.Content(
            modifier = Modifier.padding(
                start = Dimens.separation,
                end = Dimens.separation,
                bottom = Dimens.separation,
            )
        )
    }

    companion object {

        fun factory(
            model: TimestampViewModel,
        ): ViewProjector.Factory<Instant> = ViewProjector.Factory { _, dependencies ->
            TimestampViewProjector(
                model = model,
                dependencies = dependencies.timestamp(),
            )
        }
    }
}

internal fun ViewProjector.Companion.timestamp(
    model: TimestampViewModel,
): ViewProjector.Factory<Instant> = TimestampViewProjector.factory(
    model = model,
)