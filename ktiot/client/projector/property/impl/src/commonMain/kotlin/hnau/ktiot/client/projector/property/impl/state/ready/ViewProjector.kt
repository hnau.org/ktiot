package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.property.api.value.state.ready.NumberViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.RGBViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.TextViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedViewModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewBasedOnActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.client.projector.property.api.state.ready.ViewProjectorDependencies
import kotlinx.coroutines.CoroutineScope

interface ViewProjector<T> {

    @Composable
    fun Content()

    fun interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: ViewProjectorDependencies,
        ): ViewProjector<T>
    }

    companion object {

        operator fun <T> get(
            model: ViewModel<T>,
        ): Factory<T> = when (model) {
            is NumberViewModel -> number(model)
            is TextViewModel -> text(model)
            is TimestampViewModel -> timestamp(model)
            is TimestampedViewModel<*> -> timestamped(model)
            is RGBViewModel -> rgb(model)
            is ViewBasedOnActiveModel -> basedOnActive(model)
        } as Factory<T>
    }
}