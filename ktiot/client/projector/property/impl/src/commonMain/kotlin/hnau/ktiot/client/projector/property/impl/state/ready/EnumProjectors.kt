package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.UnfoldMore
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.Icon
import hnau.common.compose.utils.clickableOption
import hnau.ktiot.client.model.property.api.value.state.ready.EnumActiveModel
import hnau.ktiot.client.projector.property.impl.utils.CancelContent
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import java.util.Locale

class EnumProjector(
    private val model: EnumActiveModel,
) : ActiveProjector<String> {

    override val iconWithContent: IconWithContent = run {
        val content = IconWithContent.Content {
            val state by model.state.collectAsState()
            AnimatedContent(
                targetState = state,
                contentKey = { stateLocal ->
                    when (stateLocal) {
                        is EnumActiveModel.State.Closed -> 0
                        is EnumActiveModel.State.Opened -> 1
                    }
                },
                modifier = Modifier.fillMaxWidth(),
            ) { stateLocal ->
                when (stateLocal) {
                    is EnumActiveModel.State.Closed -> ClosedContent(stateLocal)
                    is EnumActiveModel.State.Opened -> OpenedContent(stateLocal)
                }
            }
        }
        IconWithContent(
            iconWithSubcontent = IconWithContent.IconWithSubcontent.create(content),
            content = content,
        )
    }

    @Composable
    private fun ClosedContent(
        state: EnumActiveModel.State.Closed,
    ) {
        val openOrNull: (() -> Unit)? = state.open
        Item(
            variant = state.selectedVariant,
            onClick = openOrNull,
            icon = openOrNull?.let { { Icon { UnfoldMore } } },
        )
    }

    @Composable
    private fun OpenedContent(
        state: EnumActiveModel.State.Opened,
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
        ) {
            state.items.forEach { item ->
                val itemState by item.state.collectAsState()
                Item(
                    variant = item.variant,
                    onClick = when (val localItemState = itemState) {
                        is EnumActiveModel.State.Opened.Item.State.NotSelected -> localItemState.selectIfNotSelecting
                        is EnumActiveModel.State.Opened.Item.State.Selected -> localItemState.closeIfNotSelecting
                        is EnumActiveModel.State.Opened.Item.State.Selecting -> localItemState.cancel
                    },
                    icon = when (itemState) {
                        is EnumActiveModel.State.Opened.Item.State.Selecting -> {
                            { CancelContent() }
                        }

                        is EnumActiveModel.State.Opened.Item.State.NotSelected -> null
                        is EnumActiveModel.State.Opened.Item.State.Selected -> {
                            { Icon { Done } }
                        }
                    },
                )
            }
        }
    }

    @Composable
    private fun Item(
        variant: PropertyViewType.State.Enum.Variant,
        onClick: (() -> Unit)?,
        icon: (@Composable () -> Unit)?,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickableOption(onClick)
                .height(Dimens.rowHeight)
                .padding(horizontal = Dimens.separation),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(Dimens.smallSeparation)
        ) {
            Text(
                modifier = Modifier.weight(1f),
                text = variant.customTitle ?: variant.key.replaceFirstChar { char ->
                    when (char.isLowerCase()) {
                        true -> char.titlecase(Locale.getDefault())
                        false -> char.toString()
                    }
                },
            )
            icon?.invoke()
        }
    }

    companion object {

        fun factory(
            model: EnumActiveModel,
        ): ActiveProjector.Factory<String> = ActiveProjector.Factory { _, _ ->
            EnumProjector(
                model = model,
            )
        }
    }
}

internal fun ActiveProjector.Companion.enum(
    model: EnumActiveModel,
): ActiveProjector.Factory<String> = EnumProjector.factory(
    model = model,
)