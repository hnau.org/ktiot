package hnau.ktiot.client.projector.property.impl.utils

import androidx.compose.foundation.layout.Box
import androidx.compose.material.LocalContentColor
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.progressindicator.ProgressIndicator
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorSize
import hnau.common.compose.utils.Icon

@Composable
internal fun CancelContent(
    modifier: Modifier = Modifier,
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center,
    ) {
        ProgressIndicator(
            size = ProgressIndicatorSize.small,
            color = LocalContentColor.current,
        )
        Icon { Clear }
    }
}