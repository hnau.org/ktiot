package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.ktiot.client.model.common.ornull.ValueOrDisabled
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.common.ornull.toOption
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveBasedOnPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.model.property.api.value.state.ready.ViewModel
import hnau.ktiot.client.projector.property.api.state.ready.ActiveBasedOnPassiveProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.Content
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class ActiveBasedOnPassiveProjector<T>(
    scope: CoroutineScope,
    model: ActiveBasedOnPassiveModel<T>,
    dependencies: ActiveBasedOnPassiveProjectorDependencies,
) : ActiveProjector<T> {

    private sealed interface State<T> {

        val key: Int

        val iconWithContent: IconWithContent

        class View<T>(
            scope: CoroutineScope,
            model: ViewModel<T>,
            private val edit: StateFlow<ValueOrReadOnly<StateFlow<ValueOrDisabled<() -> Unit>>>>,
            private val dependencies: ActiveBasedOnPassiveProjectorDependencies.View,
        ) : State<T> {

            override val key: Int
                get() = 0

            private val delegate: ViewProjector<T> = ViewProjector[model].create(
                scope = scope,
                dependencies = dependencies.delegate(),
            )

            @Composable
            private fun Icon(
                compact: Boolean,
                modifier: Modifier = Modifier,
            ) {
                edit.Content(
                    transitions = AnimatedVisibilityTransitions.vertical,
                ) { editOrCancelFlow: StateFlow<ValueOrDisabled<() -> Unit>> ->
                    val editOrDisabled: ValueOrDisabled<() -> Unit> by editOrCancelFlow.collectAsState()
                    val editOrNull = editOrDisabled.toOption().getOrNull()
                    StateButtonChip(
                        modifier = modifier,
                        onClick = editOrNull,
                        content = StateButtonChipContent.create(
                            icon = { Edit },
                            title = dependencies.localizer.property.edit.takeIf { !compact },
                        ),
                    )
                }
            }

            override val iconWithContent: IconWithContent = run {
                IconWithContent(
                    content = {
                        Column(
                            horizontalAlignment = Alignment.End,
                        ) {
                            delegate.Content()
                            Icon(
                                compact = false,
                                modifier = Modifier.padding(
                                    start = Dimens.separation,
                                    end = Dimens.separation,
                                    bottom = Dimens.separation,
                                )
                            )
                        }
                    },
                    iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
                        Icon = { Icon(compact = true) },
                        Subcontent = { delegate.Content() },
                    ),
                )
            }
        }

        class Edit<T>(
            scope: CoroutineScope,
            model: EditModel<T>,
            dependencies: ActiveBasedOnPassiveProjectorDependencies.Edit,
        ) : State<T> {

            override val key: Int
                get() = 1

            private val delegate: EditProjector<T> = EditProjector(
                scope = scope,
                model = model,
                dependencies = dependencies.edit(),
            )

            override val iconWithContent: IconWithContent
                get() = delegate.iconWithContent
        }

    }

    private val state: StateFlow<State<T>> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (modelStateScope, modelState) ->
            when (modelState) {
                is ActiveBasedOnPassiveModel.State.Edit -> State.Edit(
                    scope = modelStateScope,
                    model = modelState.model,
                    dependencies = dependencies.edit(),
                )

                is ActiveBasedOnPassiveModel.State.View -> State.View(
                    scope = modelStateScope,
                    model = modelState.model,
                    dependencies = dependencies.view(),
                    edit = modelState.edit,
                )
            }
        }

    override val iconWithContent: IconWithContent = IconWithContent(
        iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
            Icon = {
                val state by state.collectAsState()
                AnimatedContent(
                    targetState = state,
                    contentKey = State<T>::key
                ) { stateLocal ->
                    stateLocal.iconWithContent.iconWithSubcontent.Icon()
                }
            },
            Subcontent = {
                val state by state.collectAsState()
                AnimatedContent(
                    targetState = state,
                    contentKey = State<T>::key
                ) { stateLocal ->
                    stateLocal.iconWithContent.iconWithSubcontent.Subcontent()
                }
            },
        ),
        content = {
            val state by state.collectAsState()
            AnimatedContent(
                targetState = state,
                contentKey = State<T>::key
            ) { stateLocal ->
                stateLocal.iconWithContent.content.Content()
            }
        },
    )

    companion object {

        fun <T> factory(
            model: ActiveBasedOnPassiveModel<T>,
        ): ActiveProjector.Factory<T> = ActiveProjector.Factory { scope, dependencies ->
            ActiveBasedOnPassiveProjector(
                scope = scope,
                model = model,
                dependencies = dependencies.basedOnPassive(dependencies),
            )
        }
    }
}

internal fun <T> ActiveProjector.Companion.basedOnPassive(
    model: ActiveBasedOnPassiveModel<T>,
): ActiveProjector.Factory<T> = ActiveBasedOnPassiveProjector.factory(
    model = model,
)