package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LocalContentColor
import androidx.compose.material.Text
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.progressindicator.ProgressIndicator
import hnau.common.compose.uikit.progressindicator.ProgressIndicatorSize
import hnau.common.compose.uikit.shape.HnauShapeInRow
import hnau.common.compose.uikit.shape.end
import hnau.common.compose.uikit.shape.start
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.toOption
import hnau.ktiot.client.model.property.api.value.state.ready.EditModel
import hnau.ktiot.client.projector.property.api.state.ready.EditProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import kotlinx.coroutines.CoroutineScope

internal class EditProjector<T>(
    scope: CoroutineScope,
    private val model: EditModel<T>,
    private val dependencies: EditProjectorDependencies,
) {

    private val passive: PassiveProjector<T> = PassiveProjector[model.passive].create(
        scope = scope,
        dependencies = dependencies.passive(),
    )

    val iconWithContent: IconWithContent = createCancelDone()

    private fun createCancelDone(): IconWithContent {

        @Composable
        fun Buttons(
            compact: Boolean,
            modifier: Modifier = Modifier,
        ) {
            val actionsOrCancel: ValueOrCancel<EditModel.Actions> by model.actions.collectAsState()
            AnimatedContent(
                targetState = actionsOrCancel,
                contentKey = { localActionsOrCancel ->
                    when (localActionsOrCancel) {
                        is ValueOrCancel.Cancel -> 0
                        is ValueOrCancel.Value -> 1
                    }
                }
            ) { localActionsOrCancel ->
                when (localActionsOrCancel) {
                    is ValueOrCancel.Cancel -> StateButtonChip(
                        modifier = modifier,
                        onClick = localActionsOrCancel.cancel,
                        content = StateButtonChipContent(
                            leading = {
                                ProgressIndicator(
                                    size = ProgressIndicatorSize.small,
                                    color = LocalContentColor.current,
                                )
                            },
                            content = { Text(dependencies.localizer.common.cancel) },
                        ),
                    )

                    is ValueOrCancel.Value -> Row(
                        modifier = modifier,
                        horizontalArrangement = Arrangement.spacedBy(Dimens.chipsSeparation),
                    ) {
                        val actions = localActionsOrCancel.value
                        StateButtonChip(
                            onClick = actions.cancel,
                            content = StateButtonChipContent.create(
                                icon = { Cancel },
                                title = dependencies.localizer.common.cancel.takeIf { !compact },
                            ),
                            primary = false,
                            shape = HnauShapeInRow.start,
                        )
                        val sendOrDisabled by actions.send.collectAsState()
                        val sendOrNull = sendOrDisabled.toOption().getOrNull()
                        StateButtonChip(
                            onClick = sendOrNull,
                            content = StateButtonChipContent.create(
                                icon = { Done },
                                title = dependencies.localizer.property.save.takeIf { !compact },
                            ),
                            shape = HnauShapeInRow.end,
                        )
                    }
                }
            }
        }

        return IconWithContent(
            iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
                Icon = { Buttons(compact = true) },
                Subcontent = { passive.Content() },
            ),
            content = {
                Column(
                    horizontalAlignment = Alignment.End,
                ) {
                    passive.Content()
                    Buttons(
                        compact = false,
                        modifier = Modifier.padding(
                            start = Dimens.separation,
                            end = Dimens.separation,
                            bottom = Dimens.separation,
                        )
                    )
                }
            }
        )
    }
}