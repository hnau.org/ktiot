package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.input.ImeAction
import hnau.common.compose.uikit.TextInput
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.TextPassiveModel

class TextPassiveProjector(
    private val model: TextPassiveModel,
) : PassiveProjector<String> {

    @Composable
    override fun Content() {
        val focusRequester = remember { FocusRequester() }
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
        TextInput(
            value = model.text,
            maxLines = Int.MAX_VALUE,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                )
                .focusRequester(focusRequester),
            keyboardActions = KeyboardActions { model.tryComplete() },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
            ),
        )
    }

    companion object {

        fun factory(
            model: TextPassiveModel,
        ): PassiveProjector.Factory<String> = PassiveProjector.Factory { _, _ ->
            TextPassiveProjector(
                model = model,
            )
        }
    }
}

internal fun PassiveProjector.Companion.text(
    model: TextPassiveModel,
): PassiveProjector.Factory<String> = TextPassiveProjector.factory(
    model = model,
)