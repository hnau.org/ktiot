package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.kotlin.Timestamped
import hnau.ktiot.client.model.property.api.value.state.ready.TimestampedViewModel
import hnau.ktiot.client.projector.common.timestamp.TimestampViewProjector
import hnau.ktiot.client.projector.property.api.state.ready.TimestampedViewProjectorDependencies
import kotlinx.coroutines.CoroutineScope

class TimestampedViewProjector<T>(
    scope: CoroutineScope,
    private val model: TimestampedViewModel<T>,
    private val dependencies: TimestampedViewProjectorDependencies,
) : ViewProjector<Timestamped<T>> {

    private val timestamp: TimestampViewProjector = TimestampViewProjector(
        model = model.timestamp,
        dependencies = dependencies.timestamp(),
    )

    private val value: ViewProjector<T> = ViewProjector[model.value].create(
        scope = scope,
        dependencies = dependencies.view,
    )

    @Composable
    override fun Content() {
        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        ) {
            timestamp.Content(
                modifier = Modifier
                    .padding(
                        horizontal = Dimens.separation,
                    ),
            )
            value.Content()
        }
    }

    companion object {

        fun <T> factory(
            model: TimestampedViewModel<T>,
        ): ViewProjector.Factory<Timestamped<T>> = ViewProjector.Factory { scope, dependencies ->
            TimestampedViewProjector(
                scope = scope,
                model = model,
                dependencies = dependencies.timestamped(dependencies),
            )
        }
    }
}

internal fun <T> ViewProjector.Companion.timestamped(
    model: TimestampedViewModel<T>,
): ViewProjector.Factory<Timestamped<T>> = TimestampedViewProjector.factory(
    model = model,
)