package hnau.ktiot.client.projector.property.impl

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.filled.AdsClick
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.AnimatedOptionVisibility
import hnau.common.compose.utils.AnimatedVisibilityTransitions
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.toOption
import hnau.ktiot.client.model.property.api.value.TicModel
import hnau.ktiot.client.projector.property.api.TicProjectorDependencies
import hnau.ktiot.client.projector.property.api.ValueProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.CancelContent
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import hnau.ktiot.client.projector.property.impl.utils.onClick
import kotlinx.coroutines.CoroutineScope

class TicProjector(
    private val dependencies: TicProjectorDependencies,
    private val model: TicModel,
) : ValueProjector<Unit> {

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent =
        IconWithContent.IconWithSubcontent
            .create(
                Icon = {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(start = Dimens.separation),
                        horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
                    ) {
                        val isTicking by model.isTicking.collectAsState()
                        Spacer(
                            modifier = Modifier
                                .background(
                                    color = when (isTicking) {
                                        true -> MaterialTheme.colors.primary
                                        false -> MaterialTheme.colors.onSurface
                                    },
                                    shape = CircleShape,
                                )
                                .size(16.dp),
                        )
                        val sendOrReadOnly by model.sendTicOrNullOrReadOnly.collectAsState()
                        AnimatedOptionVisibility(
                            value = sendOrReadOnly.toOption(),
                            transitions = AnimatedVisibilityTransitions.horizontal,
                        ) { sendOrCancelLocal ->
                            val currentSendOrCancel by sendOrCancelLocal.collectAsState()
                            StateButtonChip(
                                onClick = currentSendOrCancel.onClick,
                                content = when (currentSendOrCancel) {
                                    is ValueOrCancel.Cancel -> StateButtonChipContent(
                                        content = { CancelContent() },
                                    )

                                    is ValueOrCancel.Value -> StateButtonChipContent.create(
                                        icon = { AdsClick },
                                    )
                                }
                            )
                        }
                    }
                },
                Subcontent = { },
            )

    companion object {

        fun factory(
            model: TicModel,
        ): ValueProjector.Factory<Unit> = object : ValueProjector.Factory<Unit> {

            override fun create(
                scope: CoroutineScope,
                dependencies: ValueProjectorDependencies,
            ): ValueProjector<Unit> = TicProjector(
                dependencies = dependencies.tic(),
                model = model,
            )
        }
    }
}

internal fun ValueProjector.Companion.tic(
    model: TicModel,
): ValueProjector.Factory<Unit> = TicProjector.factory(
    model = model,
)
