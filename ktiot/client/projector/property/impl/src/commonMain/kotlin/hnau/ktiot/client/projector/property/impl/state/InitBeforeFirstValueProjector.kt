package hnau.ktiot.client.projector.property.impl.state

import hnau.ktiot.client.model.property.api.value.state.InitBeforeFirstValueModel
import hnau.ktiot.client.projector.property.api.state.InitBeforeFirstValueProjectorDependencies
import hnau.ktiot.client.projector.property.impl.state.ready.EditProjector
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.CoroutineScope

internal class InitBeforeFirstValueProjector<T>(
    scope: CoroutineScope,
    dependencies: InitBeforeFirstValueProjectorDependencies,
    model: InitBeforeFirstValueModel<T>,
) : StateStateProjector<T> {

    private val delegate = EditProjector(
        scope = scope,
        model = model.delegate,
        dependencies = dependencies.edit(dependencies.active()),
    )

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent =
        delegate.iconWithContent.iconWithSubcontent
}