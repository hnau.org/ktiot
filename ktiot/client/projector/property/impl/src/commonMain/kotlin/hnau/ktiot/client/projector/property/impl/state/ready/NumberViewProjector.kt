package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import hnau.common.compose.uikit.utils.Dimens
import hnau.ktiot.client.model.property.api.value.state.ready.NumberViewModel

class NumberViewProjector(
    private val model: NumberViewModel,
) : ViewProjector<Float> {

    @Composable
    override fun Content() {
        val viewType = model.viewType
        val number by model.number.collectAsState()
        Text(
            text = remember(number, viewType) { number.toString() + viewType.suffix },
            textAlign = TextAlign.End,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Dimens.separation,
                    end = Dimens.separation,
                    bottom = Dimens.separation,
                ),
        )
    }

    companion object {

        fun factory(
            model: NumberViewModel,
        ): ViewProjector.Factory<Float> = ViewProjector.Factory { _, _ ->
            NumberViewProjector(
                model = model,
            )
        }
    }
}

internal fun ViewProjector.Companion.number(
    model: NumberViewModel,
): ViewProjector.Factory<Float> = NumberViewProjector.factory(
    model = model,
)