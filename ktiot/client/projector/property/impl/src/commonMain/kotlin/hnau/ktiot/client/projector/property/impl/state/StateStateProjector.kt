package hnau.ktiot.client.projector.property.impl.state

import hnau.ktiot.client.projector.property.impl.utils.IconWithContent

internal sealed interface StateStateProjector<T> {

    val iconWithSubcontent: IconWithContent.IconWithSubcontent
}