package hnau.ktiot.client.projector.property.impl

import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.TicModel
import hnau.ktiot.client.model.property.api.value.ValueModel
import hnau.ktiot.client.projector.property.api.ValueProjectorDependencies
import hnau.ktiot.client.projector.property.impl.state.state
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.CoroutineScope

interface ValueProjector<T> {

    val iconWithSubcontent: IconWithContent.IconWithSubcontent

    interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: ValueProjectorDependencies,
        ): ValueProjector<T>
    }

    companion object {

        operator fun <T> get(
            model: ValueModel<T>,
        ): Factory<T> = when (model) {
            is StateModel -> state(model)
            is TicModel -> tic(model)
        } as Factory<T>
    }
}
