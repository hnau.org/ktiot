package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.property.api.value.state.ready.ViewBasedOnActiveModel
import hnau.ktiot.client.projector.property.api.state.ready.ViewBasedOnActiveProjectorDependencies
import kotlinx.coroutines.CoroutineScope

class ViewBasedOnActiveProjector<T>(
    scope: CoroutineScope,
    dependencies: ViewBasedOnActiveProjectorDependencies,
    model: ViewBasedOnActiveModel<T>,
) : ViewProjector<T> {

    private val delegate: ActiveProjector<T> = ActiveProjector[model.model].create(
        scope = scope,
        dependencies = dependencies.active,
    )

    @Composable
    override fun Content() {
        delegate.iconWithContent.content.Content()
    }

    companion object {

        fun <T> factory(
            model: ViewBasedOnActiveModel<T>,
        ): ViewProjector.Factory<T> = ViewProjector.Factory { scope, dependencies ->
            ViewBasedOnActiveProjector(
                model = model,
                scope = scope,
                dependencies = dependencies.basedOnActive(),
            )
        }
    }
}

internal fun <T> ViewProjector.Companion.basedOnActive(
    model: ViewBasedOnActiveModel<T>,
): ViewProjector.Factory<T> = ViewBasedOnActiveProjector.factory(
    model = model,
)