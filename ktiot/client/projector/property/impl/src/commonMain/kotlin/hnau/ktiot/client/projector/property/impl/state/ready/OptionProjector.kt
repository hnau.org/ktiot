package hnau.ktiot.client.projector.property.impl.state.ready

import androidx.compose.animation.AnimatedContent
import androidx.compose.material.Switch
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import arrow.core.Either
import arrow.core.Option
import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.ktiot.client.model.common.ornull.ValueOrCancel
import hnau.ktiot.client.model.common.ornull.ValueOrReadOnly
import hnau.ktiot.client.model.property.api.value.state.ready.OptionActiveModel
import hnau.ktiot.client.projector.common.BooleanView
import hnau.ktiot.client.projector.property.api.state.ready.OptionProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.CancelContent
import hnau.ktiot.client.projector.property.impl.utils.Content
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChip
import hnau.ktiot.client.projector.property.impl.utils.StateButtonChipContent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class OptionProjector<T>(
    private val scope: CoroutineScope,
    private val model: OptionActiveModel<T>,
    private val dependencies: OptionProjectorDependencies,
) : ActiveProjector<Option<T>> {

    private val state: StateFlow<Either<EditProjector<T>, ActiveProjector<T>>?> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, state) ->
            state
                ?.map { activeModel ->
                    ActiveProjector[activeModel].create(
                        scope = scope,
                        dependencies = dependencies.active,
                    )
                }
                ?.mapLeft { editModel ->
                    EditProjector(
                        scope = stateScope,
                        model = editModel,
                        dependencies = dependencies.edit(),
                    )
                }
        }


    override val iconWithContent: IconWithContent = run {
        val iconWithSubcontent = IconWithContent.IconWithSubcontent.create(
            Icon = {
                val value by model.isSome.collectAsState()
                val sendOrCancel: ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>> by model.sendIsSomeOrNullOrReadOnly.collectAsState()
                AnimatedContent(
                    targetState = sendOrCancel,
                ) { sendOrCancelLocal: ValueOrReadOnly<StateFlow<ValueOrCancel<(Boolean) -> Unit>>> ->
                    when (sendOrCancelLocal) {
                        ValueOrReadOnly.ReadOnly -> BooleanView(
                            value = value,
                            localizer = dependencies.localizer,
                        )

                        is ValueOrReadOnly.Value -> WritableIcon(
                            value = value,
                            sendOrCancel = sendOrCancelLocal.value,
                        )
                    }
                }
            },
            Subcontent = {
                val state by state.collectAsState()
                AnimatedContent(
                    targetState = state,
                    contentKey = { initOrSomeOrNull ->
                        when (initOrSomeOrNull) {
                            null -> 0
                            is Either.Left -> 1
                            is Either.Right -> 2
                        }
                    }
                ) { initOrSomeOrNull ->
                    when (initOrSomeOrNull) {
                        is Either.Left -> initOrSomeOrNull.value.iconWithContent.content.Content()
                        is Either.Right -> initOrSomeOrNull.value.iconWithContent.content.Content()
                        null -> {}
                    }
                }
            },
        )
        IconWithContent(
            iconWithSubcontent = iconWithSubcontent,
            content = IconWithContent.Content.create(
                iconWithSubcontent = iconWithSubcontent,
                localizer = dependencies.localizer,
            )
        )
    }


    @Composable
    private fun WritableIcon(
        value: Boolean,
        sendOrCancel: StateFlow<ValueOrCancel<(Boolean) -> Unit>>,
    ) {
        val currentSendOrCancel: ValueOrCancel<(Boolean) -> Unit> by sendOrCancel.collectAsState()
        currentSendOrCancel.Content(
            cancel = { cancel ->
                StateButtonChip(
                    onClick = cancel,
                    content = StateButtonChipContent(
                        content = { CancelContent() },
                    ),
                )
            },
            value = { send ->
                Switch(
                    checked = value,
                    onCheckedChange = send,
                )
            }
        )
    }


    companion object {

        fun <T> factory(
            model: OptionActiveModel<T>,
        ): ActiveProjector.Factory<Option<T>> = ActiveProjector.Factory { scope, dependencies ->
            OptionProjector(
                scope = scope,
                model = model,
                dependencies = dependencies.option(dependencies),
            )
        }
    }
}

internal fun <T> ActiveProjector.Companion.option(
    model: OptionActiveModel<T>,
): ActiveProjector.Factory<Option<T>> = OptionProjector.factory(
    model = model,
)