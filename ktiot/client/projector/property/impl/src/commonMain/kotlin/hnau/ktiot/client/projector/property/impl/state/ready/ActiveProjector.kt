package hnau.ktiot.client.projector.property.impl.state.ready

import hnau.ktiot.client.model.property.api.value.state.ready.ActiveBasedOnPassiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.ActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.EnumActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.FlagActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.FractionActiveModel
import hnau.ktiot.client.model.property.api.value.state.ready.OptionActiveModel
import hnau.ktiot.client.projector.property.api.state.ready.ActiveProjectorDependencies
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.CoroutineScope

interface ActiveProjector<T> {

    val iconWithContent: IconWithContent

    fun interface Factory<T> {

        fun create(
            scope: CoroutineScope,
            dependencies: ActiveProjectorDependencies,
        ): ActiveProjector<T>
    }

    companion object {

        operator fun <T> get(
            model: ActiveModel<T>,
        ): Factory<T> = when (model) {
            is ActiveBasedOnPassiveModel<*> -> basedOnPassive(model)
            is EnumActiveModel -> enum(model)
            is FlagActiveModel -> flag(model)
            is FractionActiveModel -> fraction(model)
            is OptionActiveModel<*> -> option(model)
        } as Factory<T>
    }
}