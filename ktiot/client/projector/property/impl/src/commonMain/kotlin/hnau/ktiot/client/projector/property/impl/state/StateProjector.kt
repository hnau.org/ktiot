package hnau.ktiot.client.projector.property.impl.state

import hnau.common.kotlin.coroutines.mapState
import hnau.common.kotlin.coroutines.scopedInState
import hnau.ktiot.client.model.property.api.value.StateModel
import hnau.ktiot.client.model.property.api.value.state.InitBeforeFirstValueModel
import hnau.ktiot.client.model.property.api.value.state.ReadingFirstValueModel
import hnau.ktiot.client.model.property.api.value.state.ReadyModel
import hnau.ktiot.client.model.property.api.value.state.UnableParseValueModel
import hnau.ktiot.client.projector.property.api.ValueProjectorDependencies
import hnau.ktiot.client.projector.property.api.state.StateProjectorDependencies
import hnau.ktiot.client.projector.property.impl.ValueProjector
import hnau.ktiot.client.projector.property.impl.state.utils.create
import hnau.ktiot.client.projector.property.impl.utils.IconWithContent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

class StateProjector<T>(
    scope: CoroutineScope,
    dependencies: StateProjectorDependencies,
    model: StateModel<T>,
) : ValueProjector<T> {

    private val state: StateFlow<StateStateProjector<T>> = model
        .state
        .scopedInState(scope)
        .mapState(scope) { (stateScope, stateModel) ->
            when (stateModel) {

                is InitBeforeFirstValueModel -> InitBeforeFirstValueProjector(
                    scope = scope,
                    dependencies = dependencies.initBeforeFirstValue(),
                    model = stateModel,
                )

                is ReadingFirstValueModel -> ReadingFirstValueProjector(
                    model = stateModel,
                    dependencies = dependencies.readingFirstValueProjector(),
                )

                is ReadyModel -> ReadyProjector(
                    scope = stateScope,
                    dependencies = dependencies.ready(),
                    model = stateModel,
                )

                is UnableParseValueModel -> UnableParseValueProjector(
                    model = stateModel,
                    dependencies = dependencies.unableParseValue(),
                )
            }
        }

    override val iconWithSubcontent: IconWithContent.IconWithSubcontent =
        IconWithContent.IconWithSubcontent
            .create(
                state = state,
                contentKey = { stateProjector ->
                    when (stateProjector) {
                        is ReadingFirstValueProjector -> 0
                        is InitBeforeFirstValueProjector -> 1
                        is UnableParseValueProjector -> 2
                        is ReadyProjector -> 3
                    }
                },
                extractReadingFirstValueProjector = { it.iconWithSubcontent }
            )

    companion object {

        fun <T> factory(
            model: StateModel<T>,
        ): ValueProjector.Factory<T> = object : ValueProjector.Factory<T> {

            override fun create(
                scope: CoroutineScope,
                dependencies: ValueProjectorDependencies,
            ): ValueProjector<T> = StateProjector(
                scope = scope,
                model = model,
                dependencies = dependencies.state(),
            )
        }
    }
}

internal fun <T> ValueProjector.Companion.state(
    model: StateModel<T>,
): ValueProjector.Factory<T> = StateProjector.factory(
    model = model,
)
