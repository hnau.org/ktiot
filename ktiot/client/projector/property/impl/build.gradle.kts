plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(compose.materialIconsExtended)
            implementation(libs.kotlin.datetime)
            implementation(project(":common:color"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:property:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:property:api"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
