plugins {
    alias(libs.plugins.compose.desktop)
    alias(libs.plugins.ksp)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":ktiot:client:model:property:api"))
            implementation(project(":ktiot:client:projector:common"))
        }
    }
}
