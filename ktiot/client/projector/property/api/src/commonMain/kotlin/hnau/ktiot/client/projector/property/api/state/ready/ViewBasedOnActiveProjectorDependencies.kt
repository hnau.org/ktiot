package hnau.ktiot.client.projector.property.api.state.ready

import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ViewBasedOnActiveProjectorDependencies {

    val active: ActiveProjectorDependencies
}