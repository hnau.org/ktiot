package hnau.ktiot.client.projector.property.api

import hnau.ktiot.client.projector.property.api.state.StateProjectorDependencies
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ValueProjectorDependencies {

    fun tic(): TicProjectorDependencies

    fun state(): StateProjectorDependencies
}