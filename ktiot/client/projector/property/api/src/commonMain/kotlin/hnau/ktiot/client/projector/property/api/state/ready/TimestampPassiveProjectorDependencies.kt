package hnau.ktiot.client.projector.property.api.state.ready

import hnau.common.compose.uikit.bubble.BubblesShower
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.common.timestamp.TimestampEditProjector
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface TimestampPassiveProjectorDependencies {

    val bubblesShower: BubblesShower

    val localizer: Localizer

    fun delegate(): TimestampEditProjector.Dependencies
}