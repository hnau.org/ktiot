package hnau.ktiot.client.projector.property.api.state.ready

import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ActiveProjectorDependencies {

    fun basedOnPassive(
        active: ActiveProjectorDependencies,
    ): ActiveBasedOnPassiveProjectorDependencies

    fun flag(): FlagProjectorDependencies

    fun option(
        active: ActiveProjectorDependencies,
    ): OptionProjectorDependencies

    fun fraction(): FractionProjectorDependencies
}