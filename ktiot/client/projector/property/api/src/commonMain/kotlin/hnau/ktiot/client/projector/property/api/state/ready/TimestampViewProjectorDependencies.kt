package hnau.ktiot.client.projector.property.api.state.ready

import hnau.ktiot.client.projector.common.timestamp.TimestampViewProjector
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface TimestampViewProjectorDependencies {

    fun delegate(): TimestampViewProjector.Dependencies
}