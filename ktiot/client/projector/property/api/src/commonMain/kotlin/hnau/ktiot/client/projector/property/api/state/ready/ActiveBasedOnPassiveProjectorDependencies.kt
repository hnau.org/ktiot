package hnau.ktiot.client.projector.property.api.state.ready

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ActiveBasedOnPassiveProjectorDependencies {

    @Shuffle
    interface View {

        fun delegate(): ViewProjectorDependencies

        val localizer: Localizer
    }

    fun view(): View

    @Shuffle
    interface Edit {

        fun edit(): EditProjectorDependencies
    }

    fun edit(): Edit

    val localizer: Localizer
}