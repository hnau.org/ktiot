package hnau.ktiot.client.projector.property.api

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface TicProjectorDependencies {

    val localizer: Localizer
}