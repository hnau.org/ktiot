package hnau.ktiot.client.projector.property.api.state

import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.property.api.state.ready.ActiveProjectorDependencies
import hnau.ktiot.client.projector.property.api.state.ready.EditProjectorDependencies
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface InitBeforeFirstValueProjectorDependencies {

    fun active(): ActiveProjectorDependencies

    fun edit(
        active: ActiveProjectorDependencies,
    ): EditProjectorDependencies

    val localizer: Localizer
}