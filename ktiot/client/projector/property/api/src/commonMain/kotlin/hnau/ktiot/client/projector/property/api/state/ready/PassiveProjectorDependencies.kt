package hnau.ktiot.client.projector.property.api.state.ready

import hnau.shuffler.annotations.Shuffle

@Shuffle
interface PassiveProjectorDependencies {

    fun basedOnActive(): PassiveBasedOnActiveProjectorDependencies

    fun timestamp(): TimestampPassiveProjectorDependencies

    fun timestamped(
        passive: PassiveProjectorDependencies,
    ): TimestampedPassiveProjectorDependencies
}