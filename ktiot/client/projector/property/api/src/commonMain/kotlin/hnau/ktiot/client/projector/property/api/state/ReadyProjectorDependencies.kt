package hnau.ktiot.client.projector.property.api.state

import hnau.ktiot.client.projector.property.api.state.ready.ActiveProjectorDependencies
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ReadyProjectorDependencies {

    fun active(): ActiveProjectorDependencies
}
