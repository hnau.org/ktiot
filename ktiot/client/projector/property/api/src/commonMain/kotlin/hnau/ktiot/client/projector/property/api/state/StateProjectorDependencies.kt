package hnau.ktiot.client.projector.property.api.state

import hnau.shuffler.annotations.Shuffle

@Shuffle
interface StateProjectorDependencies {

    fun unableParseValue(): UnableParseValueProjectorDependencies

    fun ready(): ReadyProjectorDependencies

    fun initBeforeFirstValue(): InitBeforeFirstValueProjectorDependencies

    fun readingFirstValueProjector(): ReadingFirstValueProjectorDependencies
}