package hnau.ktiot.client.projector.property.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface PropertyProjector<T> {

    @Shuffle
    interface Dependencies {

        val localizer: Localizer

        fun value(): ValueProjectorDependencies
    }

    @Composable
    fun Content()

    interface Factory {

        fun <T> createPropertyProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: PropertyModel<T>,
        ): PropertyProjector<T>

        companion object
    }
}