package hnau.ktiot.client.projector.property.api.state.ready

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface FractionProjectorDependencies {

    val localizer: Localizer
}