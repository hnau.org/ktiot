package hnau.ktiot.client.projector.property.api.state

import hnau.ktiot.client.projector.common.Localizer
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface UnableParseValueProjectorDependencies {

    val localizer: Localizer
}