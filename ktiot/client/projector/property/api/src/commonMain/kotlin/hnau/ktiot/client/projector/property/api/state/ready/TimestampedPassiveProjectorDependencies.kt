package hnau.ktiot.client.projector.property.api.state.ready

import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.common.timestamp.TimestampEditProjector
import hnau.shuffler.annotations.Shuffle

@Shuffle
interface TimestampedPassiveProjectorDependencies {

    fun timestampEdit(): TimestampEditProjector.Dependencies

    val passive: PassiveProjectorDependencies

    val localizer: Localizer
}