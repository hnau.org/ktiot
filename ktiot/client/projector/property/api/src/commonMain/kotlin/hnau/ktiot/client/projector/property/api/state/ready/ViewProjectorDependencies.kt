package hnau.ktiot.client.projector.property.api.state.ready

import hnau.shuffler.annotations.Shuffle

@Shuffle
interface ViewProjectorDependencies {

    fun text(): TextViewProjectorDependencies

    fun timestamp(): TimestampViewProjectorDependencies

    fun timestamped(
        view: ViewProjectorDependencies,
    ): TimestampedViewProjectorDependencies

    fun basedOnActive(): ViewBasedOnActiveProjectorDependencies
}