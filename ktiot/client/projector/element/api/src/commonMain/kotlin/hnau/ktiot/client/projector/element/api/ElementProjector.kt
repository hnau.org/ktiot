package hnau.ktiot.client.projector.element.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.property.api.PropertyProjector
import hnau.shuffler.annotations.Shuffle
import kotlinx.coroutines.CoroutineScope

interface ElementProjector {

    @Shuffle
    interface Dependencies {

        val localizer: Localizer

        fun property(): PropertyProjector.Dependencies
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createElementProjector(
            scope: CoroutineScope,
            dependencies: Dependencies,
            model: ElementModel,
        ): ElementProjector

        companion object
    }
}