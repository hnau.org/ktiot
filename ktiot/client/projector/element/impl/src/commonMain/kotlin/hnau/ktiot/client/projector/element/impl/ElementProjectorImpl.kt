package hnau.ktiot.client.projector.element.impl

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import hnau.common.app.ListScrollState
import hnau.common.compose.uikit.chip.Chip
import hnau.common.compose.uikit.chip.ChipSize
import hnau.common.compose.uikit.chip.ChipStyle
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.toLazyListState
import hnau.ktiot.client.model.common.ClickableElement
import hnau.ktiot.client.model.element.api.ElementModel
import hnau.ktiot.client.model.property.api.PropertyModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.common.title
import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.ktiot.client.projector.property.api.PropertyProjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow

internal class ElementProjectorImpl(
    scope: CoroutineScope,
    private val dependencies: ElementProjector.Dependencies,
    private val model: ElementModel,
    private val propertyProjectorFactory: PropertyProjector.Factory,
) : ElementProjector {

    @Composable
    override fun Content() {
        BoxWithConstraints(
            modifier = Modifier
                .fillMaxSize(),
        ) {
            val localizer = dependencies.localizer
            when (maxWidth > 512.dp) {
                true -> Row(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    val rowModifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                    ShowList(
                        scrollState = model.propertiesScrollState,
                        modifier = rowModifier,
                    ) {
                        addProperties(localizer = localizer)
                    }
                    ShowList(
                        scrollState = model.childrenScrollState,
                        modifier = rowModifier,
                    ) {
                        addChildren(localizer = localizer)
                    }
                }

                false -> ShowList(
                    scrollState = model.propertiesScrollState,
                    modifier = Modifier.fillMaxSize(),
                ) {
                    addProperties(localizer = localizer)
                    addChildren(localizer = localizer)
                }
            }
        }
    }

    private val propertyProjectors: List<PropertyProjector<*>> =
        model.properties.map { propertyModel: PropertyModel<*> ->
            propertyProjectorFactory.createPropertyProjector(
                scope = scope,
                dependencies = dependencies.property(),
                model = propertyModel,
            )
        }

    @OptIn(ExperimentalFoundationApi::class)
    private fun LazyListScope.addHeader(
        key: String,
        title: String,
    ) {
        stickyHeader(
            key = key,
        ) {
            Text(
                text = title,
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colors.background)
                    .padding(vertical = Dimens.smallSeparation),
            )
        }
    }

    private fun LazyListScope.addProperties(
        localizer: Localizer,
    ) {
        addHeader(
            key = "properties_header",
            title = localizer.element.properties,
        )
        items(propertyProjectors) { projector ->
            projector.Content()
        }
    }

    private fun LazyListScope.addChildren(
        localizer: Localizer,
    ) {
        addHeader(
            key = "children_header",
            title = localizer.element.children,
        )
        items(model.children) { child: ClickableElement ->
            Chip(
                modifier = Modifier.fillMaxWidth(),
                onClick = child.onClick,
                style = ChipStyle.chip,
                size = ChipSize.large,
                content = { Text(child.keyOrTitle.title(dependencies.localizer)) },
            )
        }
    }

    @Composable
    private fun ShowList(
        scrollState: MutableStateFlow<ListScrollState>,
        modifier: Modifier = Modifier,
        content: LazyListScope.() -> Unit,
    ) {
        LazyColumn(
            modifier = modifier,
            verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
            state = scrollState.toLazyListState(),
            contentPadding = PaddingValues(Dimens.separation),
        ) {
            content()
        }
    }

}
