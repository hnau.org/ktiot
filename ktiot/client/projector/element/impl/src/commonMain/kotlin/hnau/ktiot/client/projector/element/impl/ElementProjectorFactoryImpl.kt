package hnau.ktiot.client.projector.element.impl

import hnau.ktiot.client.projector.element.api.ElementProjector
import hnau.ktiot.client.projector.property.api.PropertyProjector

fun ElementProjector.Factory.Companion.impl(
    propertyProjectorFactory: PropertyProjector.Factory,
): ElementProjector.Factory = ElementProjector.Factory { scope, dependencies, model ->
    ElementProjectorImpl(
        scope = scope,
        dependencies = dependencies,
        model = model,
        propertyProjectorFactory = propertyProjectorFactory,
    )
}