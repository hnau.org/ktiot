plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:model:common"))
            implementation(project(":ktiot:client:model:element:api"))
            implementation(project(":ktiot:client:model:property:api"))
            implementation(project(":ktiot:client:projector:common"))
            implementation(project(":ktiot:client:projector:element:api"))
            implementation(project(":ktiot:client:projector:property:api"))
        }
    }
}
