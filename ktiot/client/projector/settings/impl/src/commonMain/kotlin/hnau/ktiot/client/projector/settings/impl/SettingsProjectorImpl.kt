package hnau.ktiot.client.projector.settings.impl

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import hnau.common.app.goback.GoBackHandlerProvider
import hnau.common.compose.uikit.Button
import hnau.common.compose.uikit.utils.Dimens
import hnau.common.compose.utils.toLazyListState
import hnau.common.kotlin.remindSupertype
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.client.projector.common.TopAppBarDelegate
import hnau.ktiot.client.projector.settings.api.SettingsProjector

internal class SettingsProjectorImpl(
    private val dependencies: SettingsProjector.Dependencies,
    private val model: SettingsModel,
) : SettingsProjector {

    private val topAppBarDelegate = TopAppBarDelegate(
        dependencies = dependencies.topAppBar(),
    )

    @Composable
    override fun Content() = Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
    ) {
        topAppBarDelegate.Content { startContentPadding ->
            Text(
                modifier = Modifier
                    .padding(start = startContentPadding),
                text = dependencies.localizer.settings.settings,
            )
        }
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            state = model
                .remindSupertype<_, GoBackHandlerProvider>()
                .scrollState
                .toLazyListState(),
            contentPadding = PaddingValues(Dimens.separation),
        ) {
            item(
                key = "logout",
            ) {
                Button(
                    onClickOrExecuting = model.logout,
                ) {
                    Text(dependencies.localizer.logged.logout)
                }
            }
        }
    }
}
