package hnau.ktiot.client.projector.settings.impl

import hnau.ktiot.client.projector.settings.api.SettingsProjector

fun SettingsProjector.Factory.Companion.impl(): SettingsProjector.Factory =
    SettingsProjector.Factory(::SettingsProjectorImpl)