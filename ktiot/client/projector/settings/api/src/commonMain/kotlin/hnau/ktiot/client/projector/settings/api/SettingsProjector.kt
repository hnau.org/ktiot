package hnau.ktiot.client.projector.settings.api

import androidx.compose.runtime.Composable
import hnau.ktiot.client.model.settings.api.SettingsModel
import hnau.ktiot.client.projector.common.Localizer
import hnau.ktiot.client.projector.common.TopAppBarDelegate
import hnau.shuffler.annotations.Shuffle

interface SettingsProjector {

    @Shuffle
    interface Dependencies {

        val localizer: Localizer

        fun topAppBar(): TopAppBarDelegate.Dependencies
    }

    @Composable
    fun Content()

    fun interface Factory {

        fun createSettingsProjector(
            dependencies: Dependencies,
            model: SettingsModel,
        ): SettingsProjector

        companion object
    }
}