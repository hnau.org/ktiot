plugins {
    alias(libs.plugins.compose.desktop)
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(libs.slf4j.simple)
            implementation(project(":common:app"))
            implementation(project(":ktiot:client:app"))
            implementation(project(":ktiot:client:compose"))
            implementation(project(":ktiot:client:projector:common"))
        }
    }
}

compose.desktop {
    application {
        mainClass = "hnau.ktiot.client.desktop.DesktopAppKt"
    }
}
