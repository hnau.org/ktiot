package hnau.ktiot.client.desktop

import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.InternalComposeApi
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.LocalSystemTheme
import androidx.compose.ui.SystemTheme
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import hnau.common.app.storage.Storage
import hnau.common.app.storage.file
import hnau.ktiot.client.app.KtIotApp
import hnau.ktiot.client.app.SavedState
import hnau.ktiot.client.app.commonImpl
import hnau.ktiot.client.compose.Content
import hnau.ktiot.client.compose.utils.KtIoTIcon
import hnau.ktiot.client.compose.utils.LocalizerImpl
import hnau.ktiot.client.projector.common.Localizer
import org.slf4j.simple.SimpleLogger
import java.io.File

@OptIn(InternalComposeApi::class)
fun main() = application {
    System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "TRACE")
    val scale = 2f
    val appScope = rememberCoroutineScope()
    val app = KtIotApp(
        scope = appScope,
        savedState = SavedState(null),
        dependencies = KtIotApp.Dependencies.commonImpl(
            storageFactory = Storage.Factory.file(
                file = File("storage.json"),
            ),
        ),
    )
    val localizer: Localizer = LocalizerImpl
    Window(
        onCloseRequest = { exitApplication() },
        title = "KtIoT",
        state = rememberWindowState(width = 720.dp * scale, height = 640.dp * scale),
        icon = rememberVectorPainter(KtIoTIcon.s256),
    ) {
        CompositionLocalProvider(
            LocalDensity provides Density(scale),
            LocalSystemTheme provides SystemTheme.Dark,
        ) {
            app.Content(
                localizer = localizer,
            )
        }
    }
}
