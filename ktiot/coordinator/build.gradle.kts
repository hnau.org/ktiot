plugins {
    id("hnau.kotlin.multiplatform")
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            implementation(project(":common:mqtt"))
            implementation(project(":ktiot:scheme"))
        }
    }
}
