package hnau.ktiot.coordinator

import arrow.core.Option
import hnau.common.kotlin.ErrorOrValue
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.MqttQos
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface Element {

    val scope: CoroutineScope

    fun <T, V : PropertyViewType<T>> property(
        topic: MqttTopic,
        publishTopic: MqttTopic? = null,
        title: String? = null,
        type: V,
        mapper: SerializableMapper<MqttData, T> = type.defaultMqttDataMapper,
        qos: MqttQos = MqttQos.default,
        valueSource: ValueSource,
    ): Property<T, V>

    fun <T, E : PropertyViewType.Events<T>> Property<T, E>.read(): Flow<T>

    fun <T, S : PropertyViewType.State<T>> Property<T, S>.read(): StateFlow<Option<ErrorOrValue<T>>>

    suspend fun <T, P : PropertyViewType<T>> Property<T, P>.send(value: T)

    fun child(
        key: MqttTopic.Relative,
        title: String? = null,
        config: Element.() -> Unit,
    )
}
