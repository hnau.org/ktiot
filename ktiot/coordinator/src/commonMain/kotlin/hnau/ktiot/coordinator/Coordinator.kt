package hnau.ktiot.coordinator

import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.publishTyped
import hnau.ktiot.coordinator.utils.SchemeReceiverImpl
import hnau.ktiot.coordinator.utils.createElement
import hnau.ktiot.scheme.SchemeConstants
import hnau.ktiot.scheme.SchemeElement
import kotlinx.coroutines.CoroutineScope

suspend fun MqttConnection.applyCoordinator(
    connectionScope: CoroutineScope,
    config: Element.() -> Unit,
) {
    val schemeReceiver = SchemeReceiverImpl(
        topic = SchemeConstants.dataElementTopic,
    )
    val rootElement = createElement(
        scope = connectionScope,
        schemeReceiver = schemeReceiver,
    )
    rootElement.config()
    val scheme = schemeReceiver.build()
    publishTyped(
        topic = SchemeConstants.schemeTopic,
        retained = true,
        payload = scheme,
        serializer = SchemeElement.serializer.reverse,
    )
}
