package hnau.ktiot.coordinator.utils

import hnau.common.mqtt.topic.MqttTopic
import hnau.common.mqtt.topic.toAbsolute
import hnau.ktiot.scheme.SchemeElement
import hnau.ktiot.scheme.property.PropertyInfo

internal class SchemeReceiverImpl(
    private val title: String? = null,
    private val topic: MqttTopic.Absolute,
) : SchemeReceiver {

    private val properties = ArrayList<PropertyInfo<*>>()

    private val children = HashMap<MqttTopic.Relative, SchemeReceiverImpl>()

    fun build(): SchemeElement = SchemeElement(
        title = title,
        properties = properties as List<PropertyInfo<Any?>>,
        children = children.mapValues { (_, child) -> child.build() },
    )

    override fun <T> registerProperty(
        info: PropertyInfo<T>,
    ): MqttTopic.Absolute {
        properties.add(info)
        return info.topic.toAbsolute(
            relativePrefix = topic
        )
    }

    override fun registerChild(
        key: MqttTopic.Relative,
        title: String?,
    ): SchemeReceiver {
        val childReceiver = SchemeReceiverImpl(
            title = title,
            topic = topic + key,
        )
        children[key] = childReceiver
        return childReceiver
    }
}
