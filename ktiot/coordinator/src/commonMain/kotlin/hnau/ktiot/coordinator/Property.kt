package hnau.ktiot.coordinator

import hnau.common.mqtt.MqttData
import hnau.common.mqtt.MqttQos
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper

data class Property<T, out V : PropertyViewType<T>>(
    internal val topic: MqttTopic.Absolute,
    internal val qos: MqttQos,
    internal val viewType: V,
    internal val mapper: SerializableMapper<MqttData, T>,
)
