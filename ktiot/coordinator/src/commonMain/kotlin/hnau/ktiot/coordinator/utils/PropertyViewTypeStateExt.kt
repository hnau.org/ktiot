package hnau.ktiot.coordinator.utils

import hnau.ktiot.scheme.property.presentation.PropertyViewType

fun <T> PropertyViewType.State<T>.toOption(): PropertyViewType.State.Option<T> =
    PropertyViewType.State.Option(this)