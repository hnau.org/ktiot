package hnau.ktiot.coordinator.utils

import arrow.core.Option
import hnau.common.kotlin.ErrorOrValue
import hnau.common.mqtt.MqttConnection
import hnau.common.mqtt.MqttData
import hnau.common.mqtt.MqttQos
import hnau.common.mqtt.publishTyped
import hnau.common.mqtt.subscribeToEventsTyped
import hnau.common.mqtt.subscribeToStateTyped
import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.coordinator.Element
import hnau.ktiot.coordinator.Property
import hnau.ktiot.scheme.property.PropertyInfo
import hnau.ktiot.scheme.property.ValueSource
import hnau.ktiot.scheme.property.presentation.PropertyType
import hnau.ktiot.scheme.property.presentation.PropertyViewType
import hnau.ktiot.scheme.property.presentation.mapper.SerializableMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

internal fun MqttConnection.createElement(
    scope: CoroutineScope,
    schemeReceiver: SchemeReceiver,
): Element = object : Element {

    override val scope: CoroutineScope
        get() = scope

    override fun <T, V : PropertyViewType<T>> property(
        topic: MqttTopic,
        publishTopic: MqttTopic?,
        title: String?,
        type: V,
        mapper: SerializableMapper<MqttData, T>,
        qos: MqttQos,
        valueSource: ValueSource,
    ): Property<T, V> = Property(
        topic = schemeReceiver.registerProperty(
            info = PropertyInfo(
                topic = topic,
                publishTopic = publishTopic,
                title = title,
                type = PropertyType(
                    viewType = type,
                    mapper = mapper,
                ),
                valueSource = valueSource,
            ),
        ),
        qos = qos,
        viewType = type,
        mapper = mapper,
    )

    override fun <T, E : PropertyViewType.Events<T>> Property<T, E>.read(): Flow<T> =
        subscribeToEventsTyped(
            topic = topic,
            qos = qos,
            deserializer = mapper.mapper.direct,
        )

    override fun <T, S : PropertyViewType.State<T>> Property<T, S>.read(): StateFlow<Option<ErrorOrValue<T>>> =
        subscribeToStateTyped(
            scope = scope,
            topic = topic,
            qos = qos,
            deserializer = mapper.mapper.direct,
        )

    override suspend fun <T, P : PropertyViewType<T>> Property<T, P>.send(
        value: T,
    ) = publishTyped(
        topic = topic,
        retained = viewType is PropertyViewType.State<*>,
        qos = qos,
        payload = value,
        serializer = mapper.mapper.reverse,
    )

    override fun child(
        key: MqttTopic.Relative,
        title: String?,
        config: Element.() -> Unit,
    ) {
        val childSchemeReceiver = schemeReceiver.registerChild(
            key = key,
            title = title,
        )
        val element = createElement(
            scope = scope,
            schemeReceiver = childSchemeReceiver,
        )
        element.config()
    }
}
