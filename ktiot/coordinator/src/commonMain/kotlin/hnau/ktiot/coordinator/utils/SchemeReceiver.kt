package hnau.ktiot.coordinator.utils

import hnau.common.mqtt.topic.MqttTopic
import hnau.ktiot.scheme.property.PropertyInfo

internal interface SchemeReceiver {

    fun <T> registerProperty(
        info: PropertyInfo<T>,
    ): MqttTopic.Absolute

    fun registerChild(
        key: MqttTopic.Relative,
        title: String? = null,
    ): SchemeReceiver
}
